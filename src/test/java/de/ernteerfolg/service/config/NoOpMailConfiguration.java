package de.ernteerfolg.service.config;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.ernteerfolg.service.service.NotificationService;

@Configuration
public class NoOpMailConfiguration {
    private final NotificationService mockNotificationService;

    public NoOpMailConfiguration() {
        mockNotificationService = mock(NotificationService.class);
        doNothing().when(mockNotificationService).notifyUserAboutActivationLink(any());
    }

    @Bean
    public NotificationService notificationService() {
        return mockNotificationService;
    }
}
