package de.ernteerfolg.service.uc.deactivatetask;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.repository.WorkdayRepository;
import de.ernteerfolg.service.web.api.model.CreateTaskPayload;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import de.ernteerfolg.service.web.rest.Routinen.GenerateEntities;
import de.ernteerfolg.service.web.rest.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser(roles = "FARM", username = "user")
public class TestUpdateTaskActive {

    @Autowired
    private GenerateEntities generateEntities;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    public MockMvc restUserExtendMockMvc;

    @Autowired
    private WorkdayRepository workdayRepository;


    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void addWorkDayToActiveTaskAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        task.setActive(true);
        task = taskRepository.save(task);
        int vorPostWorkday = workdayRepository.findAll().size();
        task.addRequestedDays(new Workday().date(LocalDate.of(2014, 12, 31)).workingHours(12));
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        int vorPostTask = taskRepository.findAll().size();
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(task.getPhone()))
            .andExpect(jsonPath("$.email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(0).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(0).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(2).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(2).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(3).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(3).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(4).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(4).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(5).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(5).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(1).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(1).getWorkingHours())));

        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday + task.getRequestedDays().size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void removeWorkDayToActiveTaskAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        task.setActive(true);
        task = taskRepository.save(task);
        int vorPostWorkday = workdayRepository.findAll().size();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        createTaskPayload.setRequestedDays(new ArrayList<WorkdayPayload>());
        int vorPostTask = taskRepository.findAll().size();
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());

        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday);
    }

//    @Test
//    @Transactional
//    @WithMockUser(roles = "FARM", username = "user")
//    public void changeAddressAtActiveTaskAsUserWithRoleFarm() throws Exception {
//
//        Task task = generateEntities.createTask();
//        task.setActive(true);
//        task = taskRepository.save(task);
//        int vorPostWorkday = workdayRepository.findAll().size();
//        int vorPostTask = taskRepository.findAll().size();
//        CreateTaskPayload createTaskPayload = createTaskPayload(task);
//        createTaskPayload.setCity("BBBBBBB");
//        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
//        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
//            .andExpect(status().isForbidden());
//
//        List<Task> taskList = taskRepository.findAll();
//        assertThat(taskList).hasSize(vorPostTask);
//
//        List<Workday> workdayList = workdayRepository.findAll();
//        assertThat(workdayList).hasSize(vorPostWorkday);
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser(roles = "FARM", username = "user")
//    public void changeDesciptionAtActiveTaskAsUserWithRoleFarm() throws Exception {
//
//        Task task = generateEntities.createTask();
//        task.setActive(true);
//        task = taskRepository.save(task);
//        int vorPostWorkday = workdayRepository.findAll().size();
//        CreateTaskPayload createTaskPayload = createTaskPayload(task);
//        createTaskPayload.setTaskDescription("BBBBBBB");
//        int vorPostTask = taskRepository.findAll().size();
//        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
//        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
//            .andExpect(status().isForbidden());
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser(roles = "FARM", username = "user")
//    public void changeHowToFindUsAtActiveTaskAsUserWithRoleFarm() throws Exception {
//
//        Task task = generateEntities.createTask();
//        task.setActive(true);
//        task = taskRepository.save(task);
//        int vorPostWorkday = workdayRepository.findAll().size();
//        CreateTaskPayload createTaskPayload = createTaskPayload(task);
//        createTaskPayload.setHowToFindUs("BBBBBBB");
//        int vorPostTask = taskRepository.findAll().size();
//        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
//        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
//            .andExpect(status().isForbidden());
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser(roles = "FARM", username = "user")
//    public void changeTitleAtActiveTaskAsUserWithRoleFarm() throws Exception {
//
//        Task task = generateEntities.createTask();
//        task.setActive(true);
//        task = taskRepository.save(task);
//        int vorPostWorkday = workdayRepository.findAll().size();
//        CreateTaskPayload createTaskPayload = createTaskPayload(task);
//        createTaskPayload.setTitle("BBBBBBB");
//        int vorPostTask = taskRepository.findAll().size();
//        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
//        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
//            .andExpect(status().isForbidden());
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser(roles = "FARM", username = "user")
//    public void changeEmailAtActiveTaskAsUserWithRoleFarm() throws Exception {
//
//        Task task = generateEntities.createTask();
//        task.setActive(true);
//        task = taskRepository.save(task);
//        int vorPostWorkday = workdayRepository.findAll().size();
//        CreateTaskPayload createTaskPayload = createTaskPayload(task);
//        createTaskPayload.setEmail("BBBBBBB@bingo.com");
//        int vorPostTask = taskRepository.findAll().size();
//        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
//        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
//            .andExpect(status().isForbidden());
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser(roles = "FARM", username = "user")
//    public void changePhoneAtActiveTaskAsUserWithRoleFarm() throws Exception {
//
//        Task task = generateEntities.createTask();
//        task.setActive(true);
//        task = taskRepository.save(task);
//        int vorPostWorkday = workdayRepository.findAll().size();
//        CreateTaskPayload createTaskPayload = createTaskPayload(task);
//        createTaskPayload.setPhone("BBBBBBB@bingo.com");
//        int vorPostTask = taskRepository.findAll().size();
//        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
//        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
//            .andExpect(status().isForbidden());
//    }
//
//
    private CreateTaskPayload createTaskPayload(Task task) {
        return new CreateTaskPayload()
            .title(task.getTitle())
            .taskDescription(task.getDescription())
            .howToFindUs(task.getHowToFindUs())
            .phone(task.getPhone())
            .email(task.getContactEmail())
            .houseNr(task.getAddress().getHouseNr())
            .street(task.getAddress().getStreet())
            .zip(task.getAddress().getZip())
            .city(task.getAddress().getCity())
            .requestedDays(
                new ArrayList<WorkdayPayload>
                    (task.getRequestedDays()
                        .stream()
                        .map(
                            workday -> {
                                return new WorkdayPayload().workingHours(workday.getWorkingHours()).date(workday.getDate());
                            }).collect(Collectors.toList()))
            );
    }


}
