package de.ernteerfolg.service.uc.deactivatetask;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortFarmStorage;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortOfferStorage;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortTaskStorage;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortUserNotification;
import de.ernteerfolg.service.uc.deaktivatetask.UseCaseDeactivateTask;
import de.ernteerfolg.service.uc.deletefarm.OutputPortUserStorage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


public class TestDeactivateTask_sendUserNotification {

    private OutputPortUserNotification outputPortUserNotification;
    private OutputPortTaskStorage outputPortTaskStorage;
    private OutputPortFarmStorage outputPortFarmStorage;
    private OutputPortOfferStorage outputPortOfferStorage;
    private OutputPortUserStorage outputPortUserStorage;

    @BeforeEach
    public void before() {
        MockitoAnnotations.initMocks(this);

        outputPortUserNotification = Mockito.mock(OutputPortUserNotification.class);
        outputPortTaskStorage = Mockito.mock(OutputPortTaskStorage.class);
        outputPortFarmStorage = Mockito.mock(OutputPortFarmStorage.class);
        outputPortOfferStorage = Mockito.mock(OutputPortOfferStorage.class);
        outputPortUserStorage = Mockito.mock(OutputPortUserStorage.class);
    }

    @Test
    void testSendConfirmationMail_success() {

        UseCaseDeactivateTask toTest = new UseCaseDeactivateTask(outputPortTaskStorage,
            outputPortFarmStorage, outputPortOfferStorage, outputPortUserNotification);

        User user = new User();
        user.setLogin("user");

        Helper helper = new Helper();
        helper.setUser(user);

        Offer offer = new Offer();
        offer.setHelper(helper);

        List<Offer> offersToBeNotified = new ArrayList<>();
        offersToBeNotified.add(offer);

        toTest.notifyHelpersAboutDeactivatedTask(offersToBeNotified);

        // Check that the notification was called
        verify(outputPortUserNotification, times(1))
            .notifyHelperAboutDeactivatedTask(user, offer);
    }

    @Test
    void testSendConfirmationMail_NULL() {

        UseCaseDeactivateTask toTest = new UseCaseDeactivateTask(outputPortTaskStorage,
            outputPortFarmStorage, outputPortOfferStorage, outputPortUserNotification);

        toTest.notifyHelpersAboutDeactivatedTask(null);

        // Check that the notification was NOT called
        verify(outputPortUserNotification, times(0))
            .notifyHelperAboutDeactivatedTask(Mockito.any(), Mockito.any());

        // Check, that save was NOT called
        verify(outputPortUserStorage, times(0)).save(Mockito.any());
    }

    @Test
    public void testNoMail_TaskWithoutOffer() {

        UseCaseDeactivateTask toTest = new UseCaseDeactivateTask(outputPortTaskStorage,
            outputPortFarmStorage, outputPortOfferStorage, outputPortUserNotification);

        List<Offer> offersToBeNotified = new ArrayList<>();
        toTest.notifyHelpersAboutDeactivatedTask(offersToBeNotified);

        // Check that the notification was called
        verify(outputPortUserNotification, times(0))
            .notifyHelperAboutDeactivatedTask(Mockito.any(), Mockito.any());
    }
}
