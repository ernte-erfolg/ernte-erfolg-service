package de.ernteerfolg.service.uc.deactivatetask;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.repository.WorkdayRepository;
import de.ernteerfolg.service.web.rest.Routinen.GenerateEntities;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser(roles = "FARM", username = "user")
public class TestDeaktivetedTask {

    @Autowired
    private GenerateEntities generateEntities;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    public MockMvc restUserExtendMockMvc;

    @Autowired
    private WorkdayRepository workdayRepository;

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void getDeactivatedTaskAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createTask();
        task.setActive(false);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        Set<Workday> workdaySet = taskRequestedDays.stream()
            .map(work -> (Workday)((JpaRepository) workdayRepository).save(work)).collect(Collectors.toSet());
        task.setRequestedDays(workdaySet);
        task = taskRepository.save(task);

        restUserExtendMockMvc.perform(MockMvcRequestBuilders.get("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasSize(0)));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getDeactivatedOwnTaskAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        task.setActive(false);
        task = taskRepository.save(task);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(task.getPhone()))
            .andExpect(jsonPath("$.email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.lat").isNotEmpty())
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(0).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(0).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(2).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(2).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(3).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(3).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(4).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(4).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(1).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(1).getWorkingHours())));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "admin")
    public void getTasksByFarmAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        task.setActive(true);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        Set<Workday> workdaySet = taskRequestedDays.stream().map(work -> (Workday)((JpaRepository) workdayRepository).save(work)).collect(Collectors.toSet());
        task.setRequestedDays(workdaySet);
        task = taskRepository.save(task);
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);
        task.setActive(false);
        task = taskRepository.save(task);

        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasSize(1)));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void getTasksByFarmAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        task.setActive(false);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        Set<Workday> workdaySet = taskRequestedDays.stream().map(work -> (Workday)((JpaRepository) workdayRepository).save(work)).collect(Collectors.toSet());
        task.setRequestedDays(workdaySet);
        task.setActive(true);
        task = taskRepository.save(task);
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);
        task.setActive(false);
        task = taskRepository.save(task);

        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasSize(0)));
    }


    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void searchOwnTasksAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        task.setActive(true);
        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);
        task.setActive(false);
        task = taskRepository.save(task);

        restUserExtendMockMvc
            .perform(get("/api/tasks/search?zip=" + task.getAddress().getZip())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasSize(0)));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ANONYMOUS", username = "user")
    public void searchTasksAsUserWithRoleAnonymous() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        task.setActive(true);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        Set<Workday> workdaySet = taskRequestedDays.stream().map(work -> (Workday)((JpaRepository) workdayRepository).save(work)).collect(Collectors.toSet());
        task.setRequestedDays(workdaySet);
        task = taskRepository.save(task);
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);
        task.setActive(false);
        taskRepository.save(task);

        restUserExtendMockMvc
            .perform(get("/api/tasks/search?zip=" + task.getAddress().getZip())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasSize(0)));
    }


    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getOwnTasksByFarmAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        Set<Workday> workdaySet = taskRequestedDays.stream().map(work -> (Workday)((JpaRepository) workdayRepository).save(work)).collect(Collectors.toSet());
        task.setRequestedDays(workdaySet);
        task = taskRepository.save(task);
        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(task.getId().intValue())))
            .andExpect(jsonPath("$.[*].farmId").value(hasItem(task.getFarm().getId().intValue())))
        ;
    }


}
