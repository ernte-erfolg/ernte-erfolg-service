package de.ernteerfolg.service.uc.deletefarm;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Workday;

class TestDeleteFarm_hasGuaranteedDayInFuture {

	Workday future = new Workday().date(LocalDate.now().plus(Period.ofDays(2)));
    Workday past = new Workday().date(LocalDate.now().minus(Period.ofDays(2)));
    Set<Workday> listOfWorkdays;

    @BeforeEach
    void setUp()
    {
        listOfWorkdays = new HashSet<>();
    }

	@Test
	void testHasGuaranteedDayInFuture_EmptyOffer() {
		Offer offer = new Offer();
		assertFalse(UseCaseDeleteFarm.hasGuaranteedDayInFuture(offer));
	}

	@Test
	void testHasGuaranteedDayInFuture_NULL() {
		assertFalse(UseCaseDeleteFarm.hasGuaranteedDayInFuture(null));
	}

	@Test
	void testHasGuaranteedDayInFuture_Future() {
		Offer offer = new Offer();
		listOfWorkdays.add(future);
		offer.setGuaranteedDays(listOfWorkdays);
		assertTrue(UseCaseDeleteFarm.hasGuaranteedDayInFuture(offer));
	}

	@Test
	void testHasGuaranteedDayInFuture_Past() {
		Offer offer = new Offer();
		listOfWorkdays.add(past);
		offer.setGuaranteedDays(listOfWorkdays);
		assertFalse(UseCaseDeleteFarm.hasGuaranteedDayInFuture(offer));
	}
	
	@Test
	void testHasGuaranteedDayInFuture_Mixed() {
		Offer offer = new Offer();
		listOfWorkdays.add(past);
		listOfWorkdays.add(future);
		offer.setGuaranteedDays(listOfWorkdays);
		assertTrue(UseCaseDeleteFarm.hasGuaranteedDayInFuture(offer));
	}
}
