package de.ernteerfolg.service.uc.deletefarm;

import java.util.List;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;

class TasksAndOffers {
	List<Task> tasks;
	List<Offer> offers;
	
	Farm getFarmOfTask(int index) {
		return tasks.get(index).getFarm();
	}

	Helper getHelperOfOffer(int index) {
		return offers.get(index).getHelper();
	}
}
