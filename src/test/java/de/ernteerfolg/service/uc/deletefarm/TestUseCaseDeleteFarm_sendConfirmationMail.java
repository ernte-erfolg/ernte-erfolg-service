package de.ernteerfolg.service.uc.deletefarm;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.User;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.*;

class TestUseCaseDeleteFarm_sendConfirmationMail {

	@Captor
	private ArgumentCaptor<User> captor;

	private OutputPortUserNotification outputPortUserNotification;
	private OutputPortTaskStorage outputPortTaskStorage;
	private OutputPortAddressStorage outputPortAddressStorage; 
	private OutputPortFarmStorage outputPortFarmStorage;
	private OutputPortOfferStorage outputPortOfferStorage;
	private OutputPortUserStorage outputPortUserStorage;
	
	@BeforeEach
	public void before() {
		MockitoAnnotations.initMocks(this);
		
		outputPortUserNotification = Mockito.mock(OutputPortUserNotification.class);
		outputPortTaskStorage = Mockito.mock(OutputPortTaskStorage.class);
		outputPortAddressStorage = Mockito.mock(OutputPortAddressStorage.class); 
		outputPortFarmStorage = Mockito.mock(OutputPortFarmStorage.class);
		outputPortOfferStorage = Mockito.mock(OutputPortOfferStorage.class);
		outputPortUserStorage = Mockito.mock(OutputPortUserStorage.class);
	}	
	
	@Test
	void testSendConfirmationMail_success() {
		
		
		UseCaseDeleteFarm toTest = new UseCaseDeleteFarm(outputPortUserNotification, outputPortTaskStorage,
				outputPortAddressStorage, outputPortFarmStorage, outputPortOfferStorage, outputPortUserStorage);
		
		User savedUser = new User();
		savedUser.setLogin("savedUser");
				
		User user = new User();
		user.setLogin("user");
		
		when(outputPortFarmStorage.findOneByUserIsCurrentUser()).thenReturn(Optional.of(new Farm()));
		when(outputPortUserStorage.save(user)).thenReturn(savedUser);
		
		toTest.sendConfirmationMail(user);
		
		// Check that the notification was called
		verify(outputPortUserNotification, times(1)).notifyFarmAboutAccountDeletionLink(savedUser);

		// Check, that the deletionKey was specified	
		verify(outputPortUserStorage, times(1)).save(captor.capture());
		User userThatWasSaved = captor.getValue();
	    assertThat(userThatWasSaved.getDeletionKey(), is(notNullValue()));
	}

	@Test
	void testSendConfirmationMail_NULL() {
		
		UseCaseDeleteFarm toTest = new UseCaseDeleteFarm(outputPortUserNotification, outputPortTaskStorage,
				outputPortAddressStorage, outputPortFarmStorage, outputPortOfferStorage, outputPortUserStorage);
		
		toTest.sendConfirmationMail(null);
		
		// Check that the notification was NOT called
		verify(outputPortUserNotification, times(0)).notifyFarmAboutAccountDeletionLink(Mockito.any());

		// Check, that save was NOT called	
		verify(outputPortUserStorage, times(0)).save(Mockito.any());
	}

}
