package de.ernteerfolg.service.uc.deletefarm;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.domain.Workday;

class TestUseCaseDeleteFarm_confirmDeletion {

	@Captor
	private ArgumentCaptor<User> userCaptor;
	
	@Captor
	private ArgumentCaptor<Task> taskCaptor;

	private OutputPortUserNotification outputPortUserNotification;
	private OutputPortTaskStorage outputPortTaskStorage;
	private OutputPortAddressStorage outputPortAddressStorage; 
	private OutputPortFarmStorage outputPortFarmStorage;
	private OutputPortOfferStorage outputPortOfferStorage;
	private OutputPortUserStorage outputPortUserStorage;
	
	@BeforeEach
	public void before() {
		MockitoAnnotations.initMocks(this);
		
		outputPortUserNotification = Mockito.mock(OutputPortUserNotification.class);
		outputPortTaskStorage = Mockito.mock(OutputPortTaskStorage.class);
		outputPortAddressStorage = Mockito.mock(OutputPortAddressStorage.class); 
		outputPortFarmStorage = Mockito.mock(OutputPortFarmStorage.class);
		outputPortOfferStorage = Mockito.mock(OutputPortOfferStorage.class);
		outputPortUserStorage = Mockito.mock(OutputPortUserStorage.class);
	}	

	@Test
	public void testSendConfirmationMail_FarmWith2TaskAnd2OfferFromHelper1And1OfferFromHelper2() {
		testSendConfirmationMail(createFarmWith2TaskAnd2OfferFromHelper1And1OfferFromHelper2());
	}	
	
	@Test
	public void testSendConfirmationMail_FarmWith2TaskAnd1OfferFromHelper1() {
		testSendConfirmationMail(createFarmWith2TaskAnd1OfferFromHelper1());
	}	

	@Test
	public void testSendConfirmationMail_FarmWithTaskAndOneOfferFromHelper1() {
		testSendConfirmationMail(createFarmWithTaskAndOneOfferFromHelper1());
	}	

	@Test
	public void testSendConfirmationMail_FarmWithTaskAndWithoutOffer() {		
		TasksAndOffers tasksAndOffers = createFarmWithTaskAndOneOfferFromHelper1();
		tasksAndOffers.offers = null;
		testSendConfirmationMail(tasksAndOffers);
	}	

	@Test
	public void testSendConfirmationMail_FarmWithoutTask() {
		
		UseCaseDeleteFarm toTest = new UseCaseDeleteFarm(outputPortUserNotification, outputPortTaskStorage,
				outputPortAddressStorage, outputPortFarmStorage, outputPortOfferStorage, outputPortUserStorage);
		
		User user = new User();
		user.setLogin("user");
		when(outputPortFarmStorage.findOneByUserIsCurrentUser()).thenReturn(Optional.of(new Farm()));
		DeleteFarmState deleteFarmState = toTest.sendConfirmationMail(user);
		assertThat(deleteFarmState, is(DeleteFarmState.ok));
		
		verify(outputPortUserStorage, times(1)).save(userCaptor.capture());
		User userThatWasSaved = userCaptor.getValue();
		
		when(outputPortUserStorage.findOneByDeletionKey(userThatWasSaved.getDeletionKey())).thenReturn(Optional.of(userThatWasSaved));
		when(outputPortFarmStorage.findOneByUserIsCurrentUser()).thenReturn(Optional.empty());
				
		assertThat(toTest.confirmDeletion(userThatWasSaved.getDeletionKey()), is(DeleteFarmState.ok));	    
		verify(outputPortFarmStorage,times(2)).findOneByUserIsCurrentUser();
		verify(outputPortUserStorage,times(1)).deleteUserByDeletionKey(userThatWasSaved.getDeletionKey());
		verify(outputPortOfferStorage, times(0)).findByFarmId(Mockito.any());

		// notification for each helper
		verify(outputPortUserNotification, times(0)).notifyHelperAboutDeletedTask(Mockito.any(), Mockito.any());
	}
	
	void testSendConfirmationMail(TasksAndOffers tasksAndOffers) {
		
		UseCaseDeleteFarm toTest = new UseCaseDeleteFarm(outputPortUserNotification, outputPortTaskStorage,
				outputPortAddressStorage, outputPortFarmStorage, outputPortOfferStorage, outputPortUserStorage);
		
		User user = new User();
		user.setLogin("user");
		when(outputPortFarmStorage.findOneByUserIsCurrentUser()).thenReturn(Optional.of(new Farm()));
		DeleteFarmState deleteFarmState = toTest.sendConfirmationMail(user);
		assertThat(deleteFarmState, is(DeleteFarmState.ok));

		verify(outputPortUserStorage, times(1)).save(userCaptor.capture());
		
		User userThatWasSaved = userCaptor.getValue();
	    
		when(outputPortUserStorage.findOneByDeletionKey(userThatWasSaved.getDeletionKey())).thenReturn(Optional.of(userThatWasSaved));
		when(outputPortOfferStorage.findByFarmId(Mockito.any())).thenReturn(tasksAndOffers.offers);
		when(outputPortFarmStorage.findOneByUserIsCurrentUser()).thenReturn(Optional.of(tasksAndOffers.getFarmOfTask(0)));
		when(outputPortTaskStorage.findByFarmIsCurrentUser()).thenReturn(tasksAndOffers.tasks);
				
		assertThat(toTest.confirmDeletion(userThatWasSaved.getDeletionKey()), is(DeleteFarmState.ok));	    
		verify(outputPortFarmStorage,times(2)).findOneByUserIsCurrentUser();
		verify(outputPortUserStorage,times(1)).deleteUserByDeletionKey(userThatWasSaved.getDeletionKey());
		verify(outputPortOfferStorage, times(1)).findByFarmId(Mockito.any());

		// notification for each helper
		if(tasksAndOffers.offers != null && !tasksAndOffers.offers.isEmpty() ) {
			verify(outputPortUserNotification, times(1)).notifyHelperAboutDeletedTask(tasksAndOffers.getHelperOfOffer(0).getUser(), tasksAndOffers.offers.get(0));
		}
		
		// anonynisierung and farm deletion
		verify(outputPortAddressStorage,times(1)).delete(tasksAndOffers.tasks.get(0).getAddress());
		verify(outputPortTaskStorage,times(tasksAndOffers.tasks.size())).save(taskCaptor.capture());
		Task savedTask = taskCaptor.getValue();
		assertThat(savedTask.getTitle(), is("deleted task"));
		assertThat(savedTask.getHowToFindUs(), is(nullValue()));
		verify(outputPortFarmStorage, times(1)).delete(Mockito.any());
	}
	
	// farm mit task und einem offer von helfer1
	private TasksAndOffers createFarmWithTaskAndOneOfferFromHelper1() {
		User userHans = new User();
		
		Helper helperHans = new Helper();
		helperHans.setFirstname("Hans");
		helperHans.setUser(userHans);
		
		Workday tomorrow = new Workday();
		tomorrow.setDate(LocalDate.now().plusDays(1));
		
		Set<Workday> guaranteddDaysOfHans = new HashSet<>();
		guaranteddDaysOfHans.add(tomorrow);

		List<Offer> offers = new ArrayList<>();
		Offer offerOfHans = new Offer();
		offerOfHans.setHelper(helperHans);
		offerOfHans.setGuaranteedDays(guaranteddDaysOfHans);
		offers.add(offerOfHans);
		
		Farm farmSonnenhof = new Farm();
		farmSonnenhof.setName("Sonnenhof");
		
		Task taskOfSonnenhof = new Task();
		taskOfSonnenhof.setAddress(new Address());
		taskOfSonnenhof.setFarm(farmSonnenhof);
						
		List<Task> tasksOfFarm = new ArrayList<>();
		tasksOfFarm.add(taskOfSonnenhof);
		
		TasksAndOffers tasksAndOffers = new TasksAndOffers();
		tasksAndOffers.tasks = tasksOfFarm; 
		tasksAndOffers.offers = offers;
		return tasksAndOffers;
	}
	
	// farm mit 2 task und zwei offer von helfer1	
	private TasksAndOffers createFarmWith2TaskAnd1OfferFromHelper1() {
		TasksAndOffers tasksAndOffers = createFarmWithTaskAndOneOfferFromHelper1();

		Farm farmMondhof = new Farm();
		farmMondhof.setName("Mondhof");
		
		Task taskOfMondhof = new Task();
		taskOfMondhof.setAddress(new Address());
		taskOfMondhof.setFarm(farmMondhof);
		
		tasksAndOffers.tasks.add(taskOfMondhof);

		Offer anotherOfferOfHans = new Offer();
		anotherOfferOfHans.setHelper(tasksAndOffers.offers.get(0).getHelper());
		anotherOfferOfHans.setGuaranteedDays(tasksAndOffers.offers.get(0).getGuaranteedDays());

		tasksAndOffers.offers.add(anotherOfferOfHans);
				
		return tasksAndOffers;
	}
	
	//	farm mit 2 task und 2 offer von helfer1 und 1 offer von helfer2
	private TasksAndOffers createFarmWith2TaskAnd2OfferFromHelper1And1OfferFromHelper2() {
		TasksAndOffers tasksAndOffers = createFarmWith2TaskAnd1OfferFromHelper1();

		User userMax = new User();
		
		Helper helperMax = new Helper();
		helperMax.setFirstname("Max");
		helperMax.setUser(userMax);

		Workday dayAfterTomorrow = new Workday();
		dayAfterTomorrow.setDate(LocalDate.now().plusDays(2));
		
		Set<Workday> guaranteddDaysOfMax = new HashSet<>();
		guaranteddDaysOfMax.add(dayAfterTomorrow);

		Offer offer = new Offer();
		offer.setHelper(helperMax);
		offer.setGuaranteedDays(guaranteddDaysOfMax);
		
		tasksAndOffers.offers.add(offer);
		
		return tasksAndOffers;
	}
}


