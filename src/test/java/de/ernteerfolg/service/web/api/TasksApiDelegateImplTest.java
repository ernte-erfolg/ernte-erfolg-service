package de.ernteerfolg.service.web.api;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
@SpringBootTest(classes = ErnteErfolgServiceApp.class)
class TasksApiDelegateImplTest
{
    WorkdayPayload future = new WorkdayPayload().date(LocalDate.now().plus(Period.ofDays(2)));
    WorkdayPayload past = new WorkdayPayload().date(LocalDate.now().minus(Period.ofDays(2)));
    ArrayList<WorkdayPayload> listOfWorkdays;

    @Autowired
    TasksApiDelegateImpl tasksApiDelegate;

    @BeforeEach
    void setUp()
    {
        listOfWorkdays = new ArrayList<>();
    }

    @Test
    void workingdayInThePastIsIncluded_Future()
    {
        listOfWorkdays.add(future);
        Assertions.assertFalse(tasksApiDelegate.workingdayInThePastIsIncluded(null,listOfWorkdays));
    }

    @Test
    void workingdayInThePastIsIncluded_Past()
    {
        listOfWorkdays.add(past);
        Assertions.assertTrue(tasksApiDelegate.workingdayInThePastIsIncluded(null,listOfWorkdays));
    }

    @Test
    void workingdayInThePastIsIncluded_Mixed()
    {
        listOfWorkdays.add(future);
        listOfWorkdays.add(past);
        Assertions.assertTrue(tasksApiDelegate.workingdayInThePastIsIncluded(null,listOfWorkdays));
    }

    @Test
    void workingdayInThePastIsIncluded_Empty()
    {
        Assertions.assertFalse(tasksApiDelegate.workingdayInThePastIsIncluded(null,listOfWorkdays));
    }
}
