package de.ernteerfolg.service.web.rest.Routinen;

import com.neovisionaries.i18n.LanguageCode;
import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.repository.AddressRepository;
import de.ernteerfolg.service.repository.FarmRepository;
import de.ernteerfolg.service.repository.HelperRepository;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.repository.UserRepository;
import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.service.ValidationException;
import de.ernteerfolg.service.service.dto.UserDTO;
import de.ernteerfolg.service.uc.offermakeorupdate.InputPortMakeOrUpdateAnOffer;
import de.ernteerfolg.service.uc.offermakeorupdate.OutputPortOfferStorage;
import de.ernteerfolg.service.uc.offermakeorupdate.OutputPortWorkdayStorage;
import de.ernteerfolg.service.web.rest.errors.NotFoundAlertException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class GenerateEntities {


    private static final String TASK_MAIL = "farmer@trash-mail.com";
    private static final String TASK_DESCRIPTION = "farm_description";
    private static final String TASK_HOW_TO_FIND_US = "howToFindUs";
    private static final String TASK_PHONE = "+49 98 345 125637";
    private static final String TASK_TITLE = "Farm Titel";

    private static final String[] TASK_REQUESTED_DAY_DATE = {
        LocalDate.now().toString(),
        LocalDate.now().plusDays(1).toString(),
        LocalDate.now().plusDays(2).toString(),
        LocalDate.now().plusDays(3).toString(),
        LocalDate.now().plusDays(4).toString()};
    private static final int[] TASK_REQUESTED_DAY_HOURS = {26, 30, 30, 30, 30};

    private static final String[] TASK_GUARANTEED_AY_DATE = {
        LocalDate.now().toString(),
        LocalDate.now().plusDays(2).toString(),
        LocalDate.now().plusDays(4).toString()};
    private static final int[] TASK_GUARANTEED_AY_HOURS = {5, 3, 7};

    private static final String STREET = "Wilhelmstraße";
    private static final String HOUS_NR = "54";
    private static final String ZIP = "10117";
    private static final String CITY = "Berlin";
    private static final BigDecimal LAT_ADDRESS = BigDecimal.valueOf(52.513611);
    private static final BigDecimal LONG_ADDRESS = BigDecimal.valueOf(13.382500);


    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private FarmRepository farmRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OutputPortOfferStorage offerRepository;

    @Autowired
    private OutputPortWorkdayStorage workdayRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private InputPortMakeOrUpdateAnOffer useCaseMakeAnOffer;


    @Autowired
    private HelperRepository helperRepository;


    public static String[] getTaskGuaranteedAyDate() {
        return TASK_GUARANTEED_AY_DATE;
    }

    public static int[] getTaskGuaranteedAyHours() {
        return TASK_GUARANTEED_AY_HOURS;
    }

    public Offer createAndSaveOfferForTaskAndHelper(Task task, Helper helper)
        throws ValidationException {
        return useCaseMakeAnOffer.makeAnOffer(helper, getGuaranteedDaysAsWorkdaySet(), task.getId()).getOffer().get();
    }

    public Offer createAndSaveOfferForTaskAndHelper(Task task, Helper helper,
        Set<Workday> guaranteeWorkdays)
        throws ValidationException {
        Offer offer = new Offer();
        offer.setGuaranteedDays(guaranteeWorkdays);
        offer.setHelper(helper);
        offer.setTask(task);
        Offer savedOffer = offerRepository.save(offer);

        for (Workday workday : guaranteeWorkdays) {
            workday.setOffer(offer);
            Workday savedWorkday = workdayRepository.save(workday);
        }
        return savedOffer;
    }

    public Helper createAndSaveHelperForCurrentUser() {
        Helper helper = new Helper();
        helper.setUser(getCurrentUser());
        helper = helperRepository.save(helper);
        return helper;
    }

    public Helper createAndSaveHelperForNewUser() {
        return createAndSaveHelperForNewUserPrefixed("H");
    }

    public Helper createAndSaveHelperForNewUserPrefixed(String prefix) {
        User user = createUser(prefix, "HELPER");
        Helper helper = new Helper();
        helper.setUser(user);
        helper = helperRepository.save(helper);
        return helper;
    }

    public Address createAddress() {
        Address address = new Address();
        address.setStreet(STREET);
        address.setHouseNr(HOUS_NR);
        address.setZip(ZIP);
        address.setCity(CITY);
        address.setLatitude(LAT_ADDRESS);
        address.setLongitude(LONG_ADDRESS);
        return address;
    }

    public Address createAndSaveAddress() {
        return addressRepository.save(createAddress());
    }

    public Farm createFarm() {
        Farm farm = new Farm();
        farm.setAdditionalInformation("additionalInformation");
        farm.setContactEmail("contact@email.de");
        farm.setName("MyFarm");
        farm.setPhone("+49 89 726538");
        return farm;
    }

    public Farm createAndSaveFarmForCurrentUser() {
        Address address = createAndSaveAddress();
        Farm farm = createFarm();
        farm.setAddress(address);
        farm.setUser(getCurrentUser());
        farm = farmRepository.save(farm);
        return farm;
    }

    public Farm createFarmAndNewUser() {
        User userFarm = createUser("F", "FARM");
        Address address = createAndSaveAddress();
        Farm farm = createFarm();
        farm.setAddress(address);
        farm.setUser(userFarm);
        farm = farmRepository.save(farm);
        return farm;
    }

    public Farm createAndSaveFarm() {
        return farmRepository.save(createAndSaveFarmForCurrentUser());
    }

    public Task createAndSaveTaskWithUser() {
        return createAndSaveTask(createAndSaveAddress(), createFarmAndNewUser());
    }

    public Task createTask() {
        return createTask(createAndSaveAddress(), createAndSaveFarmForCurrentUser());
    }

    public Task createTask(Address address, Farm farm) {
        Task task = new Task();
        task.setFarm(farm);
        task.setAddress(address);
        task.setContactEmail(TASK_MAIL);
        task.setDescription(TASK_DESCRIPTION);
        task.setHowToFindUs(TASK_HOW_TO_FIND_US);
        task.setPhone(TASK_PHONE);
        task.setTitle(TASK_TITLE);
        task.setActive(true);

        task.setRequestedDays(getRequestedDaysAsWorkdaySet());
        return task;
    }

    public Set<Workday> getRequestedDaysAsWorkdaySet() {
        return getDaysAsWorkdaySet(TASK_REQUESTED_DAY_DATE, TASK_REQUESTED_DAY_HOURS);
    }

    public Set<Workday> getGuaranteedDaysAsWorkdaySet() {
        return getDaysAsWorkdaySet(TASK_GUARANTEED_AY_DATE, TASK_GUARANTEED_AY_HOURS);
    }

    public Set<Workday> getDaysAsWorkdaySet(String[] days, int[] hours) {
        Set<Workday> requestedDays = new HashSet<>();
        for (int i = 0; i < days.length; i++) {
            Workday wd = new Workday();
            wd.setDate(LocalDate.parse(days[i]));
            wd.setWorkingHours(hours[i]);
            requestedDays.add(wd);
        }
        return requestedDays;
    }

    public Task createAndSaveTask() {
        return createAndSaveTask(createAndSaveAddress(), createAndSaveFarm());
    }

    public Task createAndSaveTask(Address address, Farm farm) {
        Task savedTask = taskRepository.save(createTask(address, farm));

        Set<Workday> savedRequestedDaysSet = new HashSet<>();

        savedTask.getRequestedDays().stream().forEach(
            requestedDays ->
            {
                requestedDays.setTask(savedTask);
                savedRequestedDaysSet
                    .add((Workday) ((JpaRepository) workdayRepository).save(requestedDays));
            }
        );
        savedTask.setRequestedDays(savedRequestedDaysSet);

        return savedTask;
    }

    public User createUser(String prefix, String role) {
        UserDTO user = new UserDTO();
        user.setLogin(prefix + "dummy");
        user.setActivated(true);
        user.setEmail(prefix + "dummy@gmail.de");
        user.setFirstName(prefix + "dummyName");
        user.setLastName(prefix + "dummyLastName");
        user.setImageUrl("");
        user.setLangKey(LanguageCode.de);
        user.setAuthorities(Collections.singleton(role));
        User userCreated = userService.createUser(user);
        return userCreated;
    }


    private User getCurrentUser() {
        Optional<String> userEmail = SecurityUtils.getCurrentUserLogin();
        Optional<User> userOptional = userRepository.findOneByLogin(userEmail.get());

        if (!userOptional.isPresent()) {
            throw new NotFoundAlertException("There is no current user logged in !?", "User",
                "userNotFouund");
        }
        User user = userOptional.get();
        return user;
    }


}
