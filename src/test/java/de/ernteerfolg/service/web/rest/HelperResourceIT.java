package de.ernteerfolg.service.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.repository.AddressRepository;
import de.ernteerfolg.service.repository.FarmRepository;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.repository.UserRepository;
import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.uc.offermakeorupdate.InputPortMakeOrUpdateAnOffer;
import de.ernteerfolg.service.web.api.model.CreateHelperPayload;

/**
 * Integration tests for the {@link UserExtendResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class HelperResourceIT {

	private static final String FIRSTNAME = "Max";
	private static final String LASTNAME = "Mustermann";
	private static final String PHONE = "12345 125637";
	private static final String STREET = "Musterstraße";
	private static final String HOUS_NR = "3";
	private static final String ZIP = "81829";
	private static final String CITY = "Musterort";

	@Autowired
    private de.ernteerfolg.service.repository.HelperRepository helperRepository;

    @Autowired
    private MockMvc restUserExtendMockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private FarmRepository farmRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private InputPortMakeOrUpdateAnOffer useCaseMakeAnOffer;

    @Autowired
    UserService userService;

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void findHelperByUserIsCurrentUserNotExists() throws Exception {
     	restUserExtendMockMvc.perform(get("/api/helpers/byUserIsCurrentUser")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void getHelperByIdAsUserWithRoleHelper() throws Exception {
        restUserExtendMockMvc.perform(get("/api/helpers/3")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void getHelperByIdWithoutOfferAsUserWithRoleFarm() throws Exception {

        Helper helperWithoutOffer = new Helper();
        helperWithoutOffer = helperRepository.save(helperWithoutOffer);

        Helper helperWithOffer = new Helper();
        helperWithOffer = helperRepository.save(helperWithOffer);

		User user = userRepository
				.findOneByLogin(SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new IllegalStateException()))
				.orElseThrow(() -> new IllegalStateException());

        Farm farm = new Farm();
        farm.setUser(user);
        farm = farmRepository.save(farm);

        Task task = new Task();
        task.setFarm(farm);
        taskRepository.save(task);

        useCaseMakeAnOffer.makeAnOffer(helperWithOffer, new HashSet<>(), task.getId());

		restUserExtendMockMvc.perform(get("/api/helpers/" + helperWithoutOffer.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void getHelperByIdWithOfferAsUserWithRoleFarm() throws Exception {

        Helper helper = new Helper();
        helper = helperRepository.save(helper);

		User user = userRepository
				.findOneByLogin(SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new IllegalStateException()))
				.orElseThrow(() -> new IllegalStateException());

        Farm farm = new Farm();
        farm.setUser(user);
        farm = farmRepository.save(farm);

        Task task = new Task();
        task.setFarm(farm);
        task.setActive(true);
        taskRepository.save(task);

        useCaseMakeAnOffer.makeAnOffer(helper, new HashSet<>(), task.getId());

		restUserExtendMockMvc.perform(get("/api/helpers/" + helper.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
	}

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void findHelperByUserIsCurrentUser() throws Exception {

    	Optional<String> userEmail = SecurityUtils.getCurrentUserLogin();
    	System.out.println("login: " + userEmail);
    	Optional<User> userOptional = userRepository.findOneByLogin(userEmail.get());

    	User user = userOptional.get();
        System.out.println("userID: " + user.getId());
        System.out.println("userLogin: " + user.getLogin());
        System.out.println("userMail: " + user.getEmail());

        Address address = new Address();
        address.setStreet(STREET);
        address.setHouseNr(HOUS_NR);
        address.setZip(ZIP);
        address.setCity(CITY);
        address = addressRepository.save(address);

        Helper helper = new Helper();
        helper.setAddress(address);
        helper.setUser(user);
        helper.setFirstname(FIRSTNAME);
        helper.setLastname(LASTNAME);
     	helper.setPhone(PHONE);

     	helper = helperRepository.save(helper);

     	System.out.println("helper saved: " +  helperRepository.getOne(helper.getId()));

     	restUserExtendMockMvc.perform(get("/api/helpers/byUserIsCurrentUser")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void createHelperAsUserWithRoleHelper() throws Exception {
        int databaseSizeBeforeCreate = helperRepository.findAll().size();

        CreateHelperPayload createHelperPayload = buildHelperPayload();

        restUserExtendMockMvc.perform(post("/api/helpers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").isNotEmpty())
	    	.andExpect(jsonPath("$.firstName").value(FIRSTNAME))
	    	.andExpect(jsonPath("$.lastName").value(LASTNAME))
	    	.andExpect(jsonPath("$.phone").value(PHONE))
	    	.andExpect(jsonPath("$.street").doesNotExist())
	    	.andExpect(jsonPath("$.houseNr").doesNotExist())
	    	.andExpect(jsonPath("$.zip").value(ZIP))
	    	.andExpect(jsonPath("$.city").doesNotExist())
	    	.andExpect(jsonPath("$.lat").isNotEmpty())
	    	.andExpect(jsonPath("$.lon").isNotEmpty())
	    	.andExpect(jsonPath("$.level").isEmpty())
	    	.andExpect(jsonPath("$.levelProgress").isEmpty())
            ;

	    int databaseSizeAfterCreate = helperRepository.findAll().size();

	    assertThat(databaseSizeBeforeCreate+1 == databaseSizeAfterCreate);
    }

	@Test
	@Transactional
	@WithMockUser(roles="FARM", username="user")
	public void createHelperAsUserWithRoleFarm() throws Exception {

	    CreateHelperPayload createHelperPayload = buildHelperPayload();

	    restUserExtendMockMvc.perform(post("/api/helpers")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
	        .andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles="HELPER", username="user")
	public void reCreateHelperAsUserWithRoleHelper() throws Exception {

	    CreateHelperPayload createHelperPayload = buildHelperPayload();

	    restUserExtendMockMvc.perform(post("/api/helpers")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
	        .andExpect(status().isCreated());

        // Do it again should be a BAD_REQUEST
        restUserExtendMockMvc.perform(post("/api/helpers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
                .andExpect(status().isBadRequest())
    	    	.andExpect(jsonPath("$.title").value("User is already a helper!"));
	}

	@Test
	@Transactional
	@WithMockUser(roles="HELPER", username="user")
	public void updateHelperAsUserWithRoleHelper() throws Exception {

        Helper helper = new Helper();
        helper.setUser(userService.getExpectedUserWithAuthorities());
        helper = helperRepository.save(helper);

   		CreateHelperPayload createHelperPayload = buildHelperPayload();

		restUserExtendMockMvc.perform(put("/api/helpers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
    	    	.andExpect(jsonPath("$.firstName").value(FIRSTNAME))
    	    	.andExpect(jsonPath("$.lastName").value(LASTNAME))
    	    	.andExpect(jsonPath("$.phone").value(PHONE))
    	    	.andExpect(jsonPath("$.street").doesNotExist())
    	    	.andExpect(jsonPath("$.houseNr").doesNotExist())
    	    	.andExpect(jsonPath("$.zip").value(ZIP))
    	    	.andExpect(jsonPath("$.city").doesNotExist())
    	    	.andExpect(jsonPath("$.lat").isNotEmpty())
    	    	.andExpect(jsonPath("$.lon").isNotEmpty())
    	    	.andExpect(jsonPath("$.level").isEmpty())
    	    	.andExpect(jsonPath("$.levelProgress").isEmpty())
                ;
	}

	@Test
	@Transactional
	@WithMockUser(roles="FARM,HELPER", username="user")
	public void createHelperAsUserWithRoleFarmAndHelper() throws Exception {

	    CreateHelperPayload createHelperPayload = buildHelperPayload();

	    restUserExtendMockMvc.perform(post("/api/helpers")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
	        .andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles="", username="user")
	public void createHelperAsUserWithNoRole() throws Exception {

	    CreateHelperPayload createHelperPayload = buildHelperPayload();

	    restUserExtendMockMvc.perform(post("/api/helpers")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createHelperPayload)))
	        .andExpect(status().isForbidden());
	}

	private CreateHelperPayload buildHelperPayload() {
		CreateHelperPayload createHelperPayload = new CreateHelperPayload();
        createHelperPayload.setFirstName(FIRSTNAME);
        createHelperPayload.setLastName(LASTNAME);
     	createHelperPayload.setZip(ZIP);
     	createHelperPayload.setPhone(PHONE);
		return createHelperPayload;
	}
}
