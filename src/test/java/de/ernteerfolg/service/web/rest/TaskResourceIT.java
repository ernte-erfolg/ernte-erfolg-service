package de.ernteerfolg.service.web.rest;


import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.repository.*;
import de.ernteerfolg.service.web.api.model.CreateTaskPayload;
import de.ernteerfolg.service.web.api.model.TaskPayload;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import de.ernteerfolg.service.web.rest.Routinen.GenerateEntities;
import java.util.Set;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.anEmptyMap;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser(roles="FARM", username="user")
public class TaskResourceIT {

    @Autowired
    private MockMvc restUserExtendMockMvc;

    @Autowired
    private WorkdayRepository workdayRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private GenerateEntities generateEntities;

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void createTaskWrongPlzAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        createTaskPayload.setZip("11111");
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.entityName").value("Address"))
            .andExpect(jsonPath("$.errorKey").value("unknownZip"))
            .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
           	.andExpect(jsonPath("$.title").value("Diese Postleitzahl ist uns nicht bekannt."))
           	.andExpect(jsonPath("$.status").value(400))
           	.andExpect(jsonPath("$.message").value("error.unknownZip"))
           	.andExpect(jsonPath("$.params").value("Address"));
    }

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void createTaskAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload =  createTaskPayload(task);
        int vorPostTask = taskRepository.findAll().size();
        int vorPostWorkday = workdayRepository.findAll().size();
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(task.getPhone()))
            .andExpect(jsonPath("$.email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(0).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(0).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(2).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(2).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(3).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(3).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(4).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(4).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(1).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(1).getWorkingHours())));

        List<Task> farmList = taskRepository.findAll();
        assertThat(farmList).hasSize(vorPostTask + 1);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday + task.getRequestedDays().size());

    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void updateTaskAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        int vorPostWorkday = workdayRepository.findAll().size();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        createTaskPayload .addRequestedDaysItem(new WorkdayPayload().date(LocalDate.now().plusDays(12)).workingHours(12));
        int vorPostTask = taskRepository.findAll().size();
        ArrayList<WorkdayPayload> taskRequestedDays = new ArrayList<>(createTaskPayload.getRequestedDays());
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(task.getPhone()))
            .andExpect(jsonPath("$.email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(0).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(0).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(2).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(2).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(3).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(3).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(4).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(4).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(5).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(5).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(1).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(1).getWorkingHours())));

        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday + task.getRequestedDays().size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void updateTaskAsUserWithRoleFarmAndDaysInPast() throws Exception {

        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        int vorPostWorkday = workdayRepository.findAll().size();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        createTaskPayload .addRequestedDaysItem(new WorkdayPayload().date(LocalDate.now().minusDays(2)).workingHours(12));
        int vorPostTask = taskRepository.findAll().size();
        ArrayList<WorkdayPayload> taskRequestedDays = new ArrayList<>(createTaskPayload.getRequestedDays());
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isBadRequest());


        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday);
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void updateForeignTaskAsUserWithRoleFarm() throws Exception {
        Task task = generateEntities.createAndSaveTaskWithUser();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        generateEntities.createAndSaveFarmForCurrentUser();

        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void updateTaskAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        task.addRequestedDays(new Workday().date(LocalDate.of(2014, 12, 31)).workingHours(12));
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ANONYMOUS", username = "user")
    public void updateTaskAsUserWithRoleAnonymous() throws Exception {
        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        task.addRequestedDays(new Workday().date(LocalDate.of(2014, 12, 31)).workingHours(12));
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ADMIN", username = "user")
    public void updateTaskAsUserWithRoleAdmin() throws Exception {
        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        task.addRequestedDays(new Workday().date(LocalDate.of(2014, 12, 31)).workingHours(12));
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        restUserExtendMockMvc.perform(put("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void updateTaskAsUserWithRoleWithoutId() throws Exception {

        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        CreateTaskPayload createTaskPayload = createTaskPayload(task);

        restUserExtendMockMvc.perform(put("/api/tasks/" + Long.MAX_VALUE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isBadRequest());
    }


    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void createTaskAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        int vorPostTask = taskRepository.findAll().size();
        int vorPostWorkday = workdayRepository.findAll().size();
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());

        List<Task> farmList = taskRepository.findAll();
        assertThat(farmList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday);
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ADMIN", username = "user")
    public void createTaskAsUserWithRoleAdmin() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        int vorPostTask = taskRepository.findAll().size();
        int vorPostWorkday = workdayRepository.findAll().size();
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());

        List<Task> farmList = taskRepository.findAll();
        assertThat(farmList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday);
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ANONYMOUS", username = "user")
    public void createTaskAsUserWithRoleAnonymous() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        int vorPostTask = taskRepository.findAll().size();
        int vorPostWorkday = workdayRepository.findAll().size();
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isForbidden());

        List<Task> farmList = taskRepository.findAll();
        assertThat(farmList).hasSize(vorPostTask);

        List<Workday> workdayList = workdayRepository.findAll();
        assertThat(workdayList).hasSize(vorPostWorkday);
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getOwnTaskAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(task.getPhone()))
            .andExpect(jsonPath("$.email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.lat").isNotEmpty())
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(0).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(0).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(2).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(2).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(3).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(3).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(4).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(4).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(1).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(1).getWorkingHours())));
    }


    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getTaskOfForeignFarmAsUserWithRoleFarm() throws Exception {
        Task task = generateEntities.createAndSaveTaskWithUser();
        generateEntities.createAndSaveFarmForCurrentUser();

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }


    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void getTaskAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createTask();
        task = taskRepository.save(task);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(""))
            .andExpect(jsonPath("$.email").value(""))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.lat").isNotEmpty())
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(0).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(0).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(2).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(2).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(3).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(3).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(4).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(4).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(taskRequestedDays.get(1).getDate().toString())))
            .andExpect(jsonPath("$.requestedDays.[*].workingHours")
                .value(hasItem(taskRequestedDays.get(1).getWorkingHours())))
            .andExpect(jsonPath("$.helpers", is(empty())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper", is(anEmptyMap())));
    }

    @Test
    @Transactional
    @WithUnauthenticatedMockUser
    public void getTaskAsUserWithRoleAnnoymus() throws Exception {

        Task task = generateEntities.createAndSaveTaskWithUser();
        task = taskRepository.save(task);
        ArrayList<Workday> taskRequestedDays = new ArrayList<>(task.getRequestedDays());

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value(""))
            .andExpect(jsonPath("$.email").value(""))
            .andExpect(jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.lat").isNotEmpty())
            .andExpect(jsonPath("$.helpers", is(empty())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper", is(anEmptyMap())));
    }


    private CreateTaskPayload createTaskPayload(Task task) {
        return new CreateTaskPayload()
            .title(task.getTitle())
            .taskDescription(task.getDescription())
            .howToFindUs(task.getHowToFindUs())
            .phone(task.getPhone())
            .email(task.getContactEmail())
            .houseNr(task.getAddress().getHouseNr())
            .street(task.getAddress().getStreet())
            .zip(task.getAddress().getZip())
            .city(task.getAddress().getCity())
            .requestedDays(
                new ArrayList<WorkdayPayload>
                    (task.getRequestedDays()
                        .stream()
                        .map(
                            workday -> {
                return new WorkdayPayload().workingHours(workday.getWorkingHours()).date(workday.getDate());
            }).collect(Collectors.toList()))
            );
    }

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void createTaskWithHowToFindUsAndDescriptionLong() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload =  createTaskPayload(task);
        createTaskPayload.setHowToFindUs(RandomStringUtils.random(3000,true,true));
        createTaskPayload.setTaskDescription(RandomStringUtils.random(3000,true,true));
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.taskDescription").value(createTaskPayload.getTaskDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(createTaskPayload.getHowToFindUs()))
            .andExpect(descriptionInDatabase(createTaskPayload.getTaskDescription(),createTaskPayload.getHowToFindUs()));
    }

    private ResultMatcher descriptionInDatabase(String description, String howtoFindUs) {
        return mvcResult -> {
                String json = mvcResult.getResponse().getContentAsString();
            TaskPayload farmPayload = (TaskPayload) TestUtil.convertJSONStringToObject(json, TaskPayload.class);
            assertThat(taskRepository.getOne(farmPayload.getId()).getHowToFindUs())
                .isEqualTo(howtoFindUs);
            assertThat(taskRepository.getOne(farmPayload.getId()).getDescription())
                .isEqualTo(description);
        };
    }


    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void createTaskWithHowToFindUsToLong() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload =  createTaskPayload(task);
        createTaskPayload.setHowToFindUs(RandomStringUtils.random(3030,true,true));
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/constraint-violation"))
            .andExpect(jsonPath("$.title").value("Method argument not valid"))
            .andExpect(jsonPath("$.status").value(400))
            .andExpect(jsonPath("$.message").value("error.validation"))
            .andExpect(jsonPath("$.path").value("/api/tasks"))
            .andExpect(jsonPath("$.fieldErrors[0].objectName").value("createTaskPayload"))
            .andExpect(jsonPath("$.fieldErrors[0].field").value("howToFindUs"))
            .andExpect(jsonPath("$.fieldErrors[0].message").value("Size"));

    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void createTaskWithDescriptionToLong() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        createTaskPayload.setTaskDescription(RandomStringUtils.random(3040, true, true));
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(
                jsonPath("$.type").value("https://www.jhipster.tech/problem/constraint-violation"))
            .andExpect(jsonPath("$.title").value("Method argument not valid"))
            .andExpect(jsonPath("$.status").value(400))
            .andExpect(jsonPath("$.message").value("error.validation"))
            .andExpect(jsonPath("$.path").value("/api/tasks"))
            .andExpect(jsonPath("$.fieldErrors[0].objectName").value("createTaskPayload"))
            .andExpect(jsonPath("$.fieldErrors[0].field").value("taskDescription"))
            .andExpect(jsonPath("$.fieldErrors[0].message").value("Size"));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ADMIN", username = "user")
    public void getTaskByFarmAsUserWithRoleAdmin() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getOwnTasksByFarmAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(task.getId().intValue())))
            .andExpect(jsonPath("$.[*].farmId").value(hasItem(task.getFarm().getId().intValue())))
        ;
    }


    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getNoOwnTasksByFarmAsUserWithRoleFarm() throws Exception {

        Task taskOfForeignFarm = generateEntities.createAndSaveTaskWithUser();
        Farm farmOfCurrentUser = generateEntities.createAndSaveFarmForCurrentUser();

        restUserExtendMockMvc
            .perform(get("/api/tasks/byFarm/" + farmOfCurrentUser.getId())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string("[]"));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getForeignTaskByFarmAsUserWithRoleFarm() throws Exception {

        Task task = generateEntities.createAndSaveTaskWithUser();
        generateEntities.createAndSaveFarmForCurrentUser();
        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }


    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void getTasksByFarmAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        restUserExtendMockMvc.perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(task.getId().intValue())))
            .andExpect(jsonPath("$.[*].farmId").value(hasItem(task.getFarm().getId().intValue())))
            .andExpect(jsonPath("$.[0].helpers", is(empty())))
            .andExpect(jsonPath("$.[0].guaranteedDaysByHelper", is(anEmptyMap())));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ANONYMOUS", username = "user")
    public void getTasksByFarmAsUserWithRoleAnonymous() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        restUserExtendMockMvc
            .perform(get("/api/tasks/byFarm/" + task.getFarm().getId())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(task.getId().intValue())))
            .andExpect(jsonPath("$.[*].farmId").value(hasItem(task.getFarm().getId().intValue())))
            .andExpect(jsonPath("$.[0].helpers", is(empty())))
            .andExpect(jsonPath("$.[0].guaranteedDaysByHelper", is(anEmptyMap())));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void searchTasksAsUserWithRoleFarm() throws Exception {
        restUserExtendMockMvc.perform(get("/api/tasks/search?zip=12345")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ADMIN", username = "user")
    public void searchTasksAsUserWithRoleAdmin() throws Exception {

        restUserExtendMockMvc.perform(get("/api/tasks/search?zip=12345")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }


    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void searchForeignTasksAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        restUserExtendMockMvc
            .perform(get("/api/tasks/search?zip=" + task.getAddress().getZip())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].tasks.[0].phone").value(""))
            .andExpect(jsonPath("$.[0].tasks.[0].email").value(""))
            .andExpect(jsonPath("$.[*].maxDistance").exists());
    }


    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void searchOwnTasksAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        restUserExtendMockMvc
            .perform(get("/api/tasks/search?zip=" + task.getAddress().getZip())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].tasks.[0].phone").value(task.getPhone()))
            .andExpect(jsonPath("$.[0].tasks.[0].email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.[*].maxDistance").exists());
    }


    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void searchOldTasksAsUserWithRoleHelper() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        Set<Workday> requestedDaysAsWorkdaySet = generateEntities.getRequestedDaysAsWorkdaySet();
        LocalDate maxDate = requestedDaysAsWorkdaySet.stream().map(Workday::getDate)
            .max(LocalDate::compareTo).get();
        maxDate = maxDate.plusDays(1);

        restUserExtendMockMvc
            .perform(
                get("/api/tasks/search?withWorkingDaysGreaterThen=" + maxDate + "&zip=" + task
                    .getAddress()
                    .getZip())
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", is(empty())));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ANONYMOUS", username = "user")
    public void searchTasksAsUserWithRoleAnonymous() throws Exception {

        Task task = generateEntities.createAndSaveTask();
        Helper helper = generateEntities.createAndSaveHelperForNewUser();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        restUserExtendMockMvc
            .perform(get("/api/tasks/search?zip=" + task.getAddress().getZip())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[0].tasks.[0].phone").value(task.getPhone()))
            .andExpect(jsonPath("$.[0].tasks.[0].email").value(task.getContactEmail()))
            .andExpect(jsonPath("$.[*].maxDistance").exists());
    }
    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void tryToCreateTaskWithDaysInPast() throws Exception {

        Task task = generateEntities.createTask();
        CreateTaskPayload createTaskPayload = createTaskPayload(task);
        createTaskPayload.addRequestedDaysItem(
            new WorkdayPayload().date(LocalDate.now().minusDays(90)).workingHours(8));
        restUserExtendMockMvc.perform(post("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createTaskPayload)))
            .andExpect(jsonPath("$.title")
                .value("Some Workday lays in the past, only workdays in future can be created"))
            .andExpect(jsonPath("$.status").value(400))
            .andExpect(jsonPath("$.message").value("error.validation.dayInPast"))
            .andExpect(jsonPath("$.errorKey").value("validation.dayInPast"))
            .andExpect(jsonPath("$.fieldName").value("createTaskPayload.workdayPayload.date"));
    }

    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getNoOffersFromOldTaskAsUserWithRoleFarm() throws Exception {

        String[] TASK_REQUESTED_DAY_DATE_OLD = {"2020-01-24", "2020-01-26",
            "2020-01-28", "2020-01-30", "2020-02-02"};
        int[] TASK_REQUESTED_DAY_HOURS = {6, 8, 10, 12, 14};

        Helper helper1 = generateEntities.createAndSaveHelperForNewUser();
        Farm farm = generateEntities.createAndSaveFarmForCurrentUser();
        Task task = generateEntities.createTask(farm.getAddress(), farm);
        task.setRequestedDays(generateEntities
            .getDaysAsWorkdaySet(TASK_REQUESTED_DAY_DATE_OLD, TASK_REQUESTED_DAY_HOURS));

        taskRepository.save(task);

        String[] TASK_GUARANTEED_AY_DATE_OLD = {"2020-01-24", "2020-01-26",
            "2020-01-30"};
        int[] TASK_GUARANTEED_AY_HOURS = {5, 3, 7};

        Set<Workday> guaranteedWorkdays = generateEntities
            .getDaysAsWorkdaySet(TASK_GUARANTEED_AY_DATE_OLD, TASK_GUARANTEED_AY_HOURS);

        Offer offer = generateEntities
            .createAndSaveOfferForTaskAndHelper(task, helper1, guaranteedWorkdays);

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value("+49 98 345 125637"))
            .andExpect(jsonPath("$.email").value("farmer@trash-mail.com"))
            .andExpect(
                jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.lat").isNotEmpty())

            .andExpect(jsonPath("$.guaranteedDaysByHelper").isEmpty())
            .andExpect(jsonPath("$.helpers").isEmpty());
    }


    @Test
    @Transactional
    @WithMockUser(roles = "FARM", username = "user")
    public void getOffersFromTaskAsUserWithRoleFarm() throws Exception {

        Helper helper1 = generateEntities.createAndSaveHelperForNewUserPrefixed("H1");
        Helper helper2 = generateEntities.createAndSaveHelperForNewUserPrefixed("H2");
        Helper helper3 = generateEntities.createAndSaveHelperForNewUserPrefixed("H3");

        Task task = generateEntities.createAndSaveTask();

        Offer offer = generateEntities
            .createAndSaveOfferForTaskAndHelper(task, helper1);
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper2);
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper3);

        Object[] workdaysGuaranteed = offer.getGuaranteedDays().toArray();
        Object[] workdaysRequested = task.getRequestedDays().toArray();

        restUserExtendMockMvc.perform(get("/api/tasks/" + task.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.guaranteedDays.[*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[2]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[2]).getWorkingHours() * 3)))
            .andExpect(jsonPath("$.guaranteedDays.[*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[1]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[1]).getWorkingHours() * 3)))
            .andExpect(jsonPath("$.guaranteedDays.[*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[0]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[0]).getWorkingHours() * 3)))
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.farmName").value(task.getFarm().getName()))
            .andExpect(jsonPath("$.title").value(task.getTitle()))
            .andExpect(jsonPath("$.taskDescription").value(task.getDescription()))
            .andExpect(jsonPath("$.howToFindUs").value(task.getHowToFindUs()))
            .andExpect(jsonPath("$.phone").value("+49 98 345 125637"))
            .andExpect(jsonPath("$.email").value("farmer@trash-mail.com"))
            .andExpect(
                jsonPath("$.street").value(task.getAddress().getStreet()))
            .andExpect(jsonPath("$.houseNr").value(task.getAddress().getHouseNr()))
            .andExpect(jsonPath("$.zip").value(task.getAddress().getZip()))
            .andExpect(jsonPath("$.city").value(task.getAddress().getCity()))
            .andExpect(jsonPath("$.lat").isNotEmpty())
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(((Workday) workdaysRequested[0]).getDate().toString())))
            .andExpect(
                jsonPath("$.requestedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysRequested[0]).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(((Workday) workdaysRequested[1]).getDate().toString())))
            .andExpect(
                jsonPath("$.requestedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysRequested[1]).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(((Workday) workdaysRequested[2]).getDate().toString())))
            .andExpect(
                jsonPath("$.requestedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysRequested[2]).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(((Workday) workdaysRequested[3]).getDate().toString())))
            .andExpect(
                jsonPath("$.requestedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysRequested[3]).getWorkingHours())))
            .andExpect(jsonPath("$.requestedDays.[*].date")
                .value(hasItem(((Workday) workdaysRequested[4]).getDate().toString())))
            .andExpect(
                jsonPath("$.requestedDays.[*].workingHours")
                    .value(hasItem(((Workday) workdaysRequested[4]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper1.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[0]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper1.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[0]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper1.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[1]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper1.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[1]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper1.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[2]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper1.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[2]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper2.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[0]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper2.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[0]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper2.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[1]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper2.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[1]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper2.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[2]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper2.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[2]).getWorkingHours())))

            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper3.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[0]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper3.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[0]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper3.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[1]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper3.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[1]).getWorkingHours())))
            .andExpect(jsonPath("$.guaranteedDaysByHelper['" + helper3.getId() + "'][*].date")
                .value(hasItem(((Workday) workdaysGuaranteed[2]).getDate().toString())))
            .andExpect(
                jsonPath("$.guaranteedDaysByHelper['" + helper3.getId() + "'][*].workingHours")
                    .value(hasItem(((Workday) workdaysGuaranteed[2]).getWorkingHours())))
            .andExpect(jsonPath("$.helpers[0].id").value(helper1.getId().intValue()))
            .andExpect(jsonPath("$.helpers[1].id").value(helper2.getId().intValue()))
            .andExpect(jsonPath("$.helpers[2].id").value(helper3.getId().intValue()));
    }


}
