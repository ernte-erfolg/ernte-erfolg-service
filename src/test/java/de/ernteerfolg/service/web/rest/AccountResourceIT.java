package de.ernteerfolg.service.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.config.Constants;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.repository.UserRepository;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.service.dto.UserDTO;
import de.ernteerfolg.service.web.api.model.CreateUserPayload;
import de.ernteerfolg.service.web.api.model.LoginVM;
import de.ernteerfolg.service.web.api.model.ProfileTypeEnum;
import java.util.Optional;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AccountResource} REST controller.
 */
@AutoConfigureMockMvc
@WithMockUser(value = AccountResourceIT.TEST_USER_LOGIN)
@SpringBootTest(classes = ErnteErfolgServiceApp.class)
public class AccountResourceIT {

  static final String TEST_USER_LOGIN = "test";

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private MockMvc restAccountMockMvc;

  @Test
  @Transactional
  public void testGetExistingAccount() throws Exception {
    CreateUserPayload validUser = new CreateUserPayload();
    validUser.setPassword("!235DummyMail");
    validUser.setEmail("test-register-valid@example.com");
    validUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    validUser.setUserClass(ProfileTypeEnum.HELPER);

    assertThat(userRepository.findOneByLogin(validUser.getEmail()).isPresent())
        .isFalse();

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(validUser)))
        .andExpect(status().isCreated());

    Optional<User> createdUser = userRepository.findOneByLogin(validUser.getEmail());
    assertThat(createdUser.isPresent()).isTrue();

    createdUser.get().setActivated(true);
    userRepository.save(createdUser.get());

    LoginVM loginUser = new LoginVM();
    loginUser.setPassword(validUser.getPassword());
    loginUser.setUsername(validUser.getEmail());

    restAccountMockMvc.perform(
        post("/api/account/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loginUser)))
        .andExpect(status().isOk());

    restAccountMockMvc.perform(get("/api/account")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.email").value(validUser.getEmail()))
        .andExpect(jsonPath("$.activated").value("true"))
        .andExpect(jsonPath("$.userClass").value(validUser.getUserClass().getValue()))
        .andExpect(jsonPath("$.id").exists())
        .andExpect(jsonPath("$.langKey").value(validUser.getLangKey()));
  }

  @Test
  public void testGetUnknownAccount() throws Exception {
    restAccountMockMvc.perform(get("/api/account")
        .accept(MediaType.APPLICATION_PROBLEM_JSON))
        .andExpect(status().is4xxClientError());
  }

  @Test
  @Transactional
  public void testLoginUnknownAccountValid() throws Exception {
    LoginVM validUser = new LoginVM();
    validUser.setPassword("!235DummyMail");
    validUser.setUsername("test-register-valid@example.com");

    assertThat(userRepository.findOneByLogin("test-register-valid").isPresent()).isFalse();

    restAccountMockMvc.perform(
        post("/api/account/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(validUser)))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @Transactional
  public void testLoginAccountValid() throws Exception {
    CreateUserPayload validUser = new CreateUserPayload();
    validUser.setPassword("!235DummyMail");
    validUser.setEmail("test-register-valid@example.com");
    validUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    validUser.setUserClass(ProfileTypeEnum.HELPER);

    assertThat(userRepository.findOneByLogin(validUser.getEmail()).isPresent())
        .isFalse();

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(validUser)))
        .andExpect(status().isCreated());

    Optional<User> createdUser = userRepository.findOneByLogin(validUser.getEmail());
    assertThat(createdUser.isPresent()).isTrue();

    createdUser.get().setActivated(true);
    userRepository.save(createdUser.get());

    LoginVM loginUser = new LoginVM();
    loginUser.setPassword(validUser.getPassword());
    loginUser.setUsername(validUser.getEmail());

    restAccountMockMvc.perform(
        post("/api/account/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loginUser)))
        .andExpect(status().isOk());
  }

  @Test
  @Transactional
  public void testLoginAccountNotActivated() throws Exception {
    CreateUserPayload validUser = new CreateUserPayload();
    validUser.setPassword("!235DummyMail");
    validUser.setEmail("test-register-valid@example.com");
    validUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    validUser.setUserClass(ProfileTypeEnum.HELPER);

    assertThat(userRepository.findOneByLogin(validUser.getEmail()).isPresent())
        .isFalse();

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(validUser)))
        .andExpect(status().isCreated());

    Optional<User> createdUser = userRepository.findOneByLogin(validUser.getEmail());
    assertThat(createdUser.isPresent()).isTrue();

    LoginVM loginUser = new LoginVM();
    loginUser.setPassword(validUser.getPassword());
    loginUser.setUsername(validUser.getEmail());

    restAccountMockMvc.perform(
        post("/api/account/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loginUser)))
        .andExpect(status().isUnauthorized());
  }


  @Test
  @Transactional
  public void testRegisterValid() throws Exception {
    CreateUserPayload validUser = new CreateUserPayload();
    validUser.setPassword("!235DummyMail");
    validUser.setEmail("test-register-valid@example.com");
    validUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    validUser.setUserClass(ProfileTypeEnum.HELPER);

    assertThat(userRepository.findOneByLogin("test-register-valid@example.com").isPresent())
        .isFalse();

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(validUser)))
        .andExpect(status().isCreated());

    assertThat(userRepository.findOneByLogin(validUser.getEmail()).isPresent()).isTrue();
  }

  @Test
  @Transactional
  public void testRegisterInvalidEmail() throws Exception {
    CreateUserPayload invalidUser = new CreateUserPayload();
    invalidUser.setPassword("password");
    invalidUser.setEmail("invalid");// <-- invalid
    invalidUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    invalidUser.setUserClass(ProfileTypeEnum.HELPER);

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invalidUser)))
        .andExpect(status().isBadRequest());

    Optional<User> user = userRepository.findOneByLogin("bob");
    assertThat(user.isPresent()).isFalse();
  }

  @Test
  @Transactional
  public void testRegisterInvalidPassword() throws Exception {
    CreateUserPayload invalidUser = new CreateUserPayload();
    invalidUser.setPassword("123");// password with only 3 digits
    invalidUser.setEmail("bob@example.com");
    invalidUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    invalidUser.setUserClass(ProfileTypeEnum.FARM);

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invalidUser)))
        .andExpect(status().isBadRequest());

    Optional<User> user = userRepository.findOneByLogin("bob");
    assertThat(user.isPresent()).isFalse();
  }

  @Test
  @Transactional
  public void testRegisterNullPassword() throws Exception {
    CreateUserPayload invalidUser = new CreateUserPayload();
    invalidUser.setPassword(null);// invalid null password
    invalidUser.setEmail("bob@example.com");
    invalidUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    invalidUser.setUserClass(ProfileTypeEnum.FARM);

    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(invalidUser)))
        .andExpect(status().isBadRequest());

    Optional<User> user = userRepository.findOneByLogin("bob");
    assertThat(user.isPresent()).isFalse();
  }

  @Test
  @Transactional
  public void testRegisterDuplicateEmail() throws Exception {
    // First user
    CreateUserPayload firstUser = new CreateUserPayload();
    firstUser.setPassword("!235DummyMail");
    firstUser.setEmail("test-register-duplicate-email@example.com");
    firstUser.setLangKey(Constants.DEFAULT_LANGUAGE);
    firstUser.setUserClass(ProfileTypeEnum.FARM);

    assertThat(userRepository
        .findOneByLogin("test-register-duplicate-email@example.com").isPresent()).isFalse();

    // Register first user
    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(firstUser)))
        .andExpect(status().isCreated());

    Optional<User> testUser1 = userRepository
        .findOneByLogin("test-register-duplicate-email@example.com");
    assertThat(testUser1.isPresent()).isTrue();

    // Duplicate email - with uppercase email address
    CreateUserPayload userWithUpperCaseEmail = new CreateUserPayload();
    userWithUpperCaseEmail.setPassword(firstUser.getPassword());
    userWithUpperCaseEmail.setEmail("TEST-register-duplicate-email@example.com");
    userWithUpperCaseEmail.setLangKey(firstUser.getLangKey());
    userWithUpperCaseEmail.setUserClass(ProfileTypeEnum.FARM);

    //for non activated accounts its allowed to re-register

    // Register third (not activated) user
    restAccountMockMvc.perform(
        post("/api/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userWithUpperCaseEmail)))
        .andExpect(status().isCreated());

    Optional<User> testUser4 = userRepository.findOneByLogin(testUser1.get().getEmail());
    assertThat(testUser4.isPresent()).isTrue();
    assertThat(testUser4.get().getEmail()).isEqualTo("test-register-duplicate-email@example.com");

    testUser4.get().setActivated(true);
    userService.updateUser((new UserDTO(testUser4.get())));

    // Register 4th (already activated) user
    restAccountMockMvc.perform(
        post("/api/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(firstUser)))
        .andExpect(status().is4xxClientError());
  }

  @Test
  @Transactional
  public void testActivateAccount() throws Exception {
    final String activationKey = "some activation key";
    User user = new User();
    user.setLogin("activate-account");
    user.setEmail("activate-account@example.com");
    user.setPassword(RandomStringUtils.random(60));
    user.setActivated(false);
    user.setActivationKey(activationKey);

    userRepository.saveAndFlush(user);

    restAccountMockMvc.perform(get("/api/account/activate?key={activationKey}", activationKey))
        .andExpect(status().isOk());

    user = userRepository.findOneByLogin(user.getLogin()).orElse(null);
    assertThat(user.getActivated()).isTrue();
  }

  @Test
  @Transactional
  public void testActivateAccountWithWrongKey() throws Exception {
    restAccountMockMvc.perform(get("/api/account/activate?key=wrongActivationKey"))
        .andExpect(status().isUnauthorized());
  }

//    @Test
//    @Transactional
//    @WithMockUser("save-account")
//    public void testSaveAccount() throws Exception {
//        User user = new User();
//        user.setLogin("save-account");
//        user.setEmail("save-account@example.com");
//        user.setPassword(RandomStringUtils.random(60));
//        user.setActivated(true);
//
//        userRepository.saveAndFlush(user);
//
//        UserDTO userDTO = new UserDTO();
//        userDTO.setLogin("not-used");
//        userDTO.setFirstName("firstname");
//        userDTO.setLastName("lastname");
//        userDTO.setEmail("save-account@example.com");
//        userDTO.setActivated(false);
//        userDTO.setImageUrl("http://placehold.it/50x50");
//        userDTO.setLangKey(Constants.DEFAULT_LANGUAGE);
//        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.ADMIN));
//
//        restAccountMockMvc.perform(
//            post("/api/account")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
//            .andExpect(status().isOk());
//
//        User updatedUser = userRepository.findOneByLogin(user.getLogin()).orElse(null);
//        assertThat(updatedUser.getFirstName()).isEqualTo(userDTO.getFirstName());
//        assertThat(updatedUser.getLastName()).isEqualTo(userDTO.getLastName());
//        assertThat(updatedUser.getEmail()).isEqualTo(userDTO.getEmail());
//        assertThat(updatedUser.getLangKey()).isEqualTo(userDTO.getLangKey());
//        assertThat(updatedUser.getPassword()).isEqualTo(user.getPassword());
//        assertThat(updatedUser.getImageUrl()).isEqualTo(userDTO.getImageUrl());
//        assertThat(updatedUser.getActivated()).isEqualTo(true);
//        assertThat(updatedUser.getAuthorities()).isEmpty();
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("save-invalid-email")
//    public void testSaveInvalidEmail() throws Exception {
//        User user = new User();
//        user.setLogin("save-invalid-email");
//        user.setEmail("save-invalid-email@example.com");
//        user.setPassword(RandomStringUtils.random(60));
//        user.setActivated(true);
//
//        userRepository.saveAndFlush(user);
//
//        UserDTO userDTO = new UserDTO();
//        userDTO.setLogin("not-used");
//        userDTO.setFirstName("firstname");
//        userDTO.setLastName("lastname");
//        userDTO.setEmail("invalid email");
//        userDTO.setActivated(false);
//        userDTO.setImageUrl("http://placehold.it/50x50");
//        userDTO.setLangKey(Constants.DEFAULT_LANGUAGE);
//        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.ADMIN));
//
//        restAccountMockMvc.perform(
//            post("/api/account")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
//            .andExpect(status().isBadRequest());
//
//        assertThat(userRepository.findOneByEmailIgnoreCase("invalid email")).isNotPresent();
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("save-existing-email")
//    public void testSaveExistingEmail() throws Exception {
//        User user = new User();
//        user.setLogin("save-existing-email");
//        user.setEmail("save-existing-email@example.com");
//        user.setPassword(RandomStringUtils.random(60));
//        user.setActivated(true);
//
//        userRepository.saveAndFlush(user);
//
//        User anotherUser = new User();
//        anotherUser.setLogin("save-existing-email2");
//        anotherUser.setEmail("save-existing-email2@example.com");
//        anotherUser.setPassword(RandomStringUtils.random(60));
//        anotherUser.setActivated(true);
//
//        userRepository.saveAndFlush(anotherUser);
//
//        UserDTO userDTO = new UserDTO();
//        userDTO.setLogin("not-used");
//        userDTO.setFirstName("firstname");
//        userDTO.setLastName("lastname");
//        userDTO.setEmail("save-existing-email2@example.com");
//        userDTO.setActivated(false);
//        userDTO.setImageUrl("http://placehold.it/50x50");
//        userDTO.setLangKey(Constants.DEFAULT_LANGUAGE);
//        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.ADMIN));
//
//        restAccountMockMvc.perform(
//            post("/api/account")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
//            .andExpect(status().isBadRequest());
//
//        User updatedUser = userRepository.findOneByLogin("save-existing-email").orElse(null);
//        assertThat(updatedUser.getEmail()).isEqualTo("save-existing-email@example.com");
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("save-existing-email-and-login")
//    public void testSaveExistingEmailAndLogin() throws Exception {
//        User user = new User();
//        user.setLogin("save-existing-email-and-login");
//        user.setEmail("save-existing-email-and-login@example.com");
//        user.setPassword(RandomStringUtils.random(60));
//        user.setActivated(true);
//
//        userRepository.saveAndFlush(user);
//
//        UserDTO userDTO = new UserDTO();
//        userDTO.setLogin("not-used");
//        userDTO.setFirstName("firstname");
//        userDTO.setLastName("lastname");
//        userDTO.setEmail("save-existing-email-and-login@example.com");
//        userDTO.setActivated(false);
//        userDTO.setImageUrl("http://placehold.it/50x50");
//        userDTO.setLangKey(Constants.DEFAULT_LANGUAGE);
//        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.ADMIN));
//
//        restAccountMockMvc.perform(
//            post("/api/account")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(userDTO)))
//            .andExpect(status().isOk());
//
//        User updatedUser = userRepository.findOneByLogin("save-existing-email-and-login").orElse(null);
//        assertThat(updatedUser.getEmail()).isEqualTo("save-existing-email-and-login@example.com");
//    }

//    @Test
//    @Transactional
//    @WithMockUser("change-password-wrong-existing-password")
//    public void testChangePasswordWrongExistingPassword() throws Exception {
//        User user = new User();
//        String currentPassword = RandomStringUtils.random(60);
//        user.setPassword(passwordEncoder.encode(currentPassword));
//        user.setLogin("change-password-wrong-existing-password");
//        user.setEmail("change-password-wrong-existing-password@example.com");
//        userRepository.saveAndFlush(user);
//
//        restAccountMockMvc.perform(post("/api/account/change-password")
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(new PasswordChangeDTO("1"+currentPassword, "new password")))
//)
//            .andExpect(status().isBadRequest());
//
//        User updatedUser = userRepository.findOneByLogin("change-password-wrong-existing-password").orElse(null);
//        assertThat(passwordEncoder.matches("new password", updatedUser.getPassword())).isFalse();
//        assertThat(passwordEncoder.matches(currentPassword, updatedUser.getPassword())).isTrue();
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("change-password")
//    public void testChangePassword() throws Exception {
//        User user = new User();
//        String currentPassword = RandomStringUtils.random(60);
//        user.setPassword(passwordEncoder.encode(currentPassword));
//        user.setLogin("change-password");
//        user.setEmail("change-password@example.com");
//        userRepository.saveAndFlush(user);
//
//        restAccountMockMvc.perform(post("/api/account/change-password")
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(new PasswordChangeDTO(currentPassword, "new password")))
//)
//            .andExpect(status().isOk());
//
//        User updatedUser = userRepository.findOneByLogin("change-password").orElse(null);
//        assertThat(passwordEncoder.matches("new password", updatedUser.getPassword())).isTrue();
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("change-password-too-small")
//    public void testChangePasswordTooSmall() throws Exception {
//        User user = new User();
//        String currentPassword = RandomStringUtils.random(60);
//        user.setPassword(passwordEncoder.encode(currentPassword));
//        user.setLogin("change-password-too-small");
//        user.setEmail("change-password-too-small@example.com");
//        userRepository.saveAndFlush(user);
//
//        String newPassword = RandomStringUtils.random(ManagedUserVM.PASSWORD_MIN_LENGTH - 1);
//
//        restAccountMockMvc.perform(post("/api/account/change-password")
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(new PasswordChangeDTO(currentPassword, newPassword)))
//)
//            .andExpect(status().isBadRequest());
//
//        User updatedUser = userRepository.findOneByLogin("change-password-too-small").orElse(null);
//        assertThat(updatedUser.getPassword()).isEqualTo(user.getPassword());
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("change-password-too-long")
//    public void testChangePasswordTooLong() throws Exception {
//        User user = new User();
//        String currentPassword = RandomStringUtils.random(60);
//        user.setPassword(passwordEncoder.encode(currentPassword));
//        user.setLogin("change-password-too-long");
//        user.setEmail("change-password-too-long@example.com");
//        userRepository.saveAndFlush(user);
//
//        String newPassword = RandomStringUtils.random(ManagedUserVM.PASSWORD_MAX_LENGTH + 1);
//
//        restAccountMockMvc.perform(post("/api/account/change-password")
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(new PasswordChangeDTO(currentPassword, newPassword)))
//)
//            .andExpect(status().isBadRequest());
//
//        User updatedUser = userRepository.findOneByLogin("change-password-too-long").orElse(null);
//        assertThat(updatedUser.getPassword()).isEqualTo(user.getPassword());
//    }
//
//    @Test
//    @Transactional
//    @WithMockUser("change-password-empty")
//    public void testChangePasswordEmpty() throws Exception {
//        User user = new User();
//        String currentPassword = RandomStringUtils.random(60);
//        user.setPassword(passwordEncoder.encode(currentPassword));
//        user.setLogin("change-password-empty");
//        user.setEmail("change-password-empty@example.com");
//        userRepository.saveAndFlush(user);
//
//        restAccountMockMvc.perform(post("/api/account/change-password")
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(new PasswordChangeDTO(currentPassword, "")))
//)
//            .andExpect(status().isBadRequest());
//
//        User updatedUser = userRepository.findOneByLogin("change-password-empty").orElse(null);
//        assertThat(updatedUser.getPassword()).isEqualTo(user.getPassword());
//    }
//
//    @Test
//    @Transactional
//    public void testRequestPasswordReset() throws Exception {
//        User user = new User();
//        user.setPassword(RandomStringUtils.random(60));
//        user.setActivated(true);
//        user.setLogin("password-reset");
//        user.setEmail("password-reset@example.com");
//        userRepository.saveAndFlush(user);
//
//        restAccountMockMvc.perform(post("/api/account/reset-password/init")
//            .content("password-reset@example.com")
//)
//            .andExpect(status().isOk());
//    }
//
//    @Test
//    @Transactional
//    public void testRequestPasswordResetUpperCaseEmail() throws Exception {
//        User user = new User();
//        user.setPassword(RandomStringUtils.random(60));
//        user.setActivated(true);
//        user.setLogin("password-reset");
//        user.setEmail("password-reset@example.com");
//        userRepository.saveAndFlush(user);
//
//        restAccountMockMvc.perform(post("/api/account/reset-password/init")
//            .content("password-reset@EXAMPLE.COM")
//)
//            .andExpect(status().isOk());
//    }
//
//    @Test
//    public void testRequestPasswordResetWrongEmail() throws Exception {
//        restAccountMockMvc.perform(
//            post("/api/account/reset-password/init")
//                .content("password-reset-wrong-email@example.com"))
//            .andExpect(status().isOk());
//    }
//
//    @Test
//    @Transactional
//    public void testFinishPasswordReset() throws Exception {
//        User user = new User();
//        user.setPassword(RandomStringUtils.random(60));
//        user.setLogin("finish-password-reset");
//        user.setEmail("finish-password-reset@example.com");
//        user.setResetDate(Instant.now().plusSeconds(60));
//        user.setResetKey("reset key");
//        userRepository.saveAndFlush(user);
//
//        KeyAndPasswordVM keyAndPassword = new KeyAndPasswordVM();
//        keyAndPassword.setKey(user.getResetKey());
//        keyAndPassword.setNewPassword("new password");
//
//        restAccountMockMvc.perform(
//            post("/api/account/reset-password/finish")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(keyAndPassword)))
//            .andExpect(status().isOk());
//
//        User updatedUser = userRepository.findOneByLogin(user.getLogin()).orElse(null);
//        assertThat(passwordEncoder.matches(keyAndPassword.getNewPassword(), updatedUser.getPassword())).isTrue();
//    }
//
//    @Test
//    @Transactional
//    public void testFinishPasswordResetTooSmall() throws Exception {
//        User user = new User();
//        user.setPassword(RandomStringUtils.random(60));
//        user.setLogin("finish-password-reset-too-small");
//        user.setEmail("finish-password-reset-too-small@example.com");
//        user.setResetDate(Instant.now().plusSeconds(60));
//        user.setResetKey("reset key too small");
//        userRepository.saveAndFlush(user);
//
//        KeyAndPasswordVM keyAndPassword = new KeyAndPasswordVM();
//        keyAndPassword.setKey(user.getResetKey());
//        keyAndPassword.setNewPassword("foo");
//
//        restAccountMockMvc.perform(
//            post("/api/account/reset-password/finish")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(keyAndPassword)))
//            .andExpect(status().isBadRequest());
//
//        User updatedUser = userRepository.findOneByLogin(user.getLogin()).orElse(null);
//        assertThat(passwordEncoder.matches(keyAndPassword.getNewPassword(), updatedUser.getPassword())).isFalse();
//    }
//
//    @Test
//    @Transactional
//    public void testFinishPasswordResetWrongKey() throws Exception {
//        KeyAndPasswordVM keyAndPassword = new KeyAndPasswordVM();
//        keyAndPassword.setKey("wrong reset key");
//        keyAndPassword.setNewPassword("new password");
//
//        restAccountMockMvc.perform(
//            post("/api/account/reset-password/finish")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(TestUtil.convertObjectToJsonBytes(keyAndPassword)))
//            .andExpect(status().isInternalServerError());
//    }
}
