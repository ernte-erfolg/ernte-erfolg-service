package de.ernteerfolg.service.web.rest;

import static de.ernteerfolg.service.web.rest.Routinen.GenerateEntities.getTaskGuaranteedAyDate;
import static de.ernteerfolg.service.web.rest.Routinen.GenerateEntities.getTaskGuaranteedAyHours;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.repository.WorkdayRepository;
import de.ernteerfolg.service.web.api.model.CreateOfferPayload;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import de.ernteerfolg.service.web.rest.Routinen.GenerateEntities;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OfferResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class OfferResourceIT {

	@Autowired
	private MockMvc restUserExtendMockMvc;

	@Autowired
	private GenerateEntities generateEntities;

	@Autowired
    private TaskRepository taskRepository;

	@Autowired
    private WorkdayRepository workdayRepository;

	@Test
	@Transactional
	@WithMockUser(roles = "FARM", username = "user")
	public void createOfferAsUserWithRoleFarm() throws Exception {

		Task task = generateEntities.createAndSaveTask();
		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(post("/api/offers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "ANONYMOUS", username = "user")
	public void createOfferAsUserWithRoleANONYMOUS() throws Exception {

		Task task = generateEntities.createAndSaveTask();

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(post("/api/offers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "ADMIN", username = "user")
	public void createOfferAsUserWithRoleADMIN() throws Exception {

		Task task = generateEntities.createAndSaveTask();

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(post("/api/offers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void createOfferAsUserWithRoleHelperWithNotRequestedDays() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		Task task = generateEntities.createAndSaveTask();

		//NOT REQUESTED DAYS
		String[] notRequestDays = {LocalDate.now().plusDays(333).toString()};
		CreateOfferPayload createOfferPayload = createOfferPayload(task, notRequestDays,
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(post("/api/offers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isConflict());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void createOfferAsUserWithRoleHelperWithDaysInThePast() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		String[] TASK_REQUESTED_DAY_DATE_WITH_OLD = {LocalDate.now().minusDays(1).toString(),
				LocalDate.now().toString(), LocalDate.now().plusDays(1).toString()};
		int[] TASK_REQUESTED_DAY_HOURS = {6, 8, 10, 12, 14};

//		Task task = generateEntities.createAndSaveTask();
		Task task = generateEntities.createTask();
		task.setRequestedDays(generateEntities
				.getDaysAsWorkdaySet(TASK_REQUESTED_DAY_DATE_WITH_OLD, TASK_REQUESTED_DAY_HOURS));
		taskRepository.save(task);

		//NOT REQUESTED DAYS
		String[] notRequestDays = {LocalDate.now().minusDays(1).toString()};
		CreateOfferPayload createOfferPayload = createOfferPayload(task, notRequestDays,
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(post("/api/offers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isConflict());
	}


	/**
	 * Tests that a helper cannot see data of other helper when getting an offer.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void createOfferAsUserWithRoleHelper() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		Task task = generateEntities.createAndSaveTask();
		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		Set<Workday> requestedDays = task.getRequestedDays();
		Workday[] requestDaysArray = requestedDays.toArray(new Workday[requestedDays.size()]);
		List<WorkdayPayload> guaranteedDays = createOfferPayload.getGuaranteedDays();

		restUserExtendMockMvc.perform(post("/api/offers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.id").isNotEmpty())
				.andExpect(
						jsonPath("$.guaranteedDays.[*].date").value(hasItem(guaranteedDays.get(0).getDate().toString())))
				.andExpect(jsonPath("$.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDays.get(0).getWorkingHours())))
				.andExpect(
						jsonPath("$.guaranteedDays.[*].date").value(hasItem(guaranteedDays.get(1).getDate().toString())))
				.andExpect(jsonPath("$.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDays.get(1).getWorkingHours())))
				.andExpect(
						jsonPath("$.guaranteedDays.[*].date").value(hasItem(guaranteedDays.get(2).getDate().toString())))
				.andExpect(jsonPath("$.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDays.get(2).getWorkingHours())))
				.andExpect(jsonPath("$.task.id").isNotEmpty())
				.andExpect(jsonPath("$.task.id").value(task.getId()))
				.andExpect(jsonPath("$.task.farmId").value(task.getFarm().getId()))
				.andExpect(jsonPath("$.task.farmName").value(task.getFarm().getName()))
				.andExpect(jsonPath("$.task.title").value(task.getTitle()))
				.andExpect(jsonPath("$.task.taskDescription").value(task.getDescription()))
				.andExpect(jsonPath("$.task.howToFindUs").value(task.getHowToFindUs()))
				.andExpect(jsonPath("$.task.phone").value(task.getPhone()))
				.andExpect(jsonPath("$.task.email").value(task.getContactEmail()))
				.andExpect(
						jsonPath("$.task.street").value(task.getAddress().getStreet()))
				.andExpect(jsonPath("$.task.houseNr").value(task.getAddress().getHouseNr()))
				.andExpect(jsonPath("$.task.zip").value(task.getAddress().getZip()))
				.andExpect(jsonPath("$.task.city").value(task.getAddress().getCity()))
				.andExpect(jsonPath("$.task.lat").isNotEmpty())
				.andExpect(jsonPath("$.task.lon").isNotEmpty())
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[0].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[0].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[1].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[1].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[2].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[2].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[3].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[3].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[4].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[4].getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].date")
						.value(hasItem(guaranteedDays.get(2).getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDays.get(2).getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].date")
						.value(hasItem(guaranteedDays.get(1).getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDays.get(1).getWorkingHours())))
				.andExpect(
						jsonPath("$.task.guaranteedDays.[*].date")
								.value(hasItem(guaranteedDays.get(0).getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays[*].workingHours")
						.value(hasItem(guaranteedDays.get(0).getWorkingHours())))

				.andExpect(jsonPath("$.task.guaranteedDaysByHelper").isEmpty())
				.andExpect(jsonPath("$.task.helpers").isEmpty());
		// A helper is not allowed to see data of other helpers!
	}

    /**
     * Tests that a helper cannot see data of other helper when getting an offer.
     */
    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void createOfferAsUserWithRoleHelperOverBooked() throws Exception {
        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

        Task task = generateEntities.createAndSaveTask();
        CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
            getTaskGuaranteedAyHours());
        createOfferPayload.getGuaranteedDays().forEach(workdayPayload -> workdayPayload.setWorkingHours(200));

        Set<Workday> requestedDays = task.getRequestedDays();
        Workday[] requestDaysArray = requestedDays.toArray(new Workday[0]);
        List<WorkdayPayload> guaranteedDays = createOfferPayload.getGuaranteedDays();

        restUserExtendMockMvc.perform(post("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
            .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void createOfferAsUserWithRoleHelperOverBookedWithExistingOffers() throws Exception {
        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
        Helper helper1 = generateEntities.createAndSaveHelperForNewUser ();
        Task task = generateEntities.createAndSaveTask();
        generateEntities.createAndSaveOfferForTaskAndHelper(task, helper1);

        CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
            getTaskGuaranteedAyHours());
        createOfferPayload.getGuaranteedDays().forEach(workdayPayload -> workdayPayload.setWorkingHours(26));

        Set<Workday> requestedDays = task.getRequestedDays();
        Workday[] requestDaysArray = requestedDays.toArray(new Workday[0]);
        List<WorkdayPayload> guaranteedDays = createOfferPayload.getGuaranteedDays();

        restUserExtendMockMvc.perform(post("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
            .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void createOfferAsUserWithRoleHelperforTaskNotActive() throws Exception {
        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

        Task task = generateEntities.createAndSaveTask();
        task.setActive(false);
        CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
            getTaskGuaranteedAyHours());

        Set<Workday> requestedDays = task.getRequestedDays();
        Workday[] requestDaysArray = requestedDays.toArray(new Workday[requestedDays.size()]);
        List<WorkdayPayload> guaranteedDays = createOfferPayload.getGuaranteedDays();

        restUserExtendMockMvc.perform(post("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
            .andExpect(status().isConflict());
    }

	@Test
	@Transactional
	@WithMockUser(roles = "FARM", username = "user")
	public void updateOfferAsUserWithRoleFarm() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForNewUser();
		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "ADMIN", username = "user")
	public void updateOfferAsUserWithRoleADMIN() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForNewUser();

		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isForbidden());
	}


	@Test
	@Transactional
	@WithMockUser(roles = "ANONYMOUS", username = "user")
	public void updateOfferAsUserWithRoleANONYMOUS() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForNewUser();

		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());

		restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void updateOfferAsUserWithRoleHelper() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());
		createOfferPayload.getGuaranteedDays().remove(0);

		restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(offer.getId()))
				.andExpect(jsonPath("$.guaranteedDays.[0].date")
						.value(createOfferPayload.getGuaranteedDays().get(0).getDate().toString()))
				.andExpect(jsonPath("$.guaranteedDays.[0].workingHours")
						.value(createOfferPayload.getGuaranteedDays().get(0).getWorkingHours()))
				.andExpect(jsonPath("$.guaranteedDays.[1].date")
						.value(createOfferPayload.getGuaranteedDays().get(1).getDate().toString()))
				.andExpect(jsonPath("$.guaranteedDays.[1].workingHours")
						.value(createOfferPayload.getGuaranteedDays().get(1).getWorkingHours()))
				.andExpect(jsonPath("$.guaranteedDays.length()").value(2));
	}

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void updateOfferAsUserWithRoleHelperOverbooked() throws Exception {

        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
        Task task = generateEntities.createAndSaveTask();
        Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
            getTaskGuaranteedAyHours());
        createOfferPayload.getGuaranteedDays().remove(0);
        createOfferPayload.getGuaranteedDays().forEach(workdayPayload -> workdayPayload.setWorkingHours(30));

        restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
            .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "HELPER", username = "user")
    public void updateOfferAsUserWithRoleHelperTaskNotActive() throws Exception {

        Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
        Task task = generateEntities.createAndSaveTask();
        Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

        CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
            getTaskGuaranteedAyHours());
        createOfferPayload.getGuaranteedDays().remove(0);
        task.setRequestedDays(new HashSet<>(workdayRepository.saveAll(task.getRequestedDays())));
        task.setActive(false);

        taskRepository.save(task);

        restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
            .andExpect(status().isConflict());
    }

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void updateForeignOfferAsUserWithRoleHelper() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
		Helper helperForeign = generateEntities.createAndSaveHelperForNewUser();

		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helperForeign);

		CreateOfferPayload createOfferPayload = createOfferPayload(task, getTaskGuaranteedAyDate(),
				getTaskGuaranteedAyHours());
		createOfferPayload.getGuaranteedDays().remove(0);

		restUserExtendMockMvc.perform(put("/api/offers/" + offer.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(createOfferPayload)))
				.andExpect(status().isConflict());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void getOfferAsUserWithRoleHelper() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		Farm farm = generateEntities.createAndSaveFarmForCurrentUser();
		Task task1 = generateEntities.createAndSaveTask(farm.getAddress(), farm);
		Task task2 = generateEntities.createAndSaveTask(farm.getAddress(), farm);

		Offer offer = generateEntities
				.createAndSaveOfferForTaskAndHelper(task1, helper);

		Offer offer2 = generateEntities
				.createAndSaveOfferForTaskAndHelper(task2, helper);

		Set<Workday> requestedDays = task1.getRequestedDays();
		Workday[] requestDaysArray = requestedDays.toArray(new Workday[requestedDays.size()]);

		Set<Workday> guaranteedDays = offer.getGuaranteedDays();
		Workday[] guaranteedDaysArray = guaranteedDays.toArray(new Workday[guaranteedDays.size()]);

		restUserExtendMockMvc.perform(get("/api/offers/" + offer.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(offer.getId()))
				.andExpect(
						jsonPath("$.guaranteedDays.[*].date")
								.value(hasItem(guaranteedDaysArray[0].getDate().toString())))
				.andExpect(jsonPath("$.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[0].getWorkingHours())))
				.andExpect(
						jsonPath("$.guaranteedDays.[*].date")
								.value(hasItem(guaranteedDaysArray[1].getDate().toString())))
				.andExpect(jsonPath("$.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[1].getWorkingHours())))
				.andExpect(
						jsonPath("$.guaranteedDays.[*].date")
								.value(hasItem(guaranteedDaysArray[2].getDate().toString())))
				.andExpect(jsonPath("$.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[2].getWorkingHours())))
				.andExpect(jsonPath("$.task.id").isNotEmpty())
				.andExpect(jsonPath("$.task.farmId").value(farm.getId()))
				.andExpect(jsonPath("$.task.farmName").value(task1.getFarm().getName()))
				.andExpect(jsonPath("$.task.title").value(task1.getTitle()))
				.andExpect(jsonPath("$.task.taskDescription").value(task1.getDescription()))
				.andExpect(jsonPath("$.task.howToFindUs").value(task1.getHowToFindUs()))
				.andExpect(jsonPath("$.task.phone").value(task1.getPhone()))
				.andExpect(jsonPath("$.task.email").value(task1.getContactEmail()))
				.andExpect(
						jsonPath("$.task.street").value(task1.getAddress().getStreet()))
				.andExpect(jsonPath("$.task.houseNr").value(task1.getAddress().getHouseNr()))
				.andExpect(jsonPath("$.task.zip").value(task1.getAddress().getZip()))
				.andExpect(jsonPath("$.task.city").value(task1.getAddress().getCity()))
				.andExpect(jsonPath("$.task.lat").isNotEmpty())
				.andExpect(jsonPath("$.task.lon").isNotEmpty())
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[0].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[0].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[1].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[1].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[2].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[2].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[3].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[3].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[4].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[4].getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].date")
						.value(hasItem(guaranteedDaysArray[2].getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[2].getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].date")
						.value(hasItem(guaranteedDaysArray[1].getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[1].getWorkingHours())))
				.andExpect(
						jsonPath("$.task.guaranteedDays.[*].date")
								.value(hasItem(guaranteedDaysArray[0].getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays[*].workingHours")
						.value(hasItem(guaranteedDaysArray[0].getWorkingHours())))

				.andExpect(jsonPath("$.task.guaranteedDaysByHelper").isEmpty())
				.andExpect(jsonPath("$.task.helpers").isEmpty());
		// A helper is not allowed to see data of other helpers!
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void getForeignOfferAsUserWithRoleHelper() throws Exception {

		Helper helperForeign = generateEntities.createAndSaveHelperForNewUser();
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		Farm farm = generateEntities.createAndSaveFarmForCurrentUser();
		Task task1 = generateEntities.createAndSaveTask(farm.getAddress(), farm);

		Offer offer = generateEntities
				.createAndSaveOfferForTaskAndHelper(task1, helperForeign);

		restUserExtendMockMvc.perform(get("/api/offers/" + offer.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	/**
	 * Tests if a FARM is allowed to call the endpoint.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "FARM", username = "user")
	public void getOfferAsUserWithRoleFarm() throws Exception {

		Helper helper1 = generateEntities.createAndSaveHelperForNewUser();
		Task task = generateEntities.createAndSaveTask();

		Offer offer = generateEntities
				.createAndSaveOfferForTaskAndHelper(task, helper1);

		restUserExtendMockMvc.perform(get("/api/offers/" + offer.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}


	/**
	 * Tests that a ANONYMOUS is NOT allowed to call the endpoint.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "ANONYMOUS", username = "user")
	public void getOfferAsUserWithRoleAnonymous() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForNewUser();
		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		restUserExtendMockMvc.perform(get("/api/offers/" + offer.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	/**
	 * Tests that a ANONYMOUS is NOT allowed to call the endpoint.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "ADMIN", username = "user")
	public void getOfferAsUserWithRoleADMIN() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForNewUser();
		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		restUserExtendMockMvc.perform(get("/api/offers/" + offer.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}


	/**
	 * Tests that a FARM is NOT allowed to call the endpoint.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "FARM", username = "user")
	public void getOfferOfUserAsUserWithRoleFarm() throws Exception {
		restUserExtendMockMvc.perform(get("/api/offers")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	/**
	 * Tests that a ADMIN is NOT allowed to call the endpoint.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "ADMIN", username = "user")
	public void getOfferOfUserAsUserWithRoleAdmin() throws Exception {
		restUserExtendMockMvc.perform(get("/api/offers")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	/**
	 * Tests that a ANONYMOUS is NOT allowed to call the endpoint.
	 */
	@Test
	@Transactional
	@WithMockUser(roles = "ANONYMOUS", username = "user")
	public void getOfferOfUserAsUserWithRoleAnonymous() throws Exception {
		restUserExtendMockMvc.perform(get("/api/offers")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void getOfferOfUserAsUserWithRoleHelper() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		Farm farm = generateEntities.createAndSaveFarm();

		Task task1 = generateEntities.createAndSaveTask(farm.getAddress(), farm);
		Task task2 = generateEntities.createAndSaveTask(farm.getAddress(), farm);

		Offer offer1 = generateEntities.createAndSaveOfferForTaskAndHelper(task1, helper);
		Offer offer2 = generateEntities.createAndSaveOfferForTaskAndHelper(task2, helper);

		Object[] workdaysGuaranteed = offer1.getGuaranteedDays().toArray();
		Object[] workdaysRequested = task1.getRequestedDays().toArray();

		restUserExtendMockMvc.perform(get("/api/offers/")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(offer1.getId()))
				.andExpect(
						jsonPath("$[0].guaranteedDays.[0].date")
								.value(((Workday) workdaysGuaranteed[0]).getDate().toString()))
				.andExpect(
						jsonPath("$[0].guaranteedDays.[0].workingHours")
								.value(((Workday) workdaysGuaranteed[0]).getWorkingHours()))
				.andExpect(
						jsonPath("$[0].guaranteedDays.[1].date")
								.value(((Workday) workdaysGuaranteed[1]).getDate().toString()))
				.andExpect(
						jsonPath("$[0].guaranteedDays.[1].workingHours")
								.value(((Workday) workdaysGuaranteed[1]).getWorkingHours()))
				.andExpect(
						jsonPath("$[0].guaranteedDays.[2].date")
								.value(((Workday) workdaysGuaranteed[2]).getDate().toString()))
				.andExpect(
						jsonPath("$[0].guaranteedDays.[2].workingHours")
								.value(((Workday) workdaysGuaranteed[2]).getWorkingHours()))
				.andExpect(jsonPath("$[0].task.id").isNotEmpty())
				.andExpect(jsonPath("$[0].task.farmId").value(farm.getId()))
				.andExpect(jsonPath("$[0].task.farmName").value(task1.getFarm().getName()))
				.andExpect(jsonPath("$[0].task.title").value(task1.getTitle()))
				.andExpect(jsonPath("$[0].task.taskDescription").value(task1.getDescription()))
				.andExpect(jsonPath("$[0].task.howToFindUs").value(task1.getHowToFindUs()))
				.andExpect(jsonPath("$[0].task.phone").value(task1.getPhone()))
				.andExpect(jsonPath("$[0].task.email").value(task1.getContactEmail()))
				.andExpect(jsonPath("$[0].task.street")
						.value(task1.getAddress().getStreet()))
				.andExpect(jsonPath("$[0].task.houseNr").value(task1.getAddress().getHouseNr()))
				.andExpect(jsonPath("$[0].task.zip").value(task1.getAddress().getZip()))
				.andExpect(jsonPath("$[0].task.city").value(task1.getAddress().getCity()))
				.andExpect(jsonPath("$[0].task.lat").isNotEmpty())
				.andExpect(jsonPath("$[0].task.lon").isNotEmpty())
				.andExpect(
						jsonPath("$[0].task.requestedDays.[*].date")
								.value(hasItem(((Workday) workdaysRequested[0]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.requestedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysRequested[0]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.requestedDays.[*].date")
								.value(hasItem(((Workday) workdaysRequested[1]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.requestedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysRequested[1]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.requestedDays.[*].date")
								.value(hasItem(((Workday) workdaysRequested[2]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.requestedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysRequested[2]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.requestedDays.[*].date")
								.value(hasItem(((Workday) workdaysRequested[3]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.requestedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysRequested[3]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.requestedDays.[*].date")
								.value(hasItem(((Workday) workdaysRequested[4]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.requestedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysRequested[4]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.guaranteedDays.[*].date")
								.value(hasItem(((Workday) workdaysGuaranteed[2]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.guaranteedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysGuaranteed[2]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.guaranteedDays.[*].date")
								.value(hasItem(((Workday) workdaysGuaranteed[1]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.guaranteedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysGuaranteed[1]).getWorkingHours())))
				.andExpect(
						jsonPath("$[0].task.guaranteedDays.[*].date")
								.value(hasItem(((Workday) workdaysGuaranteed[0]).getDate().toString())))
				.andExpect(jsonPath("$[0].task.guaranteedDays[*].workingHours")
						.value(hasItem(((Workday) workdaysGuaranteed[0]).getWorkingHours())))

				.andExpect(jsonPath("$[0].task.guaranteedDaysByHelper").isEmpty())
				.andExpect(jsonPath("$[0].task.helpers").isEmpty())

				.andExpect(jsonPath("$[1].id").value(offer2.getId()))
				.andExpect(
						jsonPath("$[1].guaranteedDays.[*].date")
								.value(hasItem(((Workday) workdaysGuaranteed[0]).getDate().toString())))
				.andExpect(
						jsonPath("$[1].guaranteedDays.[*].workingHours")
								.value(hasItem(((Workday) workdaysGuaranteed[0]).getWorkingHours())))
				.andExpect(
						jsonPath("$[1].guaranteedDays.[*].date")
								.value(hasItem(((Workday) workdaysGuaranteed[1]).getDate().toString())))
				.andExpect(
						jsonPath("$[1].guaranteedDays.[*].workingHours")
								.value(hasItem(((Workday) workdaysGuaranteed[1]).getWorkingHours())))
				.andExpect(
						jsonPath("$[1].guaranteedDays.[*].date")
								.value(hasItem(((Workday) workdaysGuaranteed[2]).getDate().toString())))
				.andExpect(jsonPath("$[1].guaranteedDays.[*].workingHours")
						.value(hasItem(((Workday) workdaysGuaranteed[2]).getWorkingHours())));
	}


	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void getOfferForTask_ER_204() throws Exception {

		Task task = generateEntities.createAndSaveTask();

		restUserExtendMockMvc.perform(get("/api/offers/byTask/" + task.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}


	@Test
	@Transactional
	@WithMockUser(roles = "ADMIN", username = "user")
	public void getOfferByTaskIdOfUserAsUserWithRoleADMIN() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		restUserExtendMockMvc.perform(get("/api/offers/byTask/" + task.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "ANONYMOUS", username = "user")
	public void getOfferByTaskIdOfUserAsUserWithRoleANONYMOUS() throws Exception {

		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
		Task task = generateEntities.createAndSaveTask();
		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helper);

		restUserExtendMockMvc.perform(get("/api/offers/byTask/" + task.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void getForeignOfferByTaskIdOfUserAsUserWithRoleHelper() throws Exception {

		Helper helperOwn = generateEntities.createAndSaveHelperForCurrentUser();
		Helper helperForeign = generateEntities.createAndSaveHelperForNewUser();
		Task task = generateEntities.createAndSaveTask();

		Offer offer = generateEntities.createAndSaveOfferForTaskAndHelper(task, helperForeign);

		restUserExtendMockMvc.perform(get("/api/offers/byTask/" + task.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "HELPER", username = "user")
	public void getOfferByTaskIdOfUserAsUserWithRoleHelper() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();

		Farm farm = generateEntities.createAndSaveFarmForCurrentUser();
		Task task1 = generateEntities.createAndSaveTask(farm.getAddress(), farm);
		Task task2 = generateEntities.createAndSaveTask(farm.getAddress(), farm);

		Offer offer = generateEntities
				.createAndSaveOfferForTaskAndHelper(task1, helper);
		generateEntities.createAndSaveOfferForTaskAndHelper(task2, helper);

		Set<Workday> requestedDays = task1.getRequestedDays();
		Workday[] requestDaysArray = requestedDays.toArray(new Workday[requestedDays.size()]);

		Set<Workday> guaranteedDays = offer.getGuaranteedDays();
		Workday[] guaranteedDaysArray = guaranteedDays.toArray(new Workday[guaranteedDays.size()]);

		restUserExtendMockMvc.perform(get("/api/offers/byTask/" + task1.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(offer.getId()))
				.andExpect(
						jsonPath("$.guaranteedDays.[0].date")
								.value(guaranteedDaysArray[0].getDate().toString()))
				.andExpect(jsonPath("$.guaranteedDays.[0].workingHours")
						.value(guaranteedDaysArray[0].getWorkingHours()))
				.andExpect(
						jsonPath("$.guaranteedDays.[1].date")
								.value(guaranteedDaysArray[1].getDate().toString()))
				.andExpect(jsonPath("$.guaranteedDays.[1].workingHours")
						.value(guaranteedDaysArray[1].getWorkingHours()))
				.andExpect(
						jsonPath("$.guaranteedDays.[2].date")
								.value(guaranteedDaysArray[2].getDate().toString()))
				.andExpect(jsonPath("$.guaranteedDays.[2].workingHours")
						.value(guaranteedDaysArray[2].getWorkingHours()))
				.andExpect(jsonPath("$.task.id").isNotEmpty())
				.andExpect(jsonPath("$.task.farmId").value(farm.getId()))
				.andExpect(jsonPath("$.task.farmName").value(task1.getFarm().getName()))
				.andExpect(jsonPath("$.task.title").value(task1.getTitle()))
				.andExpect(jsonPath("$.task.taskDescription").value(task1.getDescription()))
				.andExpect(jsonPath("$.task.howToFindUs").value(task1.getHowToFindUs()))
				.andExpect(jsonPath("$.task.phone").value(task1.getPhone()))
				.andExpect(jsonPath("$.task.email").value(task1.getContactEmail()))
				.andExpect(
						jsonPath("$.task.street").value(task1.getAddress().getStreet()))
				.andExpect(jsonPath("$.task.houseNr").value(task1.getAddress().getHouseNr()))
				.andExpect(jsonPath("$.task.zip").value(task1.getAddress().getZip()))
				.andExpect(jsonPath("$.task.city").value(task1.getAddress().getCity()))
				.andExpect(jsonPath("$.task.lat").isNotEmpty())
				.andExpect(jsonPath("$.task.lon").isNotEmpty())
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[0].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[0].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[1].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[1].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[2].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[2].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[3].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[3].getWorkingHours())))
				.andExpect(jsonPath("$.task.requestedDays.[*].date")
						.value(hasItem(requestDaysArray[4].getDate().toString())))
				.andExpect(jsonPath("$.task.requestedDays.[*].workingHours")
						.value(hasItem(requestDaysArray[4].getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].date")
						.value(hasItem(guaranteedDaysArray[2].getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[2].getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].date")
						.value(hasItem(guaranteedDaysArray[1].getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays.[*].workingHours")
						.value(hasItem(guaranteedDaysArray[1].getWorkingHours())))
				.andExpect(
						jsonPath("$.task.guaranteedDays.[*].date")
								.value(hasItem(guaranteedDaysArray[0].getDate().toString())))
				.andExpect(jsonPath("$.task.guaranteedDays[*].workingHours")
						.value(hasItem(guaranteedDaysArray[0].getWorkingHours())))
				.andExpect(jsonPath("$.task.guaranteedDaysByHelper").isEmpty())
				.andExpect(jsonPath("$.task.helpers").isEmpty());
	}

	@Test
	@Transactional
	@WithMockUser(roles = "FARM", username = "user")
	public void getOfferByTaskIdOfUserAsUserWithRoleFarm() throws Exception {
		Helper helper = generateEntities.createAndSaveHelperForCurrentUser();
		Farm farm = generateEntities.createAndSaveFarmForCurrentUser();
		Task task1 = generateEntities.createAndSaveTask(farm.getAddress(), farm);
		Task task2 = generateEntities.createAndSaveTask(farm.getAddress(), farm);

		Offer offer = generateEntities
				.createAndSaveOfferForTaskAndHelper(task1, helper);
		generateEntities.createAndSaveOfferForTaskAndHelper(task2, helper);

		restUserExtendMockMvc.perform(get("/api/offers/byTask/" + task1.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	private CreateOfferPayload createOfferPayload(Task task,
			String[] guaranteedDaysArray, int[] guaranteedHoursArray) {
		CreateOfferPayload createofferPayload = new CreateOfferPayload();
		createofferPayload.setTaskId(task.getId());
		createofferPayload
				.setGuaranteedDays(getGuaranteedDaysAsPayload(guaranteedDaysArray, guaranteedHoursArray));
		return createofferPayload;
	}

	List<WorkdayPayload> getGuaranteedDaysAsPayload(String[] guaranteedDaysArray,
			int[] guaranteedHoursArray) {
		List<WorkdayPayload> guaranteedDays = new ArrayList<>();
		for (int i = 0; i < guaranteedDaysArray.length; i++) {
			WorkdayPayload wd = new WorkdayPayload();
			wd.setDate(LocalDate.parse(guaranteedDaysArray[i]));
			wd.setWorkingHours(guaranteedHoursArray[i]);
			guaranteedDays.add(wd);
		}
		return guaranteedDays;
	}

}
