package de.ernteerfolg.service.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import de.ernteerfolg.service.web.rest.Routinen.GenerateEntities;
import java.util.HashSet;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.transaction.annotation.Transactional;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.repository.AddressRepository;
import de.ernteerfolg.service.repository.HelperRepository;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.repository.UserRepository;
import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.uc.offermakeorupdate.InputPortMakeOrUpdateAnOffer;
import de.ernteerfolg.service.web.api.model.CreateFarmPayload;
import de.ernteerfolg.service.web.api.model.FarmPayload;

/**
 * Integration tests for the {@link de.ernteerfolg.service.web.api.FarmsApiDelegate} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class FarmResourceIT {

	private static final String NAME = "Max Farm";
	private static final String NAME_UPDATED = "Max´s Farmville";
	private static final String CONTACT_EMAIL = "Mustermann@maxfarm.com";
  private static final String PHONE = "12345 125637";
  private static final String STREET = "Farm-Musterstraße";
  private static final String HOUSE_NR = "3.1";
  private static final String ZIP = "81829";
  private static final String CITY = "Farm-Musterort";
  private static final String FARM_DESCRIPTION = "Super große Spargel Farm";

  @Autowired
  private de.ernteerfolg.service.repository.FarmRepository farmRepository;

  @Autowired
  private MockMvc restUserExtendMockMvc;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AddressRepository addressRepository;

  @Autowired
  private TaskRepository taskRepository;

  @Autowired
  private HelperRepository helperRepository;

  @Autowired
  private InputPortMakeOrUpdateAnOffer useCaseMakeAnOffer;

  @Autowired
  private GenerateEntities generateEntities;

  @Autowired
  UserService userService;

  @Test
  @Transactional
  @WithMockUser(roles = "FARM", username = "user")
  public void findFarmByUserIsCurrentUserNotExists() throws Exception {
    restUserExtendMockMvc.perform(get("/api/farms/byUserIsCurrentUser")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  @WithMockUser(roles = "HELPER", username = "user")
  public void getFarmByIdAsUserWithRoleHelper() throws Exception {

    Helper helper = new Helper();
    helper.setUser(userService.getExpectedUserWithAuthorities());
    helperRepository.save(helper);

    Farm farm = buildFarm();
    addressRepository.save(farm.getAddress());
    farm = farmRepository.save(farm);

    restUserExtendMockMvc.perform(get("/api/farms/" + farm.getId())
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(farm.getId()))
	        .andExpect(jsonPath("$.name").value(NAME))
	        .andExpect(jsonPath("$.description").value(FARM_DESCRIPTION))
	        .andExpect(jsonPath("$.contactMail").isEmpty())
	        .andExpect(jsonPath("$.phone").isEmpty())
	        .andExpect(jsonPath("$.street").isEmpty())
	        .andExpect(jsonPath("$.houseNr").isEmpty())
	        .andExpect(jsonPath("$.zip").isEmpty())
	        .andExpect(jsonPath("$.city").isEmpty())
	        .andExpect(jsonPath("$.lat").isEmpty())
	        .andExpect(jsonPath("$.lon").isEmpty());
	}

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void getFarmByIdAsUserWithRoleHelperWithOffer() throws Exception {

      Helper helper = new Helper();
      helper.setUser(userService.getExpectedUserWithAuthorities());
      helperRepository.save(helper);

      Farm farm = generateEntities.createAndSaveFarm();

      Task task = new Task();
      task.setFarm(farm);
      task.setActive(true);
      taskRepository.save(task);

      Offer offer = new Offer();
      offer.setHelper(helper);
      offer.setTask(task);

      useCaseMakeAnOffer.makeAnOffer(helper, new HashSet<>(), task.getId());
      task.setActive(true);
      taskRepository.save(task);

      restUserExtendMockMvc.perform(get("/api/farms/" + farm.getId())
          .accept(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.id").value(farm.getId()))
          .andExpect(jsonPath("$.name").value(farm.getName()))
          .andExpect(jsonPath("$.contactMail").value(farm.getContactEmail()))
          .andExpect(jsonPath("$.phone").value(farm.getPhone()))
          .andExpect(jsonPath("$.street").value(farm.getAddress().getStreet()))
          .andExpect(jsonPath("$.houseNr").value(farm.getAddress().getHouseNr()))
          .andExpect(jsonPath("$.zip").value(farm.getAddress().getZip()))
          .andExpect(jsonPath("$.city").value(farm.getAddress().getCity()))
          .andExpect(jsonPath("$.description").value(farm.getAdditionalInformation()));
    }

    @Test
    @Transactional
    @WithMockUser(roles="ANONYMOUS")
    public void getFarmByIdAsUserWithRoleAnonymous() throws Exception {

    	Farm farm = buildFarm();
    	addressRepository.save(farm.getAddress());
     	farm = farmRepository.save(farm);

        restUserExtendMockMvc.perform(get("/api/farms/" + farm.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(farm.getId()))
	        .andExpect(jsonPath("$.name").value(NAME))
	        .andExpect(jsonPath("$.description").value(FARM_DESCRIPTION))
	        .andExpect(jsonPath("$.contactMail").isEmpty())
	        .andExpect(jsonPath("$.phone").isEmpty())
	        .andExpect(jsonPath("$.street").isEmpty())
	        .andExpect(jsonPath("$.houseNr").isEmpty())
	        .andExpect(jsonPath("$.zip").isEmpty())
	        .andExpect(jsonPath("$.city").isEmpty())
	        .andExpect(jsonPath("$.lat").isEmpty())
	        .andExpect(jsonPath("$.lon").isEmpty());
	}

	private Farm buildFarm() {
    Farm farm = new Farm();
    farm.setName(NAME);
    farm.setContactEmail(CONTACT_EMAIL);
    farm.setPhone(PHONE);
    farm.setAdditionalInformation(FARM_DESCRIPTION);

    Address address = new Address();
    address.setStreet(STREET);
    address.setHouseNr(HOUSE_NR);
    address.setZip(ZIP);
    address.setCity(CITY);
    farm.setAddress(address);
    return farm;
  }

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void getFarmByIdAsUserWithRoleFarmForignFarm() throws Exception {

    	Farm farm = buildFarm();
    	addressRepository.save(farm.getAddress());
     	farm = farmRepository.save(farm);

		restUserExtendMockMvc.perform(get("/api/farms/" + farm.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(farm.getId()))
    	        .andExpect(jsonPath("$.name").value(NAME))
    	        .andExpect(jsonPath("$.description").value(FARM_DESCRIPTION))
    	        .andExpect(jsonPath("$.contactMail").isEmpty())
    	        .andExpect(jsonPath("$.phone").isEmpty())
    	        .andExpect(jsonPath("$.street").isEmpty())
    	        .andExpect(jsonPath("$.houseNr").isEmpty())
    	        .andExpect(jsonPath("$.zip").isEmpty())
    	        .andExpect(jsonPath("$.city").isEmpty())
    	        .andExpect(jsonPath("$.lat").isEmpty())
    	        .andExpect(jsonPath("$.lon").isEmpty());
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void getFarmByIdAsUserWithRoleFarmOwnFarm() throws Exception {

    	Farm farm = buildFarm();
    	farm.setUser(userService.getExpectedUserWithAuthorities());
    	addressRepository.save(farm.getAddress());
     	farm = farmRepository.save(farm);

		restUserExtendMockMvc.perform(get("/api/farms/" + farm.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(farm.getId()))
    	        .andExpect(jsonPath("$.name").value(NAME))
    	        .andExpect(jsonPath("$.contactMail").value(CONTACT_EMAIL))
    	        .andExpect(jsonPath("$.phone").value(PHONE))
    	        .andExpect(jsonPath("$.street").value(STREET))
    	        .andExpect(jsonPath("$.houseNr").value(HOUSE_NR))
    	        .andExpect(jsonPath("$.zip").value(ZIP))
    	        .andExpect(jsonPath("$.city").value(CITY))
    	        .andExpect(jsonPath("$.description").value(FARM_DESCRIPTION));
	}

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void findFarmByUserIsCurrentUserAndHelper() throws Exception {
     	restUserExtendMockMvc.perform(get("/api/farms/byUserIsCurrentUser")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
    	        .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
    	    	.andExpect(jsonPath("$.title").value("Forbidden"))
    	    	.andExpect(jsonPath("$.status").value(403))
    	    	.andExpect(jsonPath("$.detail").value("Access is denied"))
    	    	.andExpect(jsonPath("$.path").value("/api/farms/byUserIsCurrentUser"))
    	    	.andExpect(jsonPath("$.message").value("error.http.403"));
    }

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void findFarmByUserIsCurrentUserAndFarm() throws Exception {

    	Optional<String> userEmail = SecurityUtils.getCurrentUserLogin();
    	Optional<User> userOptional = userRepository.findOneByLogin(userEmail.get());

    	User user = userOptional.get();

        Address address = new Address();
        address.setStreet(STREET);
        address.setHouseNr(HOUSE_NR);
        address.setZip(ZIP);
        address.setCity(CITY);
        address = addressRepository.save(address);

        Farm farm = new Farm();
        farm.setAddress(address);
        farm.setUser(user);
        farm.setName(NAME);
        farm.setContactEmail(CONTACT_EMAIL);
     	farm.setPhone(PHONE);

     	farm = farmRepository.save(farm);

     	restUserExtendMockMvc.perform(get("/api/farms/byUserIsCurrentUser")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void createFarmAsUserWithRoleFarm() throws Exception {
        int databaseSizeBeforeCreate = farmRepository.findAll().size();

        CreateFarmPayload createFarmPayload = buildFarmPayload();

        restUserExtendMockMvc.perform(post("/api/farms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
	        .andExpect(jsonPath("$.name").value(NAME))
	        .andExpect(jsonPath("$.contactMail").value(CONTACT_EMAIL))
	        .andExpect(jsonPath("$.phone").value(PHONE))
	        .andExpect(jsonPath("$.street").value(STREET))
	        .andExpect(jsonPath("$.houseNr").value(HOUSE_NR))
	        .andExpect(jsonPath("$.zip").value(ZIP))
	        .andExpect(jsonPath("$.city").value(CITY))
	        .andExpect(jsonPath("$.description").value(FARM_DESCRIPTION))
	        .andExpect(jsonPath("$.lat").exists())
	        .andExpect(jsonPath("$.lon").exists())
	        ;
	    int databaseSizeAfterCreate = farmRepository.findAll().size();

	    assertThat(databaseSizeBeforeCreate+1 == databaseSizeAfterCreate);
    }

	@Test
	@Transactional
	@WithMockUser(roles="HELPER", username="user")
	public void createFarmAsUserWithRoleHelper() throws Exception {

	    CreateFarmPayload createFarmPayload = buildFarmPayload();

	    restUserExtendMockMvc.perform(post("/api/farms")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
	        .andExpect(status().isForbidden())
	        .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
	    	.andExpect(jsonPath("$.title").value("Forbidden"))
	    	.andExpect(jsonPath("$.status").value(403))
	    	.andExpect(jsonPath("$.detail").value("Access is denied"))
	    	.andExpect(jsonPath("$.path").value("/api/farms"))
	    	.andExpect(jsonPath("$.message").value("error.http.403"));
	}

	@Test
	@Transactional
	@WithMockUser(roles="FARM", username="user")
	public void reCreateFarmAsUserWithRoleFarm() throws Exception {

	    CreateFarmPayload createFarmPayload = buildFarmPayload();

	    restUserExtendMockMvc.perform(post("/api/farms")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
	        .andExpect(status().isCreated());

        // Do it again should be a BAD_REQUEST
        restUserExtendMockMvc.perform(post("/api/farms")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
                .andExpect(status().isBadRequest())
		        .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
		        .andExpect(jsonPath("$.title").value("User is already a farm!"))
		    	.andExpect(jsonPath("$.status").value(400))
		    	.andExpect(jsonPath("$.detail").doesNotExist())
		    	.andExpect(jsonPath("$.message").value("error.alreadyAFarm"))
		    	.andExpect(jsonPath("$.params").value("Farm"));
	}

    @Test
	@Transactional
	@WithMockUser(roles="FARM", username="user")
	public void updateFarmAsUserWithRoleFarm() throws Exception {

        Farm farm = new Farm();
        farm.setUser(userService.getExpectedUserWithAuthorities());
        farm = farmRepository.save(farm);

   		CreateFarmPayload createFarmPayload = buildFarmPayload();
		createFarmPayload.setName(NAME_UPDATED);

		restUserExtendMockMvc.perform(put("/api/farms")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
                .andExpect(status().isOk())
    	    	.andExpect(jsonPath("$.name").value(NAME_UPDATED))
    	    	.andExpect(jsonPath("$.contactMail").value(CONTACT_EMAIL))
    	    	.andExpect(jsonPath("$.phone").value(PHONE))
    	    	.andExpect(jsonPath("$.street").value(STREET))
    	    	.andExpect(jsonPath("$.houseNr").value(HOUSE_NR))
    	    	.andExpect(jsonPath("$.zip").value(ZIP))
    	    	.andExpect(jsonPath("$.city").value(CITY))
    	    	.andExpect(jsonPath("$.description").value(FARM_DESCRIPTION))
    	    	.andExpect(jsonPath("$.lat").exists())
    	    	.andExpect(jsonPath("$.lon").exists());
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="admin")
    public void updateFarmAsUserWithRoleFarmNotMy() throws Exception {

        Farm farm = new Farm();
        farm.setUser(userService.getUserWithAuthoritiesByLogin("user").get());
        farm = farmRepository.save(farm);

        CreateFarmPayload createFarmPayload = buildFarmPayload();
        createFarmPayload.setName(NAME_UPDATED);

        restUserExtendMockMvc.perform(put("/api/farms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
            .andExpect(jsonPath("$.title").value("No farm logged in."))
            .andExpect(jsonPath("$.message").value("error.noFarm"));

    }

	@Test
	@Transactional
	@WithMockUser(roles="FARM,HELPER", username="user")
	public void createFarmAsUserWithRoleFarmAndHelper() throws Exception {

	    CreateFarmPayload createFarmPayload = buildFarmPayload();
	    restUserExtendMockMvc.perform(post("/api/farms")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
	        .andExpect(status().isForbidden())
	        .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
	    	.andExpect(jsonPath("$.title").value("Forbidden"))
	    	.andExpect(jsonPath("$.status").value(403))
	    	.andExpect(jsonPath("$.detail").value("Access is denied"))
	    	.andExpect(jsonPath("$.path").value("/api/farms"))
	    	.andExpect(jsonPath("$.message").value("error.http.403"));
	}

	@Test
	@Transactional
	@WithMockUser(roles="", username="user")
	public void createFarmAsUserWithNoRole() throws Exception {

	    CreateFarmPayload createFarmPayload = buildFarmPayload();

	    restUserExtendMockMvc.perform(post("/api/farms")
	        .contentType(MediaType.APPLICATION_JSON)
	        .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
	        .andExpect(status().isForbidden())
	        .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
	    	.andExpect(jsonPath("$.title").value("Forbidden"))
	    	.andExpect(jsonPath("$.status").value(403))
	    	.andExpect(jsonPath("$.detail").value("Access is denied"))
	    	.andExpect(jsonPath("$.path").value("/api/farms"))
	    	.andExpect(jsonPath("$.message").value("error.http.403"));
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void createFarmAsUserWithRoleFarmAndLongDescription() throws Exception {
        CreateFarmPayload createFarmPayload = buildFarmPayload();

        createFarmPayload.setDescription(RandomStringUtils.random(3000,true,true));
        restUserExtendMockMvc.perform(post("/api/farms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
            .andExpect(status().isCreated())
            .andExpect(descriptionInDatabase(createFarmPayload.getDescription()));

    }


    private ResultMatcher descriptionInDatabase(String description) {
        return mvcResult -> {
            String json = mvcResult.getResponse().getContentAsString();
            FarmPayload farmPayload = (FarmPayload) TestUtil.convertJSONStringToObject(json, FarmPayload.class);
            assertThat(farmRepository.getOne(farmPayload.getId()).getAdditionalInformation())
                .isEqualTo(description);
        };
    }

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void createFarmAsUserWithRoleFarmAndToLongDescription() throws Exception {
        CreateFarmPayload createFarmPayload = buildFarmPayload();

        createFarmPayload.setDescription(RandomStringUtils.random(3400,true,true));
        restUserExtendMockMvc.perform(post("/api/farms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(createFarmPayload)))
            .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/constraint-violation"))
            .andExpect(jsonPath("$.title").value("Method argument not valid"))
            .andExpect(jsonPath("$.status").value(400))
            .andExpect(jsonPath("$.message").value("error.validation"))
            .andExpect(jsonPath("$.path").value("/api/farms"))
            .andExpect(jsonPath("$.fieldErrors[0].objectName").value("createFarmPayload"))
            .andExpect(jsonPath("$.fieldErrors[0].field").value("description"))
            .andExpect(jsonPath("$.fieldErrors[0].message").value("Size"));

    }

	private CreateFarmPayload buildFarmPayload() {
		CreateFarmPayload createFarmPayload = new CreateFarmPayload();
        createFarmPayload.setName(NAME);
        createFarmPayload.setContactMail(CONTACT_EMAIL);
        createFarmPayload.setStreet(STREET);
     	createFarmPayload.setHouseNr(HOUSE_NR);
     	createFarmPayload.setZip(ZIP);
     	createFarmPayload.setCity(CITY);
     	createFarmPayload.setPhone(PHONE);
     	createFarmPayload.setDescription(FARM_DESCRIPTION);
		return createFarmPayload;
	}

}
