package de.ernteerfolg.service.web.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.lang3.StringUtils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import de.ernteerfolg.service.ErnteErfolgServiceApp;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.repository.FarmRepository;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.web.api.FarmsApiDelegate;
import de.ernteerfolg.service.web.api.FarmsApiDelegateImpl;

@SpringBootTest(classes = ErnteErfolgServiceApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class UseCaseDeleteFarmIT {
	
	@Autowired
	UserService userService; 
	
	@Autowired
	FarmsApiDelegate farmsApiDelegate;
	
	@Autowired
	FarmRepository farmRepository;  

	@Autowired
    private MockMvc restUserExtendMockMvc;
	
    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void testFarmsDeleteAsFarm_noFarm() throws Exception {

        restUserExtendMockMvc.perform(delete("/api/farms")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
	        .andExpect(jsonPath("$.type").value("https://www.jhipster.tech/problem/problem-with-message"))
	    	.andExpect(jsonPath("$.title").value("no farm found for user"))
	    	.andExpect(jsonPath("$.status").value(400))
	    	.andExpect(jsonPath("$.entityName").value("FARM"))
	    	.andExpect(jsonPath("$.errorKey").value("farmNotFound"))
	    	.andExpect(jsonPath("$.message").value("error.farmNotFound"));
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void testFarmsDeleteAsFarm_success() throws Exception {

    	Farm farm = new Farm();
    	farm.setUser(userService.getExpectedUserWithAuthorities());
    	farmRepository.save(farm);
    	
        restUserExtendMockMvc.perform(delete("/api/farms")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
	}

    @Test
    @Transactional
    @WithMockUser(roles="HELPER", username="user")
    public void testFarmsDeleteAsHelper() throws Exception {

        restUserExtendMockMvc.perform(delete("/api/farms")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
	}

    @Test
    @Transactional
    @WithMockUser(roles="FARM", username="user")
    public void testFarmsDelete_confirmDeletion_success() throws Exception {

    	Farm farm = new Farm();
    	farm.setUser(userService.getExpectedUserWithAuthorities());
    	farm = farmRepository.save(farm);
    	
    	farmsApiDelegate.farmsDelete();
    	
    	User currentUser = userService.getExpectedUserWithAuthorities();
    	assertThat(StringUtils.isNotBlank(currentUser.getDeletionKey()));
    	
    	restUserExtendMockMvc.perform(get("/api/farms/confirmDeletion?key={deletionKey}", currentUser.getDeletionKey()))
    		.andExpect(status().isOk());
    	
    	try {
			currentUser = userService.getExpectedUserWithAuthorities();
		} catch (IllegalStateException e) {
			assertThat(e.getMessage().equals("No user logged in."));
		}
    	
		try {
			farmRepository.getOne(farm.getId());
		} catch (Exception e) {
			assertThat(e.getMessage().equals("Unable to find de.ernteerfolg.service.domain.Farm with id 1001"));
		}
	}
}
