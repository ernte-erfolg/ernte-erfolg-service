package de.ernteerfolg.service.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import de.ernteerfolg.service.web.rest.TestUtil;

public class WorkdayTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Workday.class);
        Workday workday1 = new Workday();
        workday1.setId(1L);
        Workday workday2 = new Workday();
        workday2.setId(workday1.getId());
        assertThat(workday1).isEqualTo(workday2);
        workday2.setId(2L);
        assertThat(workday1).isNotEqualTo(workday2);
        workday1.setId(null);
        assertThat(workday1).isNotEqualTo(workday2);
    }
}
