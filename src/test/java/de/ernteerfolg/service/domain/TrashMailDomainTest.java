package de.ernteerfolg.service.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import de.ernteerfolg.service.web.rest.TestUtil;

public class TrashMailDomainTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrashMailDomain.class);
        TrashMailDomain trashMailDomain1 = new TrashMailDomain();
        trashMailDomain1.setId(1L);
        TrashMailDomain trashMailDomain2 = new TrashMailDomain();
        trashMailDomain2.setId(trashMailDomain1.getId());
        assertThat(trashMailDomain1).isEqualTo(trashMailDomain2);
        trashMailDomain2.setId(2L);
        assertThat(trashMailDomain1).isNotEqualTo(trashMailDomain2);
        trashMailDomain1.setId(null);
        assertThat(trashMailDomain1).isNotEqualTo(trashMailDomain2);
    }
}
