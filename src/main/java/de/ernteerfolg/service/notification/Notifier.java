package de.ernteerfolg.service.notification;

import de.ernteerfolg.service.service.NotificationService;

/**
 * This interface is used to select all implementations to which the {@link NotificationResolver} should delegate. 
 * When using the Interface {@link NotificationService}, we would also address the {@link NotificationResolver} 
 * implementation and we would run in a dead lock. 
 */
interface Notifier extends NotificationService {

}
