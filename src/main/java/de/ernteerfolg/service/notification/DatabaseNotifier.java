package de.ernteerfolg.service.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;

public class DatabaseNotifier implements Notifier {

	private final Logger log = LoggerFactory.getLogger(DatabaseNotifier.class);
	
	@Override
	public void notifyUserAboutActivationLink(User user) {
		
		log.debug( "notifyUserAboutActivationLink("+user.getEmail()+")" );
		// TODO Auto-generated method stub -> https://ernteerfolg.atlassian.net/browse/ER-289
	}

	@Override
	public void notifyFarmAboutAccountDeletionLink(User user) {

		log.debug("notifyFarmAboutAccountDeletionLink(" + user.getEmail() + ")");
		// TODO Auto-generated method stub -> https://ernteerfolg.atlassian.net/browse/ER-289
	}

	@Override
	public void notifyHelperAboutDeletedTask(User user, Offer offer) {

		log.debug("notifyHelperAboutDeletedTask(" + user.getEmail() + ")");
		// TODO Auto-generated method stub -> https://ernteerfolg.atlassian.net/browse/ER-289
	}

	@Override
	public void notifyHelperAboutDeactivatedTask(User user, Offer offer) {
		log.debug("notifyHelperAboutDeactivatedTask(" + user.getEmail() + ")");
		// TODO Auto-generated method stub -> https://ernteerfolg.atlassian.net/browse/ER-289
	}

	@Override
	public void notifyFarmAboutNewOffer(User user, Offer offer) {
		log.debug("notifyFarmAboutNewOffer(" + user.getEmail() + ")");
		// TODO Auto-generated method stub
	}
}
