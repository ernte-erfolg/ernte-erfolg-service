package de.ernteerfolg.service.notification;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.service.NotificationService;
import de.ernteerfolg.service.uc.deletefarm.OutputPortUserNotification;

@Service
public class NotificationResolver implements NotificationService, OutputPortUserNotification,
		de.ernteerfolg.service.uc.deaktivatetask.OutputPortUserNotification,
		de.ernteerfolg.service.uc.offermakeorupdate.OutputPortUserNotification {

	@Autowired
	private List<Notifier> notifiers;

	@Override
	public void notifyUserAboutActivationLink(User user) {
		notifiers.forEach(notifier -> notifier.notifyUserAboutActivationLink(user));
	}

	@Override
	public void notifyFarmAboutAccountDeletionLink(User user) {
		notifiers.forEach(notifier -> notifier.notifyFarmAboutAccountDeletionLink(user));
	}

	@Override
	public void notifyHelperAboutDeletedTask(User user, Offer offer) {
		notifiers.forEach(notifier -> notifier.notifyHelperAboutDeletedTask(user, offer));
	}

	@Override
	public void notifyHelperAboutDeactivatedTask(User user, Offer offer) {
		notifiers.forEach(notifier -> notifier.notifyHelperAboutDeactivatedTask(user, offer));
	}

	@Override
	public void notifyFarmAboutNewOffer(User user, Offer offer) {
		notifiers.forEach(notifier -> notifier.notifyFarmAboutNewOffer(user, offer));
	}
}
