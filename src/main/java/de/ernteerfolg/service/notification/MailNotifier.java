package de.ernteerfolg.service.notification;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;

import io.github.jhipster.config.JHipsterProperties;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
class MailNotifier implements Notifier{

    private final Logger log = LoggerFactory.getLogger(MailNotifier.class);

    private static final String BASE_URL = "baseUrl";

    private static final String USER = "user";
    private static final String TASK = "task";
    private static final String OFFER = "offer";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailNotifier(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
            MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart,
                StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", to, e);
        }
    }

    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        sendEmailFromTemplate(user, templateName, titleKey, Collections.emptyMap());
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey,
        Map<String, Object> values) {
        if (user.getEmail() == null) {
            log.error("Email doesn't exist for user '{}'", user.getLogin());
            return;
        }
        if (user.getLangKey() == null) {
            log.error("Language Key doesn't exist for user '{}'", user.getLogin());
            return;
        }
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());

        for (String key : values.keySet()) {
            context.setVariable(key, values.get(key));
        }

        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Override
    public void notifyUserAboutActivationLink(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }

    @Override
    public void notifyHelperAboutDeletedTask(User user, Offer offer) {
        log.debug("Sending a notify helper about deleted task email to '{}'", user.getEmail());

        Map<String, Object> values = new HashMap<>();
        values.put(OFFER, offer);
        values.put(TASK, offer.getTask());

        sendEmailFromTemplate(user, "mail/notifyHelperAboutDeletedTaskEmail",
            "email.helper.task.deleted.title",
            values);
    }

    @Override
    public void notifyFarmAboutAccountDeletionLink(User user) {
        log.debug("Sending ask farm to really delete account email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/askFarmToReallyDeleteAccount", "email.farm.delete.title");
    }

    @Override
    public void notifyHelperAboutDeactivatedTask(User user, Offer offer) {
        log.debug("Sending a notify helper about deactivated task email to '{}'", user.getEmail());

        Map<String, Object> values = new HashMap<>();
        values.put(OFFER, offer);
        values.put(TASK, offer.getTask());

        sendEmailFromTemplate(user, "mail/notifyHelperAboutDeactivatedTaskEmail",
            "email.helper.task.deactivated.title",
            values);
    }

    @Override
    public void notifyFarmAboutNewOffer(User user, Offer offer) {
        log.debug("Sending a notify helper about deactivated task email to '{}'", user.getEmail());

        Map<String, Object> values = new HashMap<>();
        values.put(OFFER, offer);
        values.put(TASK, offer.getTask());

        sendEmailFromTemplate(user, "mail/notifyFarmAboutNewOffer",
            "email.farm.task.new.offer.title", values);
    }
}
