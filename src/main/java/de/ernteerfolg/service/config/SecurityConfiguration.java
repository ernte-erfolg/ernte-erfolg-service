package de.ernteerfolg.service.config;

import de.ernteerfolg.service.security.AuthoritiesConstants;
import de.ernteerfolg.service.security.jwt.JWTConfigurer;
import de.ernteerfolg.service.security.jwt.TokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.web.filter.CorsFilter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final TokenProvider tokenProvider;

    private final CorsFilter corsFilter;
    private final SecurityProblemSupport problemSupport;

  public SecurityConfiguration(TokenProvider tokenProvider, CorsFilter corsFilter,
      SecurityProblemSupport problemSupport) {
        this.tokenProvider = tokenProvider;
        this.corsFilter = corsFilter;
        this.problemSupport = problemSupport;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/h2-console/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .csrf()
            .disable()
            .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
            .exceptionHandling()
                .authenticationEntryPoint(problemSupport)
                .accessDeniedHandler(problemSupport)
        .and()
            .headers()
            .contentSecurityPolicy("default-src 'self'; frame-src 'self' data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://storage.googleapis.com; style-src 'self' 'unsafe-inline'; img-src 'self' data:; font-src 'self' data:")
        .and()
            .referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
        .and()
            .featurePolicy("geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'none'; camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; fullscreen 'self'; payment 'none'")
        .and()
            .frameOptions()
            .deny()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET,"/api/kpi/complete").permitAll()
            .antMatchers(HttpMethod.POST,"/api/farms").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.PUT,"/api/farms").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.DELETE,"/api/farms").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.GET,"/api/farms/byUserIsCurrentUser").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.POST,"/api/offers").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers(HttpMethod.PUT,"/api/offers/*").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers(HttpMethod.GET,"/api/offers").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers(HttpMethod.GET,"/api/offers/**").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers(HttpMethod.GET,"/api/tasks/search/**").hasAnyAuthority(AuthoritiesConstants.HELPER,AuthoritiesConstants.ANONYMOUS)
            .antMatchers(HttpMethod.GET,"/api/tasks/byFarm/*").hasAnyAuthority(AuthoritiesConstants.HELPER,AuthoritiesConstants.FARM,AuthoritiesConstants.ANONYMOUS)
            .antMatchers(HttpMethod.GET,"/api/tasks/{id}").permitAll()
            .antMatchers(HttpMethod.GET,"/api/tasks/*").hasAnyAuthority(AuthoritiesConstants.HELPER,AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.POST,"/api/tasks").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.PUT,"/api/tasks/*/deactivate").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.PUT,"/api/tasks/*/activate").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.PUT,"/api/tasks/*").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.GET,"/api/helpers/byUserIsCurrentUser").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers(HttpMethod.GET,"/api/helpers/**").hasAuthority(AuthoritiesConstants.FARM)
            .antMatchers(HttpMethod.PUT,"/api/helpers/**").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers(HttpMethod.POST,"/api/helpers").hasAuthority(AuthoritiesConstants.HELPER)
            .antMatchers("/api/users ").denyAll()
            .antMatchers(HttpMethod.GET,"/api/account/authenticate").denyAll()
            .antMatchers(HttpMethod.POST,"/api/account/authenticate").permitAll()
            .antMatchers("/api/account/register").permitAll()
            .antMatchers("/api/account/activate").permitAll()
            .antMatchers("/api/account/reset-password/init").permitAll()
            .antMatchers("/api/account/reset-password/finish").permitAll()
            .antMatchers("/api/**").authenticated()

            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/info").permitAll()
            .antMatchers("/management/prometheus").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
        .and()
            .httpBasic()
        .and()
            .apply(securityConfigurerAdapter());
        // @formatter:on
    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }
}
