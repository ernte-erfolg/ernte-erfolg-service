package de.ernteerfolg.service.config;

public class ErnteErfolgFeatures {

	private Boolean trashMailFilter;

	public ErnteErfolgFeatures() {
	}

	public Boolean getTrashMailFilter() {
		return trashMailFilter;
	}

	public void setTrashMailFilter(Boolean trashMailFilter) {
		this.trashMailFilter = trashMailFilter;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trashMailFilter == null) ? 0 : trashMailFilter.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErnteErfolgFeatures other = (ErnteErfolgFeatures) obj;
		if (trashMailFilter == null) {
			if (other.trashMailFilter != null)
				return false;
		} else if (!trashMailFilter.equals(other.trashMailFilter))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ErnteErfolgFeatures [trashMailFilter=" + trashMailFilter + "]";
	}
}
