package de.ernteerfolg.service.web.rest.errors;

public class UnknownZipException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public UnknownZipException() {
        //super(ErrorConstants.DEFAULT_TYPE, "We do not know that postal code.", "Address", "unknownZip", "zip");
    	super(ErrorConstants.DEFAULT_TYPE, "Diese Postleitzahl ist uns nicht bekannt.", "Address", "unknownZip", "zip");
    }
}
