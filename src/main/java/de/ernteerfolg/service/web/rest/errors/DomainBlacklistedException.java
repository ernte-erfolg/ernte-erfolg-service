package de.ernteerfolg.service.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class DomainBlacklistedException extends AbstractThrowableProblem {

	private static final long serialVersionUID = -8396507322999553696L;

	public DomainBlacklistedException(String token) {
        super(ErrorConstants.DOMAIN_BLACKLISTED_TYPE, "Domain blacklisted: " + token, Status.BAD_REQUEST);
    }
}
