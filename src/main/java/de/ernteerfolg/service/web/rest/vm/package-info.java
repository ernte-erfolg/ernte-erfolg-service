/**
 * View Models used by Spring MVC REST controllers.
 */
package de.ernteerfolg.service.web.rest.vm;
