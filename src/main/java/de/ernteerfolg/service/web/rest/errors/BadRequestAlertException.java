package de.ernteerfolg.service.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class BadRequestAlertException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    private final String entityName;

    private final String errorKey;

    public BadRequestAlertException(String defaultMessage, String entityName, String errorKey) {
        this(ErrorConstants.DEFAULT_TYPE, defaultMessage, entityName, errorKey, "");
    }

    public BadRequestAlertException(String defaultMessage, String entityName, String errorKey, String fieldName) {
        this(ErrorConstants.DEFAULT_TYPE, defaultMessage, entityName, errorKey, fieldName);
    }

    public BadRequestAlertException(URI type, String defaultMessage, String entityName, String errorKey) {
    	this(type, defaultMessage, entityName, errorKey, "");
    }
    
    public BadRequestAlertException(URI type, String defaultMessage, String entityName, String errorKey, String fieldName) {
        super(type, defaultMessage, Status.BAD_REQUEST, null, null, null, getAlertParameters(entityName, fieldName, errorKey));
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String fieldName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("fieldName", fieldName);
        parameters.put("params", entityName);
        return parameters;
    }
}
