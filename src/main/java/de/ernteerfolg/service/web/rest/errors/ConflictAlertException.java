package de.ernteerfolg.service.web.rest.errors;

import java.net.URI;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ConflictAlertException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public ConflictAlertException(final URI type,
            final String title,
            final String detail) {
        super(type, title, Status.CONFLICT, detail);
    }

}
