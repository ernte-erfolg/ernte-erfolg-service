package de.ernteerfolg.service.web.api;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.web.api.model.CreateTaskPayload;
import de.ernteerfolg.service.web.api.model.HelperPayload;
import de.ernteerfolg.service.web.api.model.TaskPayload;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;

@Service

class MapperTask {

	 Task toTask(Address address, Set<Workday> requestedDays, Farm farm, CreateTaskPayload createTaskPayload) {

		Task task = new Task();

		Optional<Farm> farmOptional = Optional.ofNullable(farm);
		task.setFarm( farmOptional.orElseThrow(() -> new BadRequestAlertException("Farm missing.", "Farm", "farmMissing") ));

		task.setAddress(address);
		task.setContactEmail(createTaskPayload.getEmail());
		task.setDescription(createTaskPayload.getTaskDescription());
		task.setHowToFindUs(createTaskPayload.getHowToFindUs());
		task.setPhone(createTaskPayload.getPhone());
		task.setRequestedDays(requestedDays);
		task.setTitle(createTaskPayload.getTitle());

		return task;
	}

	TaskPayload toTaskPayloadWithoutAddress(
			Task task,
			List<WorkdayPayload> guaranteedDays,
			List<HelperPayload> helpers,
			Map<String, List<WorkdayPayload>> guaranteedDaysByHelper,
			List<WorkdayPayload> requestedDays) {

    	TaskPayload taskPayload = new TaskPayload();

		if(task.getFarm() != null) { taskPayload.setFarmId(task.getFarm().getId()); }
		taskPayload.setId(task.getId());
		taskPayload.setFarmName(task.getFarm().getName());
    	taskPayload.setTitle(task.getTitle());
    	taskPayload.setEmail(task.getContactEmail());
    	taskPayload.setPhone(task.getPhone());
    	taskPayload.setTaskDescription(task.getDescription());
    	taskPayload.setHowToFindUs(task.getHowToFindUs());
    	taskPayload.setGuaranteedDays(guaranteedDays);
    	taskPayload.setHelpers(helpers);
    	taskPayload.setGuaranteedDaysByHelper(guaranteedDaysByHelper);
    	taskPayload.setRequestedDays(requestedDays);
    	taskPayload.setActive(task.isActive());

		return taskPayload;
	}
}
