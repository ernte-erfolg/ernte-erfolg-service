package de.ernteerfolg.service.web.api;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.neovisionaries.i18n.LanguageCode;

import de.ernteerfolg.service.domain.Authority;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.security.AuthoritiesConstants;
import de.ernteerfolg.service.web.api.model.CreateUserPayload;
import de.ernteerfolg.service.web.api.model.ProfileTypeEnum;
import de.ernteerfolg.service.web.api.model.UserPayload;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;
import de.ernteerfolg.service.web.rest.vm.ManagedUserVM;

@Service
public class MapperUser {

	public ManagedUserVM toManagedUserVM(CreateUserPayload createUserPayload) {
		ManagedUserVM managedUserVM = new ManagedUserVM();
		managedUserVM.setEmail(createUserPayload.getEmail());
		managedUserVM.setLogin(createUserPayload.getEmail());
		managedUserVM.setPassword(createUserPayload.getPassword());
		managedUserVM.setLangKey(toLanguageCode(createUserPayload.getLangKey()));
		return managedUserVM;
	}

	private LanguageCode toLanguageCode(String langKey) {
		LanguageCode languageCode = LanguageCode.getByCodeIgnoreCase(langKey);
		if(languageCode == null) {
			throw new BadRequestAlertException("Unknown LanguageCode: " + langKey, "User", "UnknownLanguageCode");
		}
		return languageCode;
	}

	public UserPayload toUserPayload(User user) {
		UserPayload userPayload = mapBaseAttributes(user);
		
		Optional<String> farm = user.getAuthorities().stream()
                .map(Authority::getName)
                .filter(name -> AuthoritiesConstants.FARM.equals(name))
                .findFirst();

		Optional<String> helper = user.getAuthorities().stream()
                .map(Authority::getName)
                .filter(name -> AuthoritiesConstants.HELPER.equals(name))
                .findFirst();

        
		if( farm.isPresent() ) {
			userPayload.setUserClass(ProfileTypeEnum.FARM);
		} else if( helper.isPresent() ) {
			userPayload.setUserClass(ProfileTypeEnum.HELPER);
		} else {
			throw new RuntimeException("user is no " + ProfileTypeEnum.HELPER.name() + " and no " + ProfileTypeEnum.FARM.name() );
		}
		return userPayload;
	}
	
	private UserPayload mapBaseAttributes(User user) {
		UserPayload userPayload = new UserPayload();
		userPayload.setActivated(user.getActivated());
		userPayload.setEmail(user.getEmail());
		userPayload.setLangKey(user.getLangKey());
		userPayload.setId(user.getId());
		return userPayload;
	}
}
