package de.ernteerfolg.service.web.api;

import de.ernteerfolg.service.domain.*;
import de.ernteerfolg.service.repository.*;
import de.ernteerfolg.service.service.FarmService;
import de.ernteerfolg.service.service.HelperService;
import de.ernteerfolg.service.uc.deaktivatetask.InputPortDeactivateTask;
import de.ernteerfolg.service.web.rest.errors.ForbiddenException;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.service.TaskService;
import de.ernteerfolg.service.service.dto.TasksInAreaDTO;
import de.ernteerfolg.service.service.plz.ZipGeoService;
import de.ernteerfolg.service.web.api.model.CreateTaskPayload;
import de.ernteerfolg.service.web.api.model.TaskPayload;
import de.ernteerfolg.service.web.api.model.TasksInAreaPayload;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;
import de.ernteerfolg.service.web.rest.errors.UnknownZipException;
import io.github.jhipster.web.util.HeaderUtil;

@Service
public class TasksApiDelegateImpl implements TasksApiDelegate {

    private final Logger log = LoggerFactory.getLogger(TasksApiDelegateImpl.class);

    private static final String ENTITY_NAME = "task";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private TaskRepository taskRepository;

    private FarmRepository farmRepository;

    private MapperAddress mapperAddress;

    private MapperTask mapperTask;

    private AddressRepository addressRepository;

    private MapperWorkday mapperWorkday;

    private OfferRepository offerRepository;

    private MapperHelper mapperHelper;

    private ZipGeoService zipGeoService;

    private WorkdayRepository workdayRepository;

    private TaskService taskService;

    private HelperService helperService;

    private InputPortDeactivateTask inputPortDeactivateTask;

    private final FarmService farmService;

    private final UserRepository userRepository;

    public TasksApiDelegateImpl(MapperTask mapperTask, MapperAddress mapperAddress,
                                TaskRepository taskRepository, FarmService farmService, FarmRepository farmRepository, AddressRepository addressRepository,
                                MapperWorkday mapperWorkday, OfferRepository offerRepository, MapperHelper mapperHelper,
                                ZipGeoService zipGeoService, WorkdayRepository workdayRepository, TaskService taskService,
                                HelperService helperService, InputPortDeactivateTask inputPortDeactivateTask, UserRepository userRepository) {
        this.mapperTask = mapperTask;
        this.mapperAddress = mapperAddress;
        this.taskRepository = taskRepository;
        this.farmRepository = farmRepository;
        this.farmService = farmService;
        this.addressRepository = addressRepository;
        this.mapperWorkday = mapperWorkday;
        this.offerRepository = offerRepository;
        this.mapperHelper = mapperHelper;
        this.zipGeoService = zipGeoService;
        this.workdayRepository = workdayRepository;
        this.taskService = taskService;
        this.helperService = helperService;
        this.inputPortDeactivateTask = inputPortDeactivateTask;
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<TaskPayload> createTask(CreateTaskPayload createTaskPayload) {

        log.debug("REST request to create Task : {}", createTaskPayload);

        if(workingdayInThePastIsIncluded(null, createTaskPayload.getRequestedDays())){
			throw new BadRequestAlertException("Some Workday lays in the past, only workdays in future can be created",
					ENTITY_NAME, "validation.dayInPast", "createTaskPayload.workdayPayload.date");
        }

        Task result = toSavedTask(null, createTaskPayload);
        return ResponseEntity.created(URI.create("/tasks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(toTaskPayload(result));
    }

    @Override
    public ResponseEntity<List<TaskPayload>> getAllTasksByFarm(Long farmId) {

        if (farmId == null) {
            throw new BadRequestAlertException("farmerId is mandatory", ENTITY_NAME,
                "missingFarmerId");
        }

		if (!isCurrentUsersFarmMatchingWithFarmId(farmId)) {
			throw new ForbiddenException("A farmer cannot see tasks from other farms", ENTITY_NAME,
					"farmIsNotTaskOwner");
		}

        List<Task> filtered = taskRepository.getAllByFarmId(farmId);
        filtered = filtered.stream()
            .filter(task -> task.isActive() || SecurityUtils.isCurrentUserFarm() && isCurrentUsersFarmMatchingWithFarmId(task.getFarm().getId()))
            .collect(Collectors.toList());

        List<TaskPayload> result = new ArrayList<>();
        for (Task task : filtered) {
            result.add(toTaskPayload(task));
        }

        return ResponseEntity.ok().body(result);
    }

    @Override
    public ResponseEntity<TaskPayload> getTask(Long id) {
        Optional<TaskPayload> taskPayloadOptional = getPureTaskPayload(id);

		if (taskPayloadOptional.isPresent() && !isCurrentUsersFarmMatchingWithFarmId(
				taskPayloadOptional.get().getFarmId())) {
			throw new ForbiddenException("A farmer cannot see tasks from other farms", ENTITY_NAME,
					"farmIsNotTaskOwner");
		}
		if(!taskPayloadOptional.isPresent()){
		 return ResponseEntity.notFound().build();
        }
        TaskPayload taskPayload = taskPayloadOptional.get();
        if(!SecurityUtils.isAuthenticated() || !SecurityUtils.isCurrentUserFarm()) {
            if (!SecurityUtils.isAuthenticated() || !isCurrentUserHelperAndHasAnOffer(taskPayload.getFarmId())) {
                taskPayload.setEmail("");
                taskPayload.setPhone("");
            }
        }

        return ResponseEntity.ok(taskPayload);
    }

    private boolean isCurrentUserHelperAndHasAnOffer(Long farmId) {
        Optional<Helper> helper = helperService.getHelperForCurrentUser();
        return helper.filter(value -> helperService.getHelperOfFarm(farmId).contains(value)).isPresent();
    }

    public Optional<TaskPayload> getPureTaskPayload(Long id) {
        Optional<Task> taskOptional = ((CrudRepository)taskRepository).findById(id);
        return taskOptional.map(this::toTaskPayload);
    }

	boolean isCurrentUsersFarmMatchingWithFarmId(Long farmId) {
		if (SecurityUtils.isCurrentUserFarm() && farmId != null) {
			Optional<Farm> farmForCurrentUserIsOwner = farmService.getFarmForCurrentUserIsOwner();
			return (farmForCurrentUserIsOwner.isPresent() && farmForCurrentUserIsOwner.get().getId()
					.equals(farmId));
		}
		return true; //ignore check
	}

    @Override
    public ResponseEntity<TaskPayload> updateTask(Long id, CreateTaskPayload createTaskPayload) {
        log.debug("REST request to update Task : {}", createTaskPayload);
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<Task> optTaskInDatabase = ((CrudRepository)taskRepository).findById(id);
        if (!optTaskInDatabase.isPresent()) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnotExists");
        }

        if (!isCurrentUsersFarmMatchingWithFarmId(optTaskInDatabase.get().getFarm().getId())) {
            throw new ForbiddenException("A farmer cannot update tasks from other farms",
                ENTITY_NAME,
                "farmIsNotTaskOwner");
        }
        if (workingdayInThePastIsIncluded(id, createTaskPayload.getRequestedDays())) {
            throw new BadRequestAlertException(
                "Some Workday lays in the past, only workdays in future can be created",
                ENTITY_NAME, "validation.dayInPast", "createTaskPayload.workdayPayload.date");
        }

        if (!canTaskUpdate(id, createTaskPayload)) {
            throw new ForbiddenException("You cannot update a active task", ENTITY_NAME,
                "taskIsActive");
        }

        TaskPayload result = toTaskPayload(toSavedTask(id, createTaskPayload));

        return ResponseEntity.ok()
            .headers(HeaderUtil
                .createEntityUpdateAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .body(result);
    }

    private boolean canTaskUpdate(Long id, CreateTaskPayload createTaskPayload) {
        Task task = taskRepository.getTaskById(id);
        if (!task.isActive()) {
            return true;
        }
        Boolean workDays =  createTaskPayload.getRequestedDays().containsAll(mapperWorkday.toWorkdayPayloadList(task.getRequestedDays()));
        return  workDays;
    }

    public boolean workingdayInThePastIsIncluded(Long id, List<WorkdayPayload> listOfWorkdays) {
        if (listOfWorkdays == null || listOfWorkdays.isEmpty()) return false;

        if(id != null){
            listOfWorkdays = new ArrayList<>(listOfWorkdays);
            listOfWorkdays.removeAll(mapperWorkday
                .toWorkdayPayloadList(taskRepository.getOne(id).getRequestedDays()));
        }
        if(listOfWorkdays.isEmpty()) return false;
        return  listOfWorkdays.stream().min(Comparator.comparing(WorkdayPayload::getDate))
            .get().getDate().compareTo(LocalDate.now()) < 0;
    }

    private TaskPayload toTaskPayload(Task task) {

        Map<Helper, List<WorkdayPayload>> guaranteedDaysByHelperObjects = getGuaranteedDaysByHelper(
            task.getId());

        TaskPayload taskPayload = mapperTask.toTaskPayloadWithoutAddress(
            task,
            getGuaranteedDays(guaranteedDaysByHelperObjects),
            guaranteedDaysByHelperObjects.keySet().stream()
                .map(helper -> mapperHelper.toHelperPayload(helper)).collect(Collectors.toList()),
            convertKeyOfMapToHelperId(guaranteedDaysByHelperObjects),
            mapperWorkday.toWorkdayPayloadList(task.getRequestedDays()));

        mapperAddress.fillAddress(task.getAddress(), taskPayload);

        if (!SecurityUtils.isCurrentUserFarm() ||
            hasToHideHelperForDataProtectionReasons(taskPayload.getRequestedDays())) {
            taskPayload.setHelpers(Collections.emptyList());
            taskPayload.setGuaranteedDaysByHelper(Collections.emptyMap());
            taskPayload.setHiddenHelpers(true);
				}

        return taskPayload;
    }

    private boolean hasToHideHelperForDataProtectionReasons(List<WorkdayPayload> requestedDays) {
        if (requestedDays.isEmpty()) return true;
        return requestedDays.stream()
            .max(Comparator.comparing(WorkdayPayload::getDate))
            .get()
            .getDate()
            .plusDays(90L)
            .compareTo(LocalDate.now()) < 0;
    }

    private Map<String, List<WorkdayPayload>> convertKeyOfMapToHelperId(
        Map<Helper, List<WorkdayPayload>> guaranteedDaysByHelperObjects) {
        Map<String, List<WorkdayPayload>> result = new HashMap<>();
        for (Entry<Helper, List<WorkdayPayload>> entry : guaranteedDaysByHelperObjects.entrySet()) {
            result.put(entry.getKey().getId().toString(), entry.getValue());
        }
        return result;
    }

    private List<WorkdayPayload> getGuaranteedDays(
        Map<Helper, List<WorkdayPayload>> guaranteedDaysByHelper) {

        // flatten the list of lists
        List<WorkdayPayload> workdayPayloadList =
            guaranteedDaysByHelper.values().stream()
                .collect(ArrayList::new, List::addAll, List::addAll);

        // init the map with zero hours per day
        Map<LocalDate, Integer> hoursPerDay = new HashMap<>();
        for (WorkdayPayload workdayPayload : workdayPayloadList) {
            hoursPerDay.put(workdayPayload.getDate(), 0);
        }

        // calculate the sum of hours per day
        for (WorkdayPayload workdayPayload : workdayPayloadList) {
            Integer bisher = hoursPerDay.get(workdayPayload.getDate());
            hoursPerDay.put(workdayPayload.getDate(), bisher + workdayPayload.getWorkingHours());
        }

        // create a list of workdays with the calculated sum of hours per day
        List<WorkdayPayload> result = new ArrayList<>();
        for (Entry<LocalDate, Integer> entry : hoursPerDay.entrySet()) {
            result.add(createWorkdayPayload(entry.getKey(), entry.getValue()));
        }
        return result;
    }

    private WorkdayPayload createWorkdayPayload(LocalDate date, int hours) {
        WorkdayPayload workdayPayload = new WorkdayPayload();
        workdayPayload.setDate(date);
        workdayPayload.setWorkingHours(hours);
        return workdayPayload;
    }

    private Map<Helper, List<WorkdayPayload>> getGuaranteedDaysByHelper(Long taskId) {

        Map<Helper, List<WorkdayPayload>> result = new HashMap<>();

        List<Offer> offers = offerRepository.findByTaskId(taskId);

        for (Offer offer : offers) {
            Helper offerHelper = offer.getHelper();
            if (!result.containsKey(offerHelper)) {
                result.put(offerHelper, new ArrayList<>());
            }
            List<WorkdayPayload> workdayListOfHelper = result.get(offerHelper);
            Set<Workday> guaranteedDays = offer.getGuaranteedDays();
            workdayListOfHelper.addAll(mapperWorkday.toWorkdayPayloadList(guaranteedDays));
        }

        return result;
    }

    private Task toSavedTask(Long id, CreateTaskPayload createTaskPayload) {
        Optional<Farm> farmerOptional = farmRepository.findOneByUserIsCurrentUser();
        Farm farm = farmerOptional.orElseThrow(() -> new BadRequestAlertException(
            "Current User has no farm", ENTITY_NAME,
            "unknownFarm."));


        Set<Workday> requestedDaysSet = mapperWorkday
            .toWorkday(createTaskPayload.getRequestedDays());
        checkWorkdaysToHaveUniqueDays(requestedDaysSet);

        ZipGeoData zipGeoData = zipGeoService.findByZip(createTaskPayload.getZip()).orElseThrow(() -> new UnknownZipException());
        Address address = mapperAddress.toAddress(zipGeoData, createTaskPayload);
        addressRepository.save(address);

        Set<Workday> savedRequestedDaysSet = new HashSet<>();
        requestedDaysSet.stream().forEach(
            requestedDays -> savedRequestedDaysSet.add((Workday) ((JpaRepository)workdayRepository).save(requestedDays)));

        Task task = mapperTask.toTask(address, savedRequestedDaysSet, farm, createTaskPayload);
        if (id != null) {
            task.setId(id);
        } else {
            task.setActive(true);
        }
        task = taskRepository.save(task);
        // set the relation from workday to task
        for (Workday workday : task.getRequestedDays()) {
            workday.setTask(task);
            ((JpaRepository<Workday, Long>)workdayRepository).save(workday);
        }

        return task;
    }

    private void checkWorkdaysToHaveUniqueDays(Set<Workday> requestedDaysSet) {
        Set<LocalDate> set = new HashSet<>();
        boolean areDaysUnique = requestedDaysSet.stream()
            .allMatch(workday -> set.add(workday.getDate()));
        if (!areDaysUnique) {
            throw new BadRequestAlertException("There multiple workdays with same day", ENTITY_NAME,
                "duplicateWorkdays.");
        }
    }

    @Override
    public ResponseEntity<List<TasksInAreaPayload>> searchTasksByZip(String zip, LocalDate withWorkingDaysGreaterThen) {
        Optional<ZipGeoData> zipGeoDataOptional = zipGeoService.findByZip(zip);
        if (zipGeoDataOptional.isPresent()) {
            ZipGeoData zipGeoData = zipGeoDataOptional.get();
            if (withWorkingDaysGreaterThen == null) {
                withWorkingDaysGreaterThen = LocalDate.now();
            }
            return searchTasksByZip(withWorkingDaysGreaterThen, zipGeoData.getLatitude(),
                zipGeoData.getLongitude());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private Map<Long, Task> returnOnlyTasksWhereCurrentUserHasAOffer(List<Long> taskIds) {
        if (SecurityUtils.isCurrentUserHelper()) {
            Optional<Helper> helperForCurrentUser = helperService.getHelperForCurrentUser();
            if (helperForCurrentUser.isPresent()) {
                List<Offer> helperOffersForTasks = offerRepository
                    .findAllByHelper_IdAndTask_IdIn(helperForCurrentUser.get().getId(), taskIds);
                Map<Long, Task> taskWhereHelperHasAOffer = helperOffersForTasks.stream()
                    .map(Offer::getTask)
                    .collect(Collectors.toMap(Task::getId, Function.identity()));
                return taskWhereHelperHasAOffer;
            }
        }
        return Collections.emptyMap();
    }


    private ResponseEntity<List<TasksInAreaPayload>> searchTasksByZip(
        LocalDate withWorkingDaysGreaterThen,
        BigDecimal lat, BigDecimal lon) {

        Set<TasksInAreaPayload> tasksInAreaPayloadSet = new HashSet<>();
        try {

            List<TasksInAreaDTO> tasksInAreasList = taskService
                .searchTasks(true, withWorkingDaysGreaterThen, lat, lon);
            List<Long> taskIds = getTaskIdsOfAllTasksInArea(tasksInAreasList);

            for (TasksInAreaDTO tasksInArea : tasksInAreasList) {

                Map<Long, Task> taskWhereHelperHasAOffer = returnOnlyTasksWhereCurrentUserHasAOffer(
                    taskIds);

                TasksInAreaPayload tasksInAreaPayload = new TasksInAreaPayload();
                tasksInAreaPayload.setMaxDistance(tasksInArea.getMaxDistance());

                List<TaskPayload> taskPayloadList = new ArrayList<>();
                for (Task task : tasksInArea.getTaskList()) {
                    if (!taskWhereHelperHasAOffer.containsKey(task.getId())) {
                        task.setPhone("");
                        task.setContactEmail("");
                    }
                    if (task.isActive() || isCurrentUsersFarmMatchingWithFarmId(task.getFarm().getId())) {
                        taskPayloadList.add(toTaskPayload(task));
                    }
                }
                tasksInAreaPayload.setTasks(taskPayloadList);

                tasksInAreaPayloadSet.add(tasksInAreaPayload);
                for (Task task : tasksInArea.getTaskList()) {
                    tasksInAreaPayload.putDistancesItem(task.getId().toString(),
                        BigDecimal.valueOf(tasksInArea.getTaskIdDistanceMap().get(task.getId())));
                }
            }

			return ResponseEntity.ok().body(new ArrayList<>(tasksInAreaPayloadSet));

        } catch (TransformException e) {
            throw new BadRequestAlertException("Invalid geo coorinates", ENTITY_NAME,
                "transformException");
        }
    }

    private List<Long> getTaskIdsOfAllTasksInArea(List<TasksInAreaDTO> tasksInAreasList) {
        List<Long> taskIds = new ArrayList<>();
        for (TasksInAreaDTO tasksInArea : tasksInAreasList) {
            for (Task task : tasksInArea.getTaskList()) {
                taskIds.add(task.getId());
            }
        }
        return taskIds;
    }

    @Override
    public ResponseEntity<TaskPayload> activateTask(Long taskId) {
        return ResponseEntity.ok(toTaskPayload(inputPortDeactivateTask.activateTask(taskId)));
    }

    @Override
    public ResponseEntity<TaskPayload> deactivateTask(Long taskId) {
        return ResponseEntity.ok(toTaskPayload(inputPortDeactivateTask.deactivateTask(taskId)));
    }

}
