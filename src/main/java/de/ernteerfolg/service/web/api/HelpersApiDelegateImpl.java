package de.ernteerfolg.service.web.api;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.ZipGeoData;
import de.ernteerfolg.service.repository.AddressRepository;
import de.ernteerfolg.service.repository.FarmRepository;
import de.ernteerfolg.service.repository.HelperRepository;
import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.service.HelperService;
import de.ernteerfolg.service.service.TaskService;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.service.plz.ZipGeoService;
import de.ernteerfolg.service.web.api.model.CreateHelperPayload;
import de.ernteerfolg.service.web.api.model.HelperPayload;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;
import de.ernteerfolg.service.web.rest.errors.UnknownZipException;
import io.github.jhipster.web.util.HeaderUtil;

@Service
public class HelpersApiDelegateImpl implements HelpersApiDelegate {

    private final Logger log = LoggerFactory.getLogger(HelpersApiDelegateImpl.class);

    private static final String ENTITY_NAME = "Helper";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
	private MapperHelper mapperHelper;
	private MapperAddress mapperAddress;
	private AddressRepository addressRepository;
	private HelperRepository helperRepository;
	private UserService userService;
	private HelperService helperService;

	private FarmRepository farmRepository;

	private ZipGeoService zipGeoService;

	private TaskService taskService;

	public HelpersApiDelegateImpl(MapperHelper mapperHelper, MapperAddress mapperAddress,
			AddressRepository addressRepository, HelperRepository helperRepository, UserService userService,
			HelperService helperService, FarmRepository farmRepository, ZipGeoService zipGeoService,
			TaskService taskService) {
		this.mapperHelper = mapperHelper;
		this.mapperAddress = mapperAddress;
		this.addressRepository = addressRepository;
		this.helperRepository = helperRepository;
		this.userService = userService;
		this.helperService = helperService;
		this.farmRepository = farmRepository;
		this.zipGeoService = zipGeoService;
		this.taskService = taskService;
	}
	
	@Override
	public ResponseEntity<HelperPayload> createHelper(CreateHelperPayload createHelperPayload) {
		
		log.debug("REST request to create Helper : {}", createHelperPayload);
		
		if( !SecurityUtils.isCurrentUserHelper() ) {
			throw new BadRequestAlertException("User is not registered as helper!", "User", "notAHelper");
		}
		if( SecurityUtils.isCurrentUserFarm() ) {
			throw new BadRequestAlertException("User has Role FARM !!", "User", "userIsAFarm");
		}
		if( helperService.getHelperForCurrentUser().isPresent() ) {
			throw new BadRequestAlertException("User is already a helper!", ENTITY_NAME, "alreadyAHelper");
		}		
		if( farmRepository.findOneByUserIsCurrentUser().isPresent() ) {
			throw new RuntimeException("User has Role.HELPER and is a Farm !!");
		}		
		
		ZipGeoData zipGeoData = zipGeoService.findByZip(createHelperPayload.getZip()).orElseThrow(() -> new UnknownZipException());
		Address address = addressRepository.save(mapperAddress.toAddress(zipGeoData, createHelperPayload));
		Helper helper = helperRepository.save(mapperHelper.toHelper(userService.getExpectedUserWithAuthorities(), address, createHelperPayload));
        return ResponseEntity.created(URI.create("/helpers/" + helper.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, "Helper", helper.getId().toString()))
                .body(mapperHelper.toHelperPayload(helper));
	}

	@Override
	public ResponseEntity<HelperPayload> getHelper(Long id) {
		
		log.debug("REST request to get Helper : {}", id);
		Optional<Helper> helperOptional = taskService.getHelperWithOfferForCurrentFarmById(id);
		if(helperOptional.isPresent()) {
			return ResponseEntity.ok(mapperHelper.toHelperPayload(helperOptional.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@Override
	public ResponseEntity<HelperPayload> updateHelper(CreateHelperPayload createHelperPayload) {
		
		log.debug("REST request to update current Helper : {}", createHelperPayload);
		Helper helper = helperService.getExpectedHelperForCurrentUser();
		ZipGeoData zipGeoData = zipGeoService.findByZip(createHelperPayload.getZip()).orElseThrow(() -> new UnknownZipException());
		Address address = updateAddress(helper.getId(), createHelperPayload, zipGeoData);
		Helper helperToSave = mapperHelper.toHelper(helper.getId(), helper.getUser(), address, createHelperPayload);
		Helper helperSaved = helperRepository.save(helperToSave);		
		return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, helperSaved.getId().toString()))
            .body(mapperHelper.toHelperPayload(helperSaved));
	}

	private Address updateAddress(Long id, CreateHelperPayload createHelperPayload, ZipGeoData zipGeoData) {
		Helper oldHelper = helperRepository.findById(id).orElseThrow(() -> new RuntimeException("cannot find helper id " + id));		
		Address address = oldHelper.getAddress();
		if(address != null) {
			Long oldAddressId = address.getId();
			address = mapperAddress.toAddress(oldAddressId, zipGeoData, createHelperPayload);
		} else {
			address = mapperAddress.toAddress(zipGeoData, createHelperPayload);
		}
		address = addressRepository.save(address);
		return address;
	}

	@Override
	public ResponseEntity<HelperPayload> getHelperByUserId() {
		
		log.debug("REST request to getHelperByUserId");
		Optional<Helper> helperOptional = helperService.getHelperForCurrentUser();
				
    	List<Helper> helpers = helperRepository.findByUserIsCurrentUser();
		if(helperOptional.isPresent()) {
			return ResponseEntity.ok().body(mapperHelper.toHelperPayload(helpers.get(0)));
		} else {
			return ResponseEntity.notFound().build();  
		}
	}
}
