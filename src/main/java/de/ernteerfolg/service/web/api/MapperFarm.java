package de.ernteerfolg.service.web.api;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.web.api.model.CreateFarmPayload;
import de.ernteerfolg.service.web.api.model.FarmPayload;

@Service
class MapperFarm {

	Farm toFarm(Long id, User user, Address address, CreateFarmPayload createFarmPayload) {
		Farm farm = toFarm(user, address, createFarmPayload);
		farm.setId(id);
		return farm;
	}

	Farm toFarm(User user, Address address, CreateFarmPayload createFarmPayload) {
		Farm farm = new Farm();
		farm.setAdditionalInformation(createFarmPayload.getDescription());
		farm.setContactEmail(createFarmPayload.getContactMail());
		farm.setName(createFarmPayload.getName());
		farm.setPhone(createFarmPayload.getPhone());
		farm.setAddress(address);
		farm.setUser(user);
		return farm;
	}

	FarmPayload toFarmPayload(Farm farm) {
		FarmPayload farmPayload = new FarmPayload();
		farmPayload.setId(farm.getId());
		farmPayload.setContactMail(farm.getContactEmail());
		farmPayload.setName(farm.getName());
		farmPayload.setPhone(farm.getPhone());
		farmPayload.setDescription(farm.getAdditionalInformation());
		Address address = farm.getAddress();
        if(address != null) {
            farmPayload.setStreet(address.getStreet());
            farmPayload.setHouseNr(address.getHouseNr());
            farmPayload.setZip(address.getZip());
            farmPayload.setCity(address.getCity());
            farmPayload.setLat(address.getLatitude());
            farmPayload.setLon(address.getLongitude());
        }
		return farmPayload;
	}

}
