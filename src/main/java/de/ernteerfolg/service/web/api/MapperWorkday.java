package de.ernteerfolg.service.web.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;

@Service
class MapperWorkday {

	Set<Workday> toWorkday(List<WorkdayPayload> workdayPayloadSet) {
		Set<Workday> workdaySet = new HashSet<>();
		for (WorkdayPayload workdayPayload : workdayPayloadSet) {
			workdaySet.add(toWorkday(workdayPayload));
		}
		return workdaySet;
	}
	
	private Workday toWorkday(WorkdayPayload workdayPayload) {
		Workday workday = new Workday();
		workday.setDate(workdayPayload.getDate());
		workday.setWorkingHours(workdayPayload.getWorkingHours());		
		return workday;
	}

	private WorkdayPayload toWorkdayPayload(Workday workday) {
		WorkdayPayload workdayPayload = new WorkdayPayload();
		workdayPayload.setDate(workday.getDate());
		workdayPayload.setWorkingHours(workday.getWorkingHours());		
		return workdayPayload;
	}

	List<WorkdayPayload> toWorkdayPayloadList(Set<Workday> requestedDays) {
		
		List<WorkdayPayload> result = new ArrayList<>();
		if(requestedDays != null) {
			for (Workday workday : requestedDays) {
				result.add( toWorkdayPayload(workday) );
			}
		}
		return result;
	}


}
