package de.ernteerfolg.service.web.api;

import de.ernteerfolg.service.uc.getkpi.UseCaseGetKpi;
import de.ernteerfolg.service.web.api.model.KpiCompletePayload;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class KpiApiDelegateImpl implements KpiApiDelegate {

    private final UseCaseGetKpi useCaseGetKpi;

    public KpiApiDelegateImpl(UseCaseGetKpi useCaseGetKpi) {
        this.useCaseGetKpi = useCaseGetKpi;
    }

    @Override
    public ResponseEntity<KpiCompletePayload> getKpis() {
        return ResponseEntity.ok(useCaseGetKpi.getKpiComplete());
    }
}
