package de.ernteerfolg.service.web.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

import de.ernteerfolg.service.uc.pictureUpload.PictureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.ZipGeoData;
import de.ernteerfolg.service.repository.AddressRepository;
import de.ernteerfolg.service.repository.FarmRepository;
import de.ernteerfolg.service.service.FarmService;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.service.plz.ZipGeoService;
import de.ernteerfolg.service.uc.deletefarm.DeleteFarmState;
import de.ernteerfolg.service.uc.deletefarm.InputPortDeleteFarm;
import de.ernteerfolg.service.web.api.model.CreateFarmPayload;
import de.ernteerfolg.service.web.api.model.FarmPayload;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;
import de.ernteerfolg.service.web.rest.errors.UnknownZipException;
import io.github.jhipster.web.util.HeaderUtil;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FarmsApiDelegateImpl implements FarmsApiDelegate {

    private final Logger log = LoggerFactory.getLogger(FarmsApiDelegateImpl.class);

    private static final String ENTITY_NAME = "Farm";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
	private FarmRepository farmRepository;
	private MapperAddress mapperAddress;
	private MapperFarm mapperFarm;

	private final AddressRepository addressRepository;

	private final UserService userService;

	private final FarmService farmService;

	private final ZipGeoService zipGeoService;

	private final InputPortDeleteFarm useCaseDeleteFarm;


    public FarmsApiDelegateImpl(MapperAddress mapperAddress, MapperFarm mapperFarm, FarmRepository farmRepository,
                                AddressRepository addressRepository, UserService userService,
                                FarmService farmService, ZipGeoService zipGeoService, InputPortDeleteFarm deleteFarm, PictureService pictureService) {
		this.mapperAddress = mapperAddress;
		this.mapperFarm = mapperFarm;
		this.farmRepository = farmRepository;
		this.addressRepository = addressRepository;
		this.userService = userService;
		this.farmService = farmService;
		this.zipGeoService = zipGeoService;
		this.useCaseDeleteFarm = deleteFarm;
    }

	@Override
	public ResponseEntity<FarmPayload> createFarm(CreateFarmPayload createFarmPayload) {

		log.debug("REST request to create Farm : {}", createFarmPayload);
		if( farmService.isCurrentUserFarmOwner() ) {
			throw new BadRequestAlertException("User is already a farm!", ENTITY_NAME, "alreadyAFarm");
		}

		ZipGeoData zipGeoData = zipGeoService.findByZip(createFarmPayload.getZip()).orElseThrow(() -> new UnknownZipException());
		Address address = addressRepository.save(mapperAddress.toAddress(zipGeoData, createFarmPayload));
		Farm farm = mapperFarm.toFarm(userService.getExpectedUserWithAuthorities(), address, createFarmPayload);
		Farm farmSaved = farmRepository.save(farm);
        return ResponseEntity.created(URI.create("/farms/" + farmSaved.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, farmSaved.getId().toString()))
            .body(mapperFarm.toFarmPayload(farmSaved));
	}

	@Override
	public ResponseEntity<FarmPayload> getFarm(Long id) {

		log.debug("REST request to get Farm : {}", id);
		Optional<Farm> farmOptional = farmService.getFarmById(id);

		if(farmOptional.isPresent()) {
			return ResponseEntity.ok(mapperFarm.toFarmPayload(farmOptional.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@Override
	public ResponseEntity<FarmPayload> updateFarm(CreateFarmPayload createFarmPayload) {

		log.debug("REST request to update current Farm : {}", createFarmPayload);
        Farm oldFarm;
		try {
            oldFarm = farmService.getExpectedFarmForCurrentUserIsOwner();
        } catch (Exception e){
		   throw new BadRequestAlertException("No farm logged in.", ENTITY_NAME, "noFarm");
        }
		ZipGeoData zipGeoData = zipGeoService.findByZip(createFarmPayload.getZip()).orElseThrow(UnknownZipException::new);
		Address address = updateAddress(oldFarm, createFarmPayload, zipGeoData);
		Farm farm = mapperFarm.toFarm(oldFarm.getId(), userService.getExpectedUserWithAuthorities(), address, createFarmPayload);
		Farm farmSaved = farmRepository.save(farm);
		        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, farmSaved.getId().toString()))
            .body(mapperFarm.toFarmPayload(farmSaved));
	}

	private Address updateAddress(Farm oldFarm, CreateFarmPayload createFarmPayload, ZipGeoData zipGeoData) {
		Address address = oldFarm.getAddress();
		if(address != null) {
			Long oldAddressId = address.getId();
			address = mapperAddress.toAddress(oldAddressId, zipGeoData, createFarmPayload);
		} else {
			address = mapperAddress.toAddress(zipGeoData, createFarmPayload);
		}
		address = addressRepository.save(address);
		return address;
	}

	@Override
	public ResponseEntity<FarmPayload> getFarmByUserId() {
		log.debug("REST request to getFarmByUserId");
		return farmRepository.findOneByUserIsCurrentUser()
				.map(farm -> ResponseEntity.ok().body(mapperFarm.toFarmPayload(farm)))
				.orElse(ResponseEntity.notFound().build());
	}

	@Override
	public ResponseEntity<Void> confirmDeletion(String key) {
		useCaseDeleteFarm.confirmDeletion(key);
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Void> farmsDelete() {
		DeleteFarmState state = useCaseDeleteFarm.sendConfirmationMail(userService.getExpectedUserWithAuthorities());
		switch (state) {
			case farmNotFound:
				throw new BadRequestAlertException("no farm found for user", "FARM", "farmNotFound");
			case userNotFound:
				throw new BadRequestAlertException("no user logged in", "FARM", "userNotFound");
			case ok:
				return ResponseEntity.ok().build();
			default:
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}


    @Override
    public ResponseEntity<Void> farmsPictureHeaderPost(MultipartFile profileImage) {
        profileImage.getContentType();
        if(!(profileImage.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) ||
            profileImage.getContentType().equals(MediaType.IMAGE_PNG_VALUE))){
            return ResponseEntity.status(406).build();
        }
        try {
            Farm farm = farmService.uploadHeader(profileImage);
            return ResponseEntity.ok().build();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Void> farmsPictureProfilePost(MultipartFile profileImage) {
        profileImage.getContentType();
        if(!(profileImage.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) ||
            profileImage.getContentType().equals(MediaType.IMAGE_PNG_VALUE))){
            return ResponseEntity.status(406).build();
        }
        try {
            Farm farm = farmService.uploadProfile(profileImage);
            return ResponseEntity.ok().build();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().body(null);

    }

    @Override
    public ResponseEntity<Resource> farmsPictureHeaderGet() {
        try {
            Path file = farmService.downloadHeader().toPath();
            Resource resource = new UrlResource(file.toUri());
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(resource);
        } catch (IOException e) {
                e.printStackTrace();
        }
        return ResponseEntity.badRequest().body(null);

    }

    @Override
    public ResponseEntity<Resource> farmsPictureProfileGet() {
            try {
                Path file = farmService.downloadProfile().toPath();
                Resource resource = new UrlResource(file.toUri());
                return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(resource);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ResponseEntity.badRequest().body(null);

    }


}
