package de.ernteerfolg.service.web.api;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.ZipGeoData;
import de.ernteerfolg.service.web.api.model.CreateFarmPayload;
import de.ernteerfolg.service.web.api.model.CreateHelperPayload;
import de.ernteerfolg.service.web.api.model.CreateTaskPayload;
import de.ernteerfolg.service.web.api.model.TaskPayload;

@Service
class MapperAddress {

	Address toAddress(ZipGeoData zipGeoData, String street, String houseNr, String zip, String city) {
		Address address = new Address();
		address.setCity(city);
		address.setHouseNr(houseNr);
		address.setStreet(street);
		address.setZip(zip);
		if(zipGeoData != null) {
			address.setLatitude(zipGeoData.getLatitude());
			address.setLongitude(zipGeoData.getLongitude());
		}
		return address;
	}

	void fillAddress(Address address, TaskPayload taskPayload) {
	
		if(address != null) {
	    	taskPayload.setStreet(address.getStreet());
	    	taskPayload.setHouseNr(address.getHouseNr());
	    	taskPayload.setZip(address.getZip());
	    	taskPayload.setCity(address.getCity());
	    	taskPayload.setLon(address.getLongitude());
	    	taskPayload.setLat(address.getLatitude());
		}
	}
	
	Address toAddress(ZipGeoData zipGeoData, CreateTaskPayload createTaskPayload) {
		return toAddress(zipGeoData, createTaskPayload.getStreet(), createTaskPayload.getHouseNr(), createTaskPayload.getZip(), createTaskPayload.getCity());
	}
	
	public Address toAddress(ZipGeoData zipGeoData, CreateHelperPayload createHelperPayload) {
		return toAddress(zipGeoData, null, null, createHelperPayload.getZip(), null);
	}

	public Address toAddress(ZipGeoData zipGeoData, CreateFarmPayload createFarmPayload) {
		return toAddress(zipGeoData, createFarmPayload.getStreet(), createFarmPayload.getHouseNr(), createFarmPayload.getZip(), createFarmPayload.getCity());
	}

	public Address toAddress(Long id, ZipGeoData zipGeoData, CreateFarmPayload createFarmPayload) {
		Address address = toAddress(zipGeoData, createFarmPayload.getStreet(), createFarmPayload.getHouseNr(), createFarmPayload.getZip(), createFarmPayload.getCity());
		address.setId(id);
		return address;
	}

	public Address toAddress(Long id, ZipGeoData zipGeoData, CreateHelperPayload createHelperPayload) {
		Address address = toAddress(zipGeoData, null, null, createHelperPayload.getZip(), null);
		address.setId(id);
		return address;
	}

}
