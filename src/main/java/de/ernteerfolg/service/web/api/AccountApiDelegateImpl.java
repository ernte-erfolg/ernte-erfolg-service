package de.ernteerfolg.service.web.api;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.config.ApplicationProperties;
import de.ernteerfolg.service.config.ErnteErfolgFeatures;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.security.jwt.JWTFilter;
import de.ernteerfolg.service.security.jwt.TokenProvider;
import de.ernteerfolg.service.service.FraudPreventionService;
import de.ernteerfolg.service.service.NotificationService;
import de.ernteerfolg.service.service.UserService;
import de.ernteerfolg.service.web.api.model.CreateUserPayload;
import de.ernteerfolg.service.web.api.model.JWTToken;
import de.ernteerfolg.service.web.api.model.LoginVM;
import de.ernteerfolg.service.web.api.model.UserPayload;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;
import de.ernteerfolg.service.web.rest.errors.DomainBlacklistedException;
import de.ernteerfolg.service.web.rest.errors.InvalidPasswordException;
import de.ernteerfolg.service.web.rest.vm.ManagedUserVM;

@Service
public class AccountApiDelegateImpl implements AccountApiDelegate {

	private final Logger log = LoggerFactory.getLogger(AccountApiDelegateImpl.class);
	private UserService userService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
	private MapperUser mapperUserPayload;
	private NotificationService notificationService;
	private ErnteErfolgFeatures features;
	private FraudPreventionService fraudPreventionService;
	private AuthenticationManagerBuilder authenticationManagerBuilder;
	private TokenProvider tokenProvider;

	public AccountApiDelegateImpl(UserService userService, MapperUser mapperUserPayload, @Qualifier("notificationResolver") NotificationService notificationService,
			ApplicationProperties applicationProperties, FraudPreventionService fraudPreventionService,
			TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
		this.userService = userService;
		this.mapperUserPayload = mapperUserPayload;
		this.notificationService = notificationService;
		this.fraudPreventionService = fraudPreventionService;
		this.tokenProvider = tokenProvider;
		this.authenticationManagerBuilder = authenticationManagerBuilder;
		features = applicationProperties.getFeatures();
	}

	@Override
	public ResponseEntity<JWTToken> authorize(LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

            Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            boolean rememberMe = (loginVM.getRememberMe() == null) ? false : loginVM.getRememberMe();
            String jwt = tokenProvider.createToken(authentication, rememberMe);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
            JWTToken jwtToken = new JWTToken();
            jwtToken.setIdToken(jwt);
            return new ResponseEntity<>(jwtToken, httpHeaders, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<UserPayload> getAccount() {

		log.debug("REST request to get the Account of the current User");
		User user = userService.getUserWithAuthorities()
                .orElseThrow(() -> new RuntimeException("User could not be found"));

		return ResponseEntity.ok().body(mapperUserPayload.toUserPayload(user));
	}

	@Override
	public ResponseEntity<Void> activateAccount(String key) {
		log.debug("REST request to activate a user account");
		Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
        	log.warn("User try to activate with key " + key + " but we didn't know that key and response with HttpStatus.UNAUTHORIZED.");
        	return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<UserPayload> registerAccount(CreateUserPayload createUserPayload) {
		log.debug("REST request to register account : {}","E-Mail " + createUserPayload.getEmail());
		if(features.getTrashMailFilter() && fraudPreventionService.isEmailBlacklisted(createUserPayload.getEmail())) {
			throw new DomainBlacklistedException(createUserPayload.getEmail());
		}

		ManagedUserVM managedUserVM = mapperUserPayload.toManagedUserVM(createUserPayload);

		User user = null;
		switch(createUserPayload.getUserClass()) {
			case FARM:
				user = userService.registerFarm(managedUserVM, managedUserVM.getPassword());
				break;
			case HELPER:
				user = userService.registerHelper(managedUserVM, managedUserVM.getPassword());
				break;
			default:
				throw new BadRequestAlertException("Profile must be a farm or helper but is: " + createUserPayload.getUserClass().name(), "User", "unknownProfileType");
		}

		notificationService.notifyUserAboutActivationLink(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(mapperUserPayload.toUserPayload(user));
	}
}
