package de.ernteerfolg.service.web.api;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.User;
import de.ernteerfolg.service.web.api.model.CreateHelperPayload;
import de.ernteerfolg.service.web.api.model.HelperPayload;

@Service
class MapperHelper {

	public Helper toHelper(Long id, User user, Address address, CreateHelperPayload createHelperPayload) {
		Helper helper = toHelper(user, address, createHelperPayload);
		helper.setId(id);
		return helper;
	}
	
	public Helper toHelper(User user, Address address, CreateHelperPayload createHelperPayload) {
		
		Helper helper = new Helper();
		helper.setAddress(address);
		helper.setUser(user);
		helper.setFirstname(createHelperPayload.getFirstName());
		helper.setLastname(createHelperPayload.getLastName());
//		helper.setLevel(level); // TODO in a Sprint > Sprint 1
		helper.setPhone(createHelperPayload.getPhone());
		return helper;
	}

	public HelperPayload toHelperPayload(Helper helper) {
		HelperPayload helperPayload = new HelperPayload();
		Address address = helper.getAddress();
		if(address != null) {
			helperPayload.setZip(address.getZip());
			helperPayload.setLat(address.getLatitude());
			helperPayload.setLon(address.getLongitude());		
		}
		helperPayload.setFirstName(helper.getFirstname());
		helperPayload.setId(helper.getId());
		helperPayload.setLastName(helper.getLastname());
		
		if(helper.getLevel() != null) {
			helperPayload.setLevel(helper.getLevel().intValue());
		}		
		helperPayload.setPhone(helper.getPhone());
		
		User user = helper.getUser();
		if(user != null) {
			helperPayload.setEmail(user.getEmail());
		}
		return helperPayload;
	}

}
