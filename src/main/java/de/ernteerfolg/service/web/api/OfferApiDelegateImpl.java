package de.ernteerfolg.service.web.api;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.service.HelperService;
import de.ernteerfolg.service.web.rest.errors.ForbiddenException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.repository.OfferRepository;
import de.ernteerfolg.service.uc.offermakeorupdate.InputPortMakeOrUpdateAnOffer;
import de.ernteerfolg.service.uc.offermakeorupdate.MakeOrUpdateAnOfferResponse;
import de.ernteerfolg.service.web.api.model.CreateOfferPayload;
import de.ernteerfolg.service.web.api.model.OfferPayload;
import de.ernteerfolg.service.web.api.model.TaskPayload;
import de.ernteerfolg.service.web.api.model.WorkdayPayload;
import de.ernteerfolg.service.web.rest.errors.ConflictAlertException;
import de.ernteerfolg.service.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;

@Service
public class OfferApiDelegateImpl implements OffersApiDelegate {

    private final Logger log = LoggerFactory.getLogger(OfferApiDelegateImpl.class);

    private static final String ENTITY_NAME = "offer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private MapperWorkday mapperWorkday;

	private  OfferRepository offerRepository;

	private TasksApiDelegateImpl tasksApiDelegate;

	private InputPortMakeOrUpdateAnOffer useCaseMakeAnOffer;

	private HelperService helperService;

	public OfferApiDelegateImpl(MapperWorkday mapperWorkday, OfferRepository offerRepository,
                                TasksApiDelegateImpl tasksApiDelegate, InputPortMakeOrUpdateAnOffer useCaseMakeAnOffer, HelperService helperService) {
		super();
		this.mapperWorkday = mapperWorkday;
        this.offerRepository = offerRepository;
		this.tasksApiDelegate = tasksApiDelegate;
		this.useCaseMakeAnOffer = useCaseMakeAnOffer;
        this.helperService = helperService;
    }

	@Override
	public ResponseEntity<OfferPayload> createOffer(CreateOfferPayload createOfferPayload) {

		log.debug("REST request to create offer : {}", createOfferPayload);

		MakeOrUpdateAnOfferResponse makeAnOfferResponse =  useCaseMakeAnOffer.makeAnOfferForCurrentHelper(mapperWorkday.toWorkday(createOfferPayload.getGuaranteedDays()), createOfferPayload.getTaskId());

		if(makeAnOfferResponse.isOk()) {
			OfferPayload offerPayload = toOfferPayload(makeAnOfferResponse.getOffer().get());

			return ResponseEntity.created(URI.create("/offers" + offerPayload.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME,
							offerPayload.getId().toString()))
					.body(offerPayload);
		}
		else {
			throw new ConflictAlertException(URI.create("/offers"), makeAnOfferResponse.getErrorKey(), "");
		}
	}

	boolean isCurrentUserHelperMatchingWithHelperId(Long helperId) {
		if (SecurityUtils.isCurrentUserHelper() && helperId != null) {
			Optional<Helper> helperForCurrentUser = helperService.getHelperForCurrentUser();
			return (helperForCurrentUser.isPresent() && helperForCurrentUser.get().getId()
					.equals(helperId));
		}
		return true; //ignore check
	}

	@Override
	public ResponseEntity<OfferPayload> getOffer(Long id) {

		log.debug("REST request to get offer : {}", id);
		Optional<Offer> offerOptional = ((CrudRepository<Offer,Long>)offerRepository).findById(id);
		if (offerOptional.isPresent()) {
			if (isCurrentUserHelperMatchingWithHelperId(offerOptional.get().getHelper().getId())) {
				return ResponseEntity.ok(toOfferPayload(offerOptional.get()));
			} else {
				throw new ForbiddenException("A helper cannot see offers from other helper", ENTITY_NAME,
						"helperIsNotOfferOwner");
			}
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@Override
	public ResponseEntity<List<OfferPayload>> getOffersOfUser() {

		log.debug("REST request to get offers of user");

		List<OfferPayload> result = new ArrayList<>();

		List<Offer> offersOfCurrentUser = offerRepository.findByHelperIsCurrentUser();

		for (Offer offer : offersOfCurrentUser) {
			result.add(toOfferPayload(offer));
		}
		return ResponseEntity.ok().body(result);
	}

	@Override
	public ResponseEntity<OfferPayload> getOfferForTask(Long taskId) {

		log.debug("REST request to get offers fro task");

		List<Offer> offersOfCurrentUser = offerRepository.findByByTaskIdAndHelperIsCurrentUser(taskId);

		if(offersOfCurrentUser.size() > 1) {
			throw new RuntimeException("there is more than one offer for this user with taskId " + taskId);
		} else if(offersOfCurrentUser == null || offersOfCurrentUser.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(toOfferPayload(offersOfCurrentUser.get(0)));
	}

	@Override
	public ResponseEntity<OfferPayload> updateOffer(Long id, CreateOfferPayload createOfferPayload) {

        log.debug("REST request to update offer : {}, {}", id, createOfferPayload);
        MakeOrUpdateAnOfferResponse makeAnOfferResponse =  useCaseMakeAnOffer.updateAnOffer(id, mapperWorkday.toWorkday(createOfferPayload.getGuaranteedDays()), createOfferPayload.getTaskId());
        if(makeAnOfferResponse.isOk()) {

            OfferPayload offerPayload = toOfferPayload(makeAnOfferResponse.getOffer().get());

            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, offerPayload.getId().toString()))
                .body(offerPayload);
        } else {
            throw new ConflictAlertException(URI.create("/offers"), makeAnOfferResponse.getErrorKey(), "");
        }
    }

	private OfferPayload toOfferPayload(Offer offer) {
		OfferPayload offerPayload = new OfferPayload();

		List<WorkdayPayload> guaranteedDays = mapperWorkday.toWorkdayPayloadList(offer.getGuaranteedDays());
		offerPayload.setGuaranteedDays(guaranteedDays);

		offerPayload.setId(offer.getId());

		TaskPayload taskPayload = tasksApiDelegate.getPureTaskPayload(offer.getTask().getId())
				.orElseThrow(() -> new NotFoundAlertException("Task with id " + offer.getTask().getId() + "not found: ", "Task", "taskNotFound"));
		offerPayload.setTask(taskPayload);
		return offerPayload;
	}
}
