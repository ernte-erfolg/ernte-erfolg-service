package de.ernteerfolg.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Offer.
 */
@Entity
@Table(name = "offer")
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "offer", fetch=FetchType.EAGER)
    private Set<Workday> guaranteedDays = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("offers")
    private Task task;

    @ManyToOne
    @JsonIgnoreProperties("offers")
    private Helper helper;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Offer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Workday> getGuaranteedDays() {
        return guaranteedDays;
    }

    public Offer guaranteedDays(Set<Workday> workdays) {
        this.guaranteedDays = workdays;
        return this;
    }

    public Offer addGuaranteedDays(Workday workday) {
        this.guaranteedDays.add(workday);
        workday.setOffer(this);
        return this;
    }

    public Offer removeGuaranteedDays(Workday workday) {
        this.guaranteedDays.remove(workday);
        workday.setOffer(null);
        return this;
    }

    public void setGuaranteedDays(Set<Workday> workdays) {
        this.guaranteedDays = workdays;
    }

    public Task getTask() {
        return task;
    }

    public Offer task(Task task) {
        this.task = task;
        return this;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Helper getHelper() {
        return helper;
    }

    public Offer helper(Helper helper) {
        this.helper = helper;
        return this;
    }

    public void setHelper(Helper helper) {
        this.helper = helper;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Offer)) {
            return false;
        }
        return id != null && id.equals(((Offer) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Offer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
