package de.ernteerfolg.service.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TrashMailDomain.
 */
@Entity
@Table(name = "trash_mail_domain")
public class TrashMailDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "domain")
    private String domain;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public TrashMailDomain domain(String domain) {
        this.domain = domain;
        return this;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrashMailDomain)) {
            return false;
        }
        return id != null && id.equals(((TrashMailDomain) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TrashMailDomain{" +
            "id=" + getId() +
            ", domain='" + getDomain() + "'" +
            "}";
    }
}
