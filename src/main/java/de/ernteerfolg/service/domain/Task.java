package de.ernteerfolg.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Task.
 */
@Entity
@Table(name = "task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "how_to_find_us")
    private String howToFindUs;

    @Column(name = "phone")
    private String phone;

    @Column(name = "contact_email")
    private String contactEmail;

    @OneToMany(mappedBy = "task", fetch=FetchType.EAGER)
    private Set<Workday> requestedDays = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tasks")
    private Address address;

    @ManyToOne
    @JsonIgnoreProperties("tasks")
    private Farm farm;

    @Column(name = "active")
    private boolean active;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Task title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Task description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowToFindUs() {
        return howToFindUs;
    }

    public Task howToFindUs(String howToFindUs) {
        this.howToFindUs = howToFindUs;
        return this;
    }

    public void setHowToFindUs(String howToFindUs) {
        this.howToFindUs = howToFindUs;
    }

    public String getPhone() {
        return phone;
    }

    public Task phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public Task contactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
        return this;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Set<Workday> getRequestedDays() {
        return requestedDays;
    }

    public Task requestedDays(Set<Workday> workdays) {
        this.requestedDays = workdays;
        return this;
    }

    public Task addRequestedDays(Workday workday) {
        this.requestedDays.add(workday);
        workday.setTask(this);
        return this;
    }

    public Task removeRequestedDays(Workday workday) {
        this.requestedDays.remove(workday);
        workday.setTask(null);
        return this;
    }

    public void setRequestedDays(Set<Workday> workdays) {
        this.requestedDays = workdays;
    }

    public Address getAddress() {
        return address;
    }

    public Task address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Farm getFarm() {
        return farm;
    }

    public Task farm(Farm farm) {
        this.farm = farm;
        return this;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Task)) {
            return false;
        }
        return id != null && id.equals(((Task) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", howToFindUs='" + getHowToFindUs() + "'" +
            ", phone='" + getPhone() + "'" +
            ", contactEmail='" + getContactEmail() + "'" +
            "}";
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
