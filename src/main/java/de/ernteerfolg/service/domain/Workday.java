package de.ernteerfolg.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;

/**
 * A Workday.
 */
@Entity
@Table(name = "workday")
public class Workday implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "working_hours")
    private Integer workingHours;

    @ManyToOne
    @JsonIgnoreProperties("requestedDays")
    private Task task;

    @ManyToOne
    @JsonIgnoreProperties("guaranteedDays")
    private Offer offer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Workday date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getWorkingHours() {
        return workingHours;
    }

    public Workday workingHours(Integer workingHours) {
        this.workingHours = workingHours;
        return this;
    }

    public void setWorkingHours(Integer workingHours) {
        this.workingHours = workingHours;
    }

    public Task getTask() {
        return task;
    }

    public Workday task(Task task) {
        this.task = task;
        return this;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Offer getOffer() {
        return offer;
    }

    public Workday offer(Offer offer) {
        this.offer = offer;
        return this;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workday)) {
            return false;
        }
        return id != null && id.equals(((Workday) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Workday{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", workingHours=" + getWorkingHours() +
            "}";
    }
}
