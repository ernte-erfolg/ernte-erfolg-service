package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.uc.deletefarm.OutputPortAddressStorage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Address entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressRepository extends JpaRepository<Address, Long>, OutputPortAddressStorage {
}
