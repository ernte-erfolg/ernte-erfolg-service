package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortTaskStorage;

import java.time.LocalDate;
import java.util.List;

import de.ernteerfolg.service.uc.getkpi.storage.OutputPortHelperStorage;
import de.ernteerfolg.service.uc.getkpi.storage.OutputPortKpiTaskStorage;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Task entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TaskRepository extends JpaRepository<Task, Long>,
    de.ernteerfolg.service.uc.deletefarm.OutputPortTaskStorage,
    de.ernteerfolg.service.uc.offermakeorupdate.OutputPortTaskStorage,
    OutputPortTaskStorage, OutputPortKpiTaskStorage {

  @Override
  @Query("select task from Task task where task.farm.user.login = ?#{principal.username}")
  List<Task> findByFarmIsCurrentUser();

  @Query ("select count (task) from Task task")
  Long countTask();

  @Override
  @SuppressWarnings("unchecked")
  Task save(Task entity);

  List<Task> getAllByFarmId(Long farmId);

  @Query(
      // @formatter:off
      value = " select tresult.* from task tresult "
            + " where tresult.id in ( "
              + " select taskfree.task_id from "
                + " ( SELECT t.id task_id, reqworkdayhours.date, coalesce(reqworkdayhours.amount, 0) as reqhours, "
                  + " coalesce(offeredworkdayhours.amount, 0) as offeredhours, "
                  + " coalesce(reqworkdayhours.amount, 0) - coalesce(offeredworkdayhours.amount, 0) diff "
                + " FROM task t "
                + " LEFT JOIN (SELECT max(working_hours) as amount, wt.task_id, wt.date FROM workday wt GROUP BY wt.task_id, wt.date) reqworkdayhours ON (reqworkdayhours.task_id= t.id)  "
                + " LEFT JOIN (SELECT sum(wo.working_hours) amount, o.task_id, wo.date FROM offer o, workday wo where o.id = wo.offer_id and wo.date > :taskWithRequestedDaysGreaterThen  GROUP BY o.task_id, wo.date) offeredworkdayhours  "
                + " ON offeredworkdayhours.task_id= t.id and reqworkdayhours.date  = offeredworkdayhours.date   "
                + " where reqworkdayhours.date  > :taskWithRequestedDaysGreaterThen"
                + " and ( t.active = :onlyActiveTasks or t.active = true )"
                + " order by date, task_id ) taskfree "
              + " where taskfree.diff > 0 "
          + ")",
      // @formatter:on
      nativeQuery = true)
  List<Task> getAllTasksThatAreActiveHasRequestedDaysGreaterAndHasStillCapacity(
      @Param("onlyActiveTasks") boolean onlyActiveTasks,
      @Param("taskWithRequestedDaysGreaterThen") LocalDate taskWithRequestedDaysGreaterThen);
}
