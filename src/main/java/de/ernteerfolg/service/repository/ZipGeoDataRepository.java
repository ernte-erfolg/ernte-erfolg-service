package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.ZipGeoData;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ZipGeoData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ZipGeoDataRepository extends JpaRepository<ZipGeoData, Long> {
	
    @Query("select zipGeoData from ZipGeoData zipGeoData where zipGeoData.zip = ?1")
    List<ZipGeoData> findByZip(String zip);
}
