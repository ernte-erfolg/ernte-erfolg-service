package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortOfferStorage;
import java.util.Collection;
import java.util.List;

import de.ernteerfolg.service.uc.getkpi.storage.OutportOfferStorage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Offer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OfferRepository extends JpaRepository<Offer, Long>,
    de.ernteerfolg.service.uc.deletefarm.OutputPortOfferStorage, OutputPortOfferStorage,
		de.ernteerfolg.service.uc.offermakeorupdate.OutputPortOfferStorage, OutportOfferStorage {

    @Query("select offer from Offer offer where offer.helper.user.login = ?#{principal.username}")
    List<Offer> findByHelperIsCurrentUser();

    @Query("select offer from Offer offer where offer.helper.user.login = ?#{principal.username} and offer.task.id = ?1")
    List<Offer> findByByTaskIdAndHelperIsCurrentUser(Long taskId);

    @Override
    @Query(value = "SELECT * FROM Offer WHERE TASK_ID = ?1", nativeQuery = true)
    List<Offer> findByTaskId(Long taskId);

    @Query("select offer from Offer offer where offer.task.farm.user.login = ?#{principal.username}")
    List<Offer> findByFarmIsCurrentUser();

    @Override
    @Query("select offer from Offer offer where offer.task.farm.id = ?1")
    List<Offer> findByFarmId(Long id);

    @Query("select offer from Offer offer where offer.helper.id = ?1")
    List<Offer> findByHelper(Long helperId);

    List<Offer> findAllByHelper_IdAndTask_IdIn(Long helperId, Collection<Long> taskIds);

    @Query("select count (offer) from Offer  offer")
    Long countOffers();

}
