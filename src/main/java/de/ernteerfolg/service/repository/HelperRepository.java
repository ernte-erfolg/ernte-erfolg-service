package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.uc.offermakeorupdate.OutputPortHelperStorage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Helper entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HelperRepository extends JpaRepository<Helper, Long>, OutputPortHelperStorage,
    de.ernteerfolg.service.uc.getkpi.storage.OutputPortHelperStorage {

    @Query("select helper from Helper helper where helper.user.login = ?#{principal.username}")
    List<Helper> findByUserIsCurrentUser();

    @Query("select helper from Helper helper where helper.user.login = ?#{principal.username}")
    Optional<Helper> findOneByUserIsCurrentUser();

    @Query("select count (helper) from Helper helper")
    Long countHelpers();

}
