package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.uc.getkpi.storage.OutputPortKpiWordayStorage;
import de.ernteerfolg.service.uc.offermakeorupdate.OutputPortWorkdayStorage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Workday entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorkdayRepository extends JpaRepository<Workday, Long>, OutputPortWorkdayStorage, OutputPortKpiWordayStorage {

    Long countWorkdaysByOfferIsNotNull();

    Long countWorkdaysByTaskIsNotNull();

}
