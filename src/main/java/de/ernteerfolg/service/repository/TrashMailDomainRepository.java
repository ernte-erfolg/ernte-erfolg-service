package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.TrashMailDomain;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TrashMailDomain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrashMailDomainRepository extends JpaRepository<TrashMailDomain, Long> {
}
