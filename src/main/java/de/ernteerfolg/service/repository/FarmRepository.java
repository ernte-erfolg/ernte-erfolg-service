package de.ernteerfolg.service.repository;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.uc.deaktivatetask.OutputPortFarmStorage;

import de.ernteerfolg.service.uc.getkpi.storage.OutputPortKpiFarmerStorage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Farm entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FarmRepository extends JpaRepository<Farm, Long>, de.ernteerfolg.service.uc.deletefarm.OutputPortFarmStorage, OutputPortFarmStorage,
    OutputPortKpiFarmerStorage {

    @Query(value = "SELECT * FROM Farm WHERE ID = 1", nativeQuery = true)
    Farm findTaskContainerForDeletedFarms();

    @Query("select farm from Farm farm where farm.user.login = ?#{principal.username}")
    Optional<Farm> findOneByUserIsCurrentUser();

    @Query("select farm from Farm farm where farm.user.login = ?#{principal.username}")
    List<Farm> findByUserIsCurrentUser();

    @Query("select count (farm) from Farm farm")
    Long countFarms();

}
