package de.ernteerfolg.service.uc.getkpi;

import de.ernteerfolg.service.uc.getkpi.storage.*;
import de.ernteerfolg.service.web.api.model.KpiCompletePayload;
import org.springframework.stereotype.Service;

@Service
public class UseCaseGetKpi {

    private final OutportOfferStorage outportOfferStorage;
    private final OutputPortHelperStorage outputPortHelperStorage;
    private final OutputPortKpiFarmerStorage outputPortKpiFarmerStorage;
    private final OutputPortKpiTaskStorage outputPortKpiTaskStorage;
    private final OutputPortKpiWordayStorage outputPortKpiWordayStorage;

    public UseCaseGetKpi(OutportOfferStorage outportOfferStorage,
                         OutputPortHelperStorage outputPortHelperStorage,
                         OutputPortKpiFarmerStorage outputPortKpiFarmerStorage,
                         OutputPortKpiTaskStorage outputPortKpiTaskStorage, OutputPortKpiWordayStorage outputPortKpiWordayStorage) {
        this.outportOfferStorage = outportOfferStorage;
        this.outputPortHelperStorage = outputPortHelperStorage;
        this.outputPortKpiFarmerStorage = outputPortKpiFarmerStorage;
        this.outputPortKpiTaskStorage = outputPortKpiTaskStorage;
        this.outputPortKpiWordayStorage = outputPortKpiWordayStorage;
    }

    public KpiCompletePayload getKpiComplete(){
        KpiCompletePayload payload =new KpiCompletePayload();
        payload.setOfferCount( outportOfferStorage.countOffers());
        payload.setHelperCount(outputPortHelperStorage.countHelpers());
        payload.setFarmerCount( outputPortKpiFarmerStorage.countFarms());
        payload.setTaskCount(outputPortKpiTaskStorage.countTask());
        payload.setOfferWorkdaysCount(outputPortKpiWordayStorage.countWorkdaysByOfferIsNotNull());
        payload.setTaskWorkdaysCount(outputPortKpiWordayStorage.countWorkdaysByTaskIsNotNull());
        return payload;
    }
}
