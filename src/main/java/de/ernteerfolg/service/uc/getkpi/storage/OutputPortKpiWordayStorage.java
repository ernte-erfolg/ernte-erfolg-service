package de.ernteerfolg.service.uc.getkpi.storage;

public interface OutputPortKpiWordayStorage {

    Long countWorkdaysByOfferIsNotNull();

    Long countWorkdaysByTaskIsNotNull();

}
