package de.ernteerfolg.service.uc.getkpi.storage;

public interface OutputPortKpiTaskStorage {

    Long countTask();
}
