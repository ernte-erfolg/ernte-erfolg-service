package de.ernteerfolg.service.uc.getkpi.storage;

public interface OutputPortHelperStorage {
    Long countHelpers();
}
