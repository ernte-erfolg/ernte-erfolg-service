package de.ernteerfolg.service.uc.getkpi.storage;

public interface OutputPortKpiFarmerStorage {

    Long countFarms();
}
