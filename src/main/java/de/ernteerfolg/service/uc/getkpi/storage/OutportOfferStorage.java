package de.ernteerfolg.service.uc.getkpi.storage;

public interface OutportOfferStorage {

    Long countOffers();

}
