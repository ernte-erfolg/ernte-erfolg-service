package de.ernteerfolg.service.uc.offermakeorupdate;

import java.util.Set;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Workday;

public interface InputPortMakeOrUpdateAnOffer {

	MakeOrUpdateAnOfferResponse makeAnOffer(Helper helper, Set<Workday> guaranteeWorkdays, Long taskId);
	MakeOrUpdateAnOfferResponse makeAnOfferForCurrentHelper(Set<Workday> guaranteeWorkdays, Long taskId);
	MakeOrUpdateAnOfferResponse updateAnOffer(Long offerId, Set<Workday> guaranteeWorkdays, Long taskId);
}
