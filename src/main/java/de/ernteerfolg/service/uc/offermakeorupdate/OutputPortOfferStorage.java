package de.ernteerfolg.service.uc.offermakeorupdate;

import java.util.List;
import java.util.Optional;

import de.ernteerfolg.service.domain.Offer;

public interface OutputPortOfferStorage {
	Optional<Offer> findById(Long id);
	List<Offer> findByHelper(Long helperId);
	List<Offer> findByFarmId(Long id);
	List<Offer> findByTaskId(Long id);
	Offer save(Offer offer);

}
