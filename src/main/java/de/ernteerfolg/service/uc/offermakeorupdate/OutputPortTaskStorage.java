package de.ernteerfolg.service.uc.offermakeorupdate;

import java.util.Optional;

import de.ernteerfolg.service.domain.Task;

public interface OutputPortTaskStorage {

	Optional<Task> findById(Long id);
	Task save(Task entity);
}
