package de.ernteerfolg.service.uc.offermakeorupdate;

import de.ernteerfolg.service.domain.Task;

class TaskAndState {
	
	MakeOrUpdateAnOfferResponse response;
	Task task;
	
	public TaskAndState(MakeOrUpdateAnOfferResponse response, Task task) {
		this.response = response;
		this.task = task;
	}

	public boolean isOk() {
		return response.getState().isOk();
	}
}
