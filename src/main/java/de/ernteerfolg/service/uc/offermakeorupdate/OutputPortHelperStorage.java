package de.ernteerfolg.service.uc.offermakeorupdate;

import java.util.Optional;

import de.ernteerfolg.service.domain.Helper;

public interface OutputPortHelperStorage {

	Optional<Helper> findOneByUserIsCurrentUser();
}
