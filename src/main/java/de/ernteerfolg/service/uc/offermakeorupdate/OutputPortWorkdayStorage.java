package de.ernteerfolg.service.uc.offermakeorupdate;

import de.ernteerfolg.service.domain.Workday;

public interface OutputPortWorkdayStorage {
	Workday save(Workday entity);
	void delete(Workday workday);
	Workday saveAndFlush(Workday workday);
}
