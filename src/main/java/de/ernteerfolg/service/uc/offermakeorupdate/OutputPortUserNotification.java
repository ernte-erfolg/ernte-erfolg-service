package de.ernteerfolg.service.uc.offermakeorupdate;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;

public interface OutputPortUserNotification {

	void notifyFarmAboutNewOffer(User user, Offer offer);

}
