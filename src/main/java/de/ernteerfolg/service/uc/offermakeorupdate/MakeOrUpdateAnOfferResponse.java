package de.ernteerfolg.service.uc.offermakeorupdate;

import de.ernteerfolg.service.domain.Offer;
import java.util.Optional;

public class MakeOrUpdateAnOfferResponse {

	enum MakeAnOfferState {
		ok, helperNotFound, duplicateOffer, taskNotFound, requestedDayVsOfferedDayMmismatch, offeredDaysAreInThePast, offerNotFound, foreignUser, taskNotAktive, taskOverBooked;

		public boolean isOk() {
			return ok.equals(this);
		}
	}

	private Offer offer;
	private MakeAnOfferState makeAnOfferState;

	MakeOrUpdateAnOfferResponse(MakeAnOfferState makeAnOfferState) {
		this.makeAnOfferState = makeAnOfferState;
	}

	MakeOrUpdateAnOfferResponse(MakeAnOfferState makeAnOfferState, Offer offer) {
		this.makeAnOfferState = makeAnOfferState;
		this.offer = offer;
	}


	public MakeAnOfferState getState() {
		return makeAnOfferState;
	}

	public Optional<Offer> getOffer() {
		return Optional.ofNullable(offer);
	}

	public boolean isOk() {
		return MakeAnOfferState.ok.equals(this.makeAnOfferState);
	}

	public String getErrorKey() {
		return makeAnOfferState.name();
	}
}
