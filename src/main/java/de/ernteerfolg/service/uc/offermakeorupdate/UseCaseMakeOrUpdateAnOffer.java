package de.ernteerfolg.service.uc.offermakeorupdate;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.Workday;
import de.ernteerfolg.service.security.SecurityUtils;
import de.ernteerfolg.service.uc.offermakeorupdate.MakeOrUpdateAnOfferResponse.MakeAnOfferState;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
class UseCaseMakeOrUpdateAnOffer implements InputPortMakeOrUpdateAnOffer {

    private static final MakeOrUpdateAnOfferResponse DUPLICATE_OFFER = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.duplicateOffer);
    private static final MakeOrUpdateAnOfferResponse OFFER_NOT_FOUND = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.offerNotFound);
    private static final MakeOrUpdateAnOfferResponse TASK_NOT_FOUND = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.taskNotFound);
    private static final MakeOrUpdateAnOfferResponse HELPER_NOT_FOUND = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.helperNotFound);
    private static final MakeOrUpdateAnOfferResponse REQUESTED_DAYS_VS_OFFERED_DAYS_MISMATCH = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.requestedDayVsOfferedDayMmismatch);
    private static final MakeOrUpdateAnOfferResponse FOREIGN_USER = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.foreignUser);
    private static final MakeOrUpdateAnOfferResponse TASK_NOT_ACTIVE = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.taskNotAktive);
    private static final MakeOrUpdateAnOfferResponse TASK_OVERBOOKED = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.taskOverBooked);
    private static final MakeOrUpdateAnOfferResponse OK = new MakeOrUpdateAnOfferResponse(
        MakeAnOfferState.ok);

    private OutputPortHelperStorage outputPortHelperStorage;
    private OutputPortWorkdayStorage outputPortWorkdayStorage;
    private OutputPortTaskStorage outputPortTaskStorage;
    private OutputPortOfferStorage outputPortOfferStorage;
    private OutputPortUserNotification outputPortUserNotification;


    public UseCaseMakeOrUpdateAnOffer(OutputPortHelperStorage outputPortHelperStorage,
        OutputPortWorkdayStorage outputPortWorkdayStorage,
        OutputPortTaskStorage outputPortTaskStorage,
        OutputPortOfferStorage outputPortOfferStorage,
        OutputPortUserNotification outputPortUserNotification) {
        this.outputPortHelperStorage = outputPortHelperStorage;
        this.outputPortWorkdayStorage = outputPortWorkdayStorage;
        this.outputPortTaskStorage = outputPortTaskStorage;
        this.outputPortOfferStorage = outputPortOfferStorage;
        this.outputPortUserNotification = outputPortUserNotification;
    }

    @Override
    public MakeOrUpdateAnOfferResponse makeAnOfferForCurrentHelper(Set<Workday> guaranteeWorkdays, Long taskId) {
        Optional<Helper> helperOptional = outputPortHelperStorage.findOneByUserIsCurrentUser();
        if (!helperOptional.isPresent()) {
            return HELPER_NOT_FOUND;
        }
        return makeAnOffer(helperOptional.get(), guaranteeWorkdays, taskId);
    }

    @Override
    public MakeOrUpdateAnOfferResponse makeAnOffer(Helper helper, Set<Workday> guaranteeWorkdays, Long taskId) {
        TaskAndState taskAndState = validateParametersForNewOffer(guaranteeWorkdays, taskId);
        if (taskAndState.isOk()) {
            return makeAnOffer(helper, guaranteeWorkdays, taskAndState.task);
        } else {
            return taskAndState.response;
        }
    }

    @Override
    public MakeOrUpdateAnOfferResponse updateAnOffer(Long offerId, Set<Workday> guaranteeWorkdays,
        Long taskId) {
        TaskAndState taskAndState = validateParametersForUpdateOffer(guaranteeWorkdays, taskId);
        if (taskAndState.isOk()) {
            return updateAnOffer(offerId, guaranteeWorkdays, taskAndState.task);
        } else {
            return taskAndState.response;
        }
    }

    private TaskAndState validateParametersForUpdateOffer(Set<Workday> guaranteeWorkdays,
        Long taskId) {
        Optional<Task> taskOptional = outputPortTaskStorage.findById(taskId);
        if (!taskOptional.isPresent()) {
            return new TaskAndState(TASK_NOT_FOUND, taskOptional.get());
        } else if (!areOfferedDaysMatchingRequestedDays(guaranteeWorkdays, taskOptional.get())) {
            return new TaskAndState(REQUESTED_DAYS_VS_OFFERED_DAYS_MISMATCH, taskOptional.get());
        } else if (!taskOptional.get().isActive()) {
            return new TaskAndState(TASK_NOT_ACTIVE, null);
        } else if (workdayOverbooked(guaranteeWorkdays, taskOptional.get())) {
            return new TaskAndState(TASK_OVERBOOKED, taskOptional.get());
        } else {
            return new TaskAndState(OK, taskOptional.get());
        }
    }

    private TaskAndState validateParametersForNewOffer(Set<Workday> guaranteeWorkdays,
        Long taskId) {
        Optional<Task> taskOptional = outputPortTaskStorage.findById(taskId);
        if (!taskOptional.isPresent()) {
            return new TaskAndState(TASK_NOT_FOUND, taskOptional.get());
        }
        if (!areOfferedDaysInThePast(guaranteeWorkdays)) {
            return new TaskAndState(REQUESTED_DAYS_VS_OFFERED_DAYS_MISMATCH, taskOptional.get());
        }
        return validateParametersForUpdateOffer(guaranteeWorkdays, taskId);
    }

    private boolean workdayOverbooked(Set<Workday> guaranteeWorkdays, Task task) {
        if (task.getRequestedDays().isEmpty()) {
            return false;
        }
        Set<Workday> requestedDays = new HashSet<>(task.getRequestedDays());
        ArrayList<Workday> oldGuaranteedWorkdays = new ArrayList<Workday>();
        outputPortOfferStorage.findByTaskId(task.getId()).forEach(
            offer -> oldGuaranteedWorkdays.addAll(offer.getGuaranteedDays()));

        Map<LocalDate, Integer> requestedDaysMap = requestedDays
            .stream()
            .collect(Collectors.groupingBy(
                Workday::getDate, Collectors.summingInt(Workday::getWorkingHours)));

        Map<LocalDate, Integer> oldGuaranteedWorkdaysMap = oldGuaranteedWorkdays
            .stream()
            .collect(Collectors.groupingBy(
                Workday::getDate, Collectors.summingInt(Workday::getWorkingHours)));

        Map<LocalDate, Integer> newGuaranteedWorkdaysMap = guaranteeWorkdays.parallelStream()
            .collect(Collectors.groupingBy(Workday::getDate, Collectors.summingInt(Workday::getWorkingHours)));

        requestedDaysMap.keySet().parallelStream().forEach(
            localDate -> {
                if (oldGuaranteedWorkdaysMap.containsKey(localDate)) {
                    requestedDaysMap.put(localDate, requestedDaysMap.get(localDate) - oldGuaranteedWorkdaysMap.get(localDate));
                }
                if (requestedDaysMap.get(localDate) < 0) {
                    requestedDaysMap.remove(localDate);
                }
                if (newGuaranteedWorkdaysMap.containsKey(localDate)) {
                    requestedDaysMap.put(localDate, requestedDaysMap.get(localDate) - newGuaranteedWorkdaysMap.get(localDate));
                }
            });
        return requestedDaysMap.values().stream().min(Integer::compareTo).get() < 0;


    }

    private MakeOrUpdateAnOfferResponse makeAnOffer(Helper helper, Set<Workday> guaranteeWorkdays, Task task) {

        if (areThereAlreadyOffersForTheRequestedTask(helper.getId(), task.getId())) {
            return DUPLICATE_OFFER;
        } else {

            Offer offer = saveOffer(helper, guaranteeWorkdays, task);

            // set the relation from workday to offer
            List<Workday> sortedGuaranteedDays = guaranteeWorkdays.stream()
                .sorted(Comparator.comparing(Workday::getDate))
                .collect(Collectors.toList());

            for (Workday workday : sortedGuaranteedDays) {
                workday.setOffer(offer);
                outputPortWorkdayStorage.save(workday);
            }

            notifyFarmerAboutNewOffer(task, offer);

            return new MakeOrUpdateAnOfferResponse(MakeAnOfferState.ok, offer);
        }
    }

    //todo dont allow to make an offer for old days
    private MakeOrUpdateAnOfferResponse updateAnOffer(Long offerId, Set<Workday> guaranteeWorkdays, Task task) {
        Optional<Offer> optionalOffer = outputPortOfferStorage.findById(offerId);
        if (!optionalOffer.isPresent()) {
            return OFFER_NOT_FOUND;
        }
        Offer offer = optionalOffer.get();

        if (!offer.getHelper().getUser().getLogin().equals(SecurityUtils.getCurrentUserLogin().get())) {
            return FOREIGN_USER;
        }

        // delete the old workdays
        for (Workday workday : offer.getGuaranteedDays()) {
            outputPortWorkdayStorage.delete(workday);
        }

        // save the changed ones
        List<Workday> sortedGuaranteedDays = guaranteeWorkdays.stream()
            .sorted(Comparator.comparing(Workday::getDate))
            .collect(Collectors.toList());

        for (Workday workday : sortedGuaranteedDays) {
            workday.setOffer(offer);
            outputPortWorkdayStorage.saveAndFlush(workday);
        }

        saveOffer(offerId, offer.getHelper(), guaranteeWorkdays, task);
        offer = outputPortOfferStorage.findById(offerId).orElseThrow(() -> new IllegalStateException());
        return new MakeOrUpdateAnOfferResponse(MakeAnOfferState.ok, offer);

    }


    private Offer saveOffer(Helper helper, Set<Workday> guaranteeWorkdays, Task task) {
        return saveOffer(null, helper, guaranteeWorkdays, task);
    }

    private Offer saveOffer(Long offerId, Helper helper, Set<Workday> guaranteeWorkdays, Task task) {

        Offer offer = new Offer();
        offer.setId(offerId);
        offer.setGuaranteedDays(guaranteeWorkdays);
        offer.setHelper(helper);
        offer.setTask(task);
        outputPortOfferStorage.save(offer);

        return offer;
    }

    private boolean areOfferedDaysInThePast(Set<Workday> guaranteeWorkdays) {
        LocalDate today = LocalDate.now();
        return guaranteeWorkdays.stream().noneMatch(workday -> today.isAfter(workday.getDate()));
    }

    private boolean areOfferedDaysMatchingRequestedDays(Set<Workday> guaranteeWorkdays, Task task) {
        Map<LocalDate, Workday> requestedDays = task.getRequestedDays().stream()
            .collect(Collectors.toMap(Workday::getDate,
                Function.identity()));

        return guaranteeWorkdays.stream()
            .noneMatch(offeredDay -> isDayNotRequested(requestedDays, offeredDay.getDate()));
    }

    private boolean isDayNotRequested(Map<LocalDate, Workday> requestedDays, LocalDate date) {
        //no check if hours are matching - maybe future use
        return !requestedDays.containsKey(date);
    }

    private boolean areThereAlreadyOffersForTheRequestedTask(Long helperId, Long taskId) {
        List<Offer> offersOfHelper = outputPortOfferStorage.findByHelper(helperId);
        long existingOffersForTheSameTask = offersOfHelper.stream()
            .filter(offer -> offer.getTask().getId().equals(taskId)).count();
        boolean areThereAlreadyOffersForTheRequestedTask = existingOffersForTheSameTask > 0;
        return areThereAlreadyOffersForTheRequestedTask;
    }


    @Async
    public void notifyFarmerAboutNewOffer(Task task, Offer offer) {
        if (task != null && offer != null) {
            outputPortUserNotification
                .notifyFarmAboutNewOffer(task.getFarm().getUser(), offer);
        }
    }
}
