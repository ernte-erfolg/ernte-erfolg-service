package de.ernteerfolg.service.uc.deletefarm;

public enum DeleteFarmState {
	ok, deletionKeyNotFound, farmNotFound, userNotFound;
}
