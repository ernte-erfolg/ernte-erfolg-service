package de.ernteerfolg.service.uc.deletefarm;

import java.util.List;

import de.ernteerfolg.service.domain.Task;

public interface OutputPortTaskStorage {

	List<Task> findByFarmIsCurrentUser();
	Task save(Task entity);
}
