package de.ernteerfolg.service.uc.deletefarm;

import java.util.List;

import de.ernteerfolg.service.domain.Offer;

public interface OutputPortOfferStorage {
	List<Offer> findByFarmId(Long id);
}
