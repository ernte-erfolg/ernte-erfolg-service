package de.ernteerfolg.service.uc.deletefarm;

import java.util.Optional;

import de.ernteerfolg.service.domain.Farm;

public interface OutputPortFarmStorage {
	Optional<Farm> findOneByUserIsCurrentUser();
	Farm findTaskContainerForDeletedFarms();
	void delete(Farm farm);
}
