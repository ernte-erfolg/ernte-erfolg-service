package de.ernteerfolg.service.uc.deletefarm;

import de.ernteerfolg.service.domain.Address;

public interface OutputPortAddressStorage {
	void delete(Address address);
}
