package de.ernteerfolg.service.uc.deletefarm;

import java.util.Optional;

import de.ernteerfolg.service.domain.User;

public interface OutputPortUserStorage {
	User save(User entity);
	Optional<User> findOneByDeletionKey(String deletionKey);
	void deleteUserByDeletionKey(String deletionKey);
}
