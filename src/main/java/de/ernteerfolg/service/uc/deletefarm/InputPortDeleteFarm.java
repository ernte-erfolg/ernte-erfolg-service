package de.ernteerfolg.service.uc.deletefarm;

import de.ernteerfolg.service.domain.User;

/**
 * 
 * 
 */
public interface InputPortDeleteFarm {

	public DeleteFarmState sendConfirmationMail(User currentUser);

	public DeleteFarmState confirmDeletion(String deletionKey);
}
