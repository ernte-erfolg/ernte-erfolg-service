package de.ernteerfolg.service.uc.deletefarm;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.domain.User;
import io.github.jhipster.security.RandomUtil;

@Service
class UseCaseDeleteFarm implements InputPortDeleteFarm {

	private OutputPortUserNotification outputPortUserNotification;
	private OutputPortTaskStorage outputPortTaskStorage;
	private OutputPortAddressStorage outputPortAddressStorage;
	private OutputPortFarmStorage outputPortFarmStorage;
	private OutputPortOfferStorage outputPortOfferStorage;
	private OutputPortUserStorage outputPortUserStorage;
	
	public UseCaseDeleteFarm(OutputPortUserNotification outputPortUserNotification, 
			OutputPortTaskStorage outputPortTaskStorage,
			OutputPortAddressStorage outputPortAddressStorage,
			OutputPortFarmStorage outputPortFarmStorage,
			OutputPortOfferStorage outputPortOfferStorage,
			OutputPortUserStorage outputPortUserStorage
			) {
		this.outputPortUserNotification = outputPortUserNotification;
		this.outputPortTaskStorage = outputPortTaskStorage;
		this.outputPortAddressStorage = outputPortAddressStorage;
		this.outputPortFarmStorage = outputPortFarmStorage;
		this.outputPortOfferStorage = outputPortOfferStorage;
		this.outputPortUserStorage = outputPortUserStorage;
	}
	
	public DeleteFarmState sendConfirmationMail(User currentUser) {
		
		if(!outputPortFarmStorage.findOneByUserIsCurrentUser().isPresent()) {
			return DeleteFarmState.farmNotFound; 
		} else if(currentUser != null) {
			currentUser.setDeletionKey(RandomUtil.generateActivationKey());
			outputPortUserNotification.notifyFarmAboutAccountDeletionLink(outputPortUserStorage.save(currentUser));
			return DeleteFarmState.ok;
		} else {
			return DeleteFarmState.userNotFound;
		}
	}

	@Transactional
	public DeleteFarmState confirmDeletion(String deletionKey) {
		return outputPortUserStorage.findOneByDeletionKey(deletionKey)
				.map(user -> anonymizeFarmAndDeleteUser(user)).orElse(DeleteFarmState.deletionKeyNotFound);
	}

	DeleteFarmState anonymizeFarmAndDeleteUser(User user) {
		outputPortFarmStorage.findOneByUserIsCurrentUser().ifPresent(farm -> notifyHelpersAndAnonymizeFarm(farm));
		outputPortUserStorage.deleteUserByDeletionKey(user.getDeletionKey());
		return DeleteFarmState.ok;
	}

	private void notifyHelpersAndAnonymizeFarm(Farm farm) {
		notifyHelpersAboutDeletedTask(outputPortOfferStorage.findByFarmId(farm.getId()));
		anonymizeTasksOfFarm(outputPortTaskStorage.findByFarmIsCurrentUser());
		outputPortFarmStorage.delete(farm);
	}

	/**
	 * GIVEN:
	 * Eine Farm mit Tasks (Gesuchen) bei denen Arbeitstage in der Zukunft
	 * vorhanden sind. Und an diesen Arbeitstage in der Zukunft Helfer Arbeit
	 * angeboten haben. 
	 * 
	 * WHEN: 
	 * Die Farm ihren Account löscht. 
	 * 
	 * THEN: 
	 * Muss jeder Helfer, der an mindestens einem Arbeitstag in der Zukunft ein
	 * Angebot abgegeben hat eine Benachrichtigungsemail bekommen.
	 * 
	 * @param farm
	 */
	private void notifyHelpersAboutDeletedTask(List<Offer> offersOfFarm) {		
		if( offersOfFarm != null) {
			for (Offer offer : offersOfFarm) {
				if(hasGuaranteedDayInFuture(offer)) {
					outputPortUserNotification.notifyHelperAboutDeletedTask(offer.getHelper().getUser(), offer);
				}
			}
		}
	}

	static boolean hasGuaranteedDayInFuture(Offer offer) {
		if( offer == null) {
			return false;	
		} else {
			// TODO What exactly means Future ?? https://ernteerfolg.atlassian.net/browse/ER-291
			return !offer.getGuaranteedDays().stream().filter(workday -> workday.getDate().isAfter(LocalDate.now())).collect(Collectors.toSet()).isEmpty();	
		}		
	}

	/**
	 * GIVEN: 
	 * Eine Farm mit Tasks (Gesuchen) bei denen Arbeitstage in der
	 * Vergangenheit vorhanden sind. Und an diesen Arbeitstage in der
	 * Vergangenheit Helfer Arbeit angeboten hatten.
	 *  
	 * WHEN: 
	 * Die Farm ihren Account löscht.
	 *  
	 * THEN: 
	 * Im Gesuch angefragte Tage, zu denen ein Helfer eine
	 * Arbeitsangebot abgegeben hat. Müssen erhalten bleiben., Diese sollen später
	 * für Reporting o.ä. ausgewertet werden können.
	 * 
	 * Entscheidung war, alle Tasks anonymisiert beizubehalten und einer dummy Farm zuzuweisen. 
	 * 
	 * @param farm
	 */
	private void anonymizeTasksOfFarm(List<Task> tasksOfFarm ) {
				
		for (Task task : tasksOfFarm) {
			anonymizeTaskAndAddItToTaskContainerForDeletedFarms(task);
		}
	}

	private void anonymizeTaskAndAddItToTaskContainerForDeletedFarms(Task task) {
		outputPortAddressStorage.delete(task.getAddress());
		task.setHowToFindUs(null);
		Task newTask = new Task();
		newTask.setId(task.getId());
		newTask.setTitle("deleted task");
		newTask.setRequestedDays(task.getRequestedDays());
		newTask.setFarm(outputPortFarmStorage.findTaskContainerForDeletedFarms());
		outputPortTaskStorage.save(newTask);
	}
}