package de.ernteerfolg.service.uc.deletefarm;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;

public interface OutputPortUserNotification {

	void notifyFarmAboutAccountDeletionLink(User saveUser);

	void notifyHelperAboutDeletedTask(User user, Offer offer);

}
