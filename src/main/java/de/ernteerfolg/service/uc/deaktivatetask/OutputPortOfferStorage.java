package de.ernteerfolg.service.uc.deaktivatetask;

import de.ernteerfolg.service.domain.Offer;
import java.util.List;

public interface OutputPortOfferStorage {

	List<Offer> findByTaskId(Long id);
}
