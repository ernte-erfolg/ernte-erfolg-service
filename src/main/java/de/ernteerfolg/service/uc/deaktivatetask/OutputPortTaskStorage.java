package de.ernteerfolg.service.uc.deaktivatetask;

import de.ernteerfolg.service.domain.Task;

public interface OutputPortTaskStorage {

    Task getTaskById(Long id);

    Task save(Task task);

}
