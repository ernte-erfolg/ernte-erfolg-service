package de.ernteerfolg.service.uc.deaktivatetask;

import de.ernteerfolg.service.domain.Task;

public interface InputPortDeactivateTask {

    Task deactivateTask(Long id);

    Task activateTask(Long id);

}
