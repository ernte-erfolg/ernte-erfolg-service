package de.ernteerfolg.service.uc.deaktivatetask;

import de.ernteerfolg.service.domain.Farm;

import java.util.Optional;

public interface OutputPortFarmStorage {

    Optional<Farm> findOneByUserIsCurrentUser();

}
