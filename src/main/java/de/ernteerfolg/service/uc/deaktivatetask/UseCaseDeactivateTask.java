package de.ernteerfolg.service.uc.deaktivatetask;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.web.rest.errors.BadRequestAlertException;
import java.util.List;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UseCaseDeactivateTask implements InputPortDeactivateTask {

    private static final String ENTITY_NAME = "task";

    private final OutputPortTaskStorage outputPortTaskStorage;
    private final OutputPortFarmStorage outputPortFarmStorage;
    private final OutputPortOfferStorage outputPortOfferStorage;
    private final OutputPortUserNotification outputPortUserNotification;


    public UseCaseDeactivateTask(OutputPortTaskStorage outputPortTaskStorage,
        OutputPortFarmStorage outputPortFarmStorage,
        OutputPortOfferStorage outputPortOfferStorage,
        OutputPortUserNotification outputPortUserNotification) {
        this.outputPortTaskStorage = outputPortTaskStorage;
        this.outputPortFarmStorage = outputPortFarmStorage;
        this.outputPortOfferStorage = outputPortOfferStorage;
        this.outputPortUserNotification = outputPortUserNotification;
    }

    @Override
    public Task deactivateTask(Long id) {
        Task task = outputPortTaskStorage.getTaskById(id);
        Optional<Farm> farm = outputPortFarmStorage.findOneByUserIsCurrentUser();
        if (!farm.isPresent() || task == null) {
            throw new BadRequestAlertException("Task not found",
                ENTITY_NAME, "deactivate Task", "deactivate");
        }
        if (!task.getFarm().equals(farm.get())) {
            throw new BadRequestAlertException("You cannot deactivate a foreign Task",
                ENTITY_NAME, "deactivate Task", "deactivate");
        }
        List<Offer> offerOfTask = outputPortOfferStorage.findByTaskId(task.getId());
        notifyHelpersAboutDeactivatedTask(offerOfTask);
        task.setActive(false);
        return outputPortTaskStorage.save(task);

    }

    @Override
    public Task activateTask(Long id) {
        Task task = outputPortTaskStorage.getTaskById(id);
        Optional<Farm> farm = outputPortFarmStorage.findOneByUserIsCurrentUser();
        if (!farm.isPresent() || task == null) {
            throw new BadRequestAlertException("Task not found",
                ENTITY_NAME, "activate Task", "activate");
        }
        if (!task.getFarm().equals(farm.get())) {
            throw new BadRequestAlertException("You cannot activate a foreign Task",
                ENTITY_NAME, "activate Task", "activate");
        }
        task.setActive(true);
        outputPortTaskStorage.save(task);
        return outputPortTaskStorage.getTaskById(task.getId());
    }

    @Async
    public void notifyHelpersAboutDeactivatedTask(List<Offer> offersOfTask) {
        if (offersOfTask != null) {
            for (Offer offer : offersOfTask) {
                outputPortUserNotification
                    .notifyHelperAboutDeactivatedTask(offer.getHelper().getUser(), offer);
            }
        }
    }

}
