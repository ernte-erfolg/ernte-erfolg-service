package de.ernteerfolg.service.uc.deaktivatetask;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;

public interface OutputPortUserNotification {

	void notifyHelperAboutDeactivatedTask(User user, Offer offer);

}
