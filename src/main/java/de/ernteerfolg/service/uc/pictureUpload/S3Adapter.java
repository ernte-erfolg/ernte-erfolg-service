package de.ernteerfolg.service.uc.pictureUpload;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Service
public class S3Adapter {

    @Value("${aws.s3AccessKey}")
    private String s3AccessKey;

    @Value("${aws.s3SecretAccessKey}")
    private String s3SecretAccessKey;

    @Value("${aws.s3Region}")
    private String s3Region;

    @Value("${aws.s3Bucket}")
    private String s3Bucket;

    private AmazonS3 createConnection (){
        AWSCredentials credentials = new BasicAWSCredentials(s3AccessKey, s3SecretAccessKey);
        return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion(s3Region).build();
    }

    public void uploadFile(File file, String fileName){
        AmazonS3 s3client = createConnection();
        s3client.putObject(s3Bucket,fileName, file);
    }

    public S3ObjectInputStream downloadFile(String fileName){
        AmazonS3 s3client = createConnection();
        return s3client.getObject(s3Bucket,fileName).getObjectContent();
    }


}
