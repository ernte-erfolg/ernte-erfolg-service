package de.ernteerfolg.service.uc.pictureUpload;

import de.ernteerfolg.service.service.FarmService;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.UUID;



@Service
@ConfigurationProperties(prefix = "picture.service")
public class PictureService {



    private final S3Adapter s3Adapter;

    @Value("${tmp.upload}")
    private String tmpUploadDir;

    @Value("${tmp.download}")
    private String tmpDownloadDir;


    public PictureService(S3Adapter s3Adapter) {
        this.s3Adapter = s3Adapter;
    }

    public String uploadPicture(MultipartFile multiPartFile,int targetPictureHeight, int targetPictureWidth) throws IOException {
        String pictureUUID = UUID.randomUUID().toString();
        File pictureRescaled = rescalePicture(multiPartFile.getInputStream(), pictureUUID, targetPictureHeight, targetPictureWidth);
        s3Adapter.uploadFile(pictureRescaled, pictureUUID);
        return pictureUUID;
    }

    private File rescalePicture(InputStream picture,String pictureUUID,int targetPictureHeight, int targetPictureWidth) throws IOException {
        BufferedImage originalBufferedImage = ImageIO.read(picture);
        File output = new File(tmpUploadDir+pictureUUID+".jpg");
         Thumbnails.of(originalBufferedImage)
            .size(targetPictureHeight, targetPictureWidth)
            .outputFormat("jpg")
            .outputQuality(1).toFile(output);
         output.deleteOnExit();
         return output;
    }

    public InputStream getPicture(String fileUUDI) {
        return s3Adapter.downloadFile(fileUUDI).getDelegateStream();
    }
}
