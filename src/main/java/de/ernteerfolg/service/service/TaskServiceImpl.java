package de.ernteerfolg.service.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Address;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.Task;
import de.ernteerfolg.service.repository.OfferRepository;
import de.ernteerfolg.service.repository.TaskRepository;
import de.ernteerfolg.service.service.dto.TasksInAreaDTO;

@Service
class TaskServiceImpl implements TaskService {

	private final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);
	private int[][] searchDistanceAreas = {{0, 30}, {31, 50}, {51, 70}, {71, 150}, {151, 500}};
	private TaskRepository taskRepository;
	private OfferRepository offerRepository;

	public TaskServiceImpl(TaskRepository taskRepository, OfferRepository offerRepository) {
		this.taskRepository = taskRepository;
		this.offerRepository = offerRepository;
	}

	@Override
	public Optional<Helper> getHelperWithOfferForCurrentFarmById(Long id) {

		log.debug("getHelperWithOfferForCurrentFarmById({})", id);
		Optional<Helper> helperOptionalResponse = Optional.empty();
		List<Offer> offersForFarm = offerRepository.findByFarmIsCurrentUser();
		List<Offer> offersOfHelper = offersForFarm.stream()
				.filter(offer -> offer.getHelper().getId().equals(id)).collect(Collectors.toList());
		if (!offersOfHelper.isEmpty()) {
			helperOptionalResponse = Optional.of(offersOfHelper.get(0).getHelper());
		}
		return helperOptionalResponse;
	}

	/* (non-Javadoc)
	 * @see de.ernteerfolg.service.service.TaskService#searchTasks(double, double)
	 */
	@Override
	public List<TasksInAreaDTO> searchTasks(boolean activeTask, LocalDate taskHasWorkingDaysGreater,
			BigDecimal lat, BigDecimal lon)
			throws TransformException {

		List<TasksInAreaDTO> result = new ArrayList<>();
		List<Task> allTasks = taskRepository
				.getAllTasksThatAreActiveHasRequestedDaysGreaterAndHasStillCapacity(activeTask,
						taskHasWorkingDaysGreater);

		int area = 0;
		while ((result.size() < 2) && area < searchDistanceAreas.length) {
			int minDistance = searchDistanceAreas[area][0];
			int maxDistance = searchDistanceAreas[area][1];
			area++;
			Map<Task, Double> taskAndDistanceMap = searchTasks(allTasks, lat, lon, minDistance,
					maxDistance);
			log.debug(
					"tasks in distance " + minDistance + " to " + maxDistance + " size: " + taskAndDistanceMap
							.size());

			if (!taskAndDistanceMap.isEmpty()) {
				TasksInAreaDTO tasksInArea = new TasksInAreaDTO(maxDistance);
				for (Entry<Task, Double> entry : taskAndDistanceMap.entrySet()) {
					tasksInArea.add(entry.getKey(), entry.getValue());
				}
				result.add(tasksInArea);
			}
		}

		return result;
	}


	/**
	 * Filtering the passed tasks by tasks that are located in the specified area.
	 *
	 * @param tasks all tasks to search/filter.
	 * @param lat latitude of the base coordinate
	 * @param lon longitude of the base coordinate
	 * @param minDistance the minimal distance of the area meassured from the base coordinate.
	 * @param maxDistance the maximuim distance of the area meassured from the base coordinate.
	 * @return a map with task,distance entries.
	 */
	private Map<Task, Double> searchTasks(List<Task> tasks, BigDecimal lat, BigDecimal lon,
			double minDistance, double maxDistance) throws TransformException {

		Map<Task, Double> taskAndDistanceMap = new HashMap<>();

		for (Task task : tasks) {
			Address address = task.getAddress();
			if (address != null && address.getLatitude() != null && address.getLongitude() != null
					&& lat != null && lon != null) {
				double calculatedDistance = getDistanceInKilometers(address.getLatitude().doubleValue(),
						address.getLongitude().doubleValue(), lat.doubleValue(), lon.doubleValue());
				log.trace("calculatedDistance " + calculatedDistance);
				if (calculatedDistance >= minDistance && calculatedDistance <= maxDistance) {
					taskAndDistanceMap.put(task, calculatedDistance);
				}
			}
		}
		return taskAndDistanceMap;
	}
    
    /**
     * Gets the distance between two coordinates in kilometers.
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return Distance between two coordinates in kilometers.
     * @throws TransformException
     * @throws org.opengis.referencing.operation.TransformException 
     */
	private double getDistanceInKilometers(double lat1, double lon1, double lat2, double lon2) throws TransformException {
		double distance = 0.0;

		GeodeticCalculator gc = new GeodeticCalculator();
        gc.setStartingPosition(new DirectPosition2D(lat1, lon1));
        gc.setDestinationPosition(new DirectPosition2D(lat2, lon2));

        distance = gc.getOrthodromicDistance();

        return distance/1000;
	}
}
