package de.ernteerfolg.service.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.repository.HelperRepository;
import de.ernteerfolg.service.repository.OfferRepository;

@Service
public class HelperService {

	private HelperRepository helperRepository;
	private OfferRepository offerRepository;

	public HelperService(HelperRepository helperRepository, OfferRepository offerRepository) {
		this.helperRepository = helperRepository;
		this.offerRepository = offerRepository;
	}
	
	public Optional<Helper> getHelperForCurrentUser() {
    	List<Helper> helpers = helperRepository.findByUserIsCurrentUser();
		if(helpers.size() > 1) {
			throw new RuntimeException("findByUserIsCurrentUser -> more than one!");
		} else if( helpers.isEmpty() ) {
			return Optional.empty();  
		} else {
			return Optional.of(helpers.get(0));
		}
	}

	public Helper getExpectedHelperForCurrentUser() {
		return getHelperForCurrentUser().orElseThrow(() -> new IllegalStateException("No helper logged in."));
	}

	public List<Helper> getHelperOfFarm(Long farmId) {
		List<Helper> result = new ArrayList<>();
		
		List<Offer> offersOfFarm = offerRepository.findByFarmId(farmId);
		result = offersOfFarm.stream().map(offer -> offer.getHelper()).collect(Collectors.toList());
		
		return result;
	}
}
