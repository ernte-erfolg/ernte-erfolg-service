package de.ernteerfolg.service.service.plz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
class ZipGeoJsonSourceCreator {

	/**
	 * This should only be called, if the data from https://public.opendatasoft.com os changing. Maybe all few years, there will be new zips available.
	 * This is the case, because we decided to NOT use an external service or database while runtime. Because of DSGVO.
	 */
	public static void main(String[] args) throws Exception {
		
		ZipGeoJsonSourceCreator creator = new ZipGeoJsonSourceCreator();
		Map<String,ZipGeoDataSet> plzDataSetMap = creator.parsePlzFile(new FileInputStream(creator.getBigPostleitzahlenDeutschlandFile()));
		
        FileOutputStream fos = new FileOutputStream("src/main/resources/config/liquibase/data/zip_geo.csv");
        
        try (OutputStreamWriter osw =  new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {

            String text = "id;country;latitude;longitude;zip\n";
            osw.write(text);
            
            int i = 1;
    		for (Entry<String,ZipGeoDataSet> entry : plzDataSetMap.entrySet()) {
    			osw.write(i++ + ";" + entry.getValue().getCountry() + ";" +  entry.getValue().getLatitude() + ";" + entry.getValue().getLongitude() + ";" + entry.getValue().getZip() + "\n" );
    		}
    		osw.flush();
    		osw.close();
        }
	}

	private File getBigPostleitzahlenDeutschlandFile() throws MalformedURLException, IOException, FileNotFoundException {
		URL url = new URL("https://public.opendatasoft.com/explore/dataset/postleitzahlen-deutschland/download/?format=json&timezone=Europe/Berlin&lang=en");
		ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());

		File outputfile = File.createTempFile("postleitzahlen-deutschland", "");
		System.out.println(outputfile.getAbsolutePath());
		FileOutputStream fileOutputStream = new FileOutputStream(outputfile);

		fileOutputStream.getChannel()
		  .transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
		
		fileOutputStream.flush();
		fileOutputStream.close();
		
		return outputfile;
	}
	
	private Map<String,ZipGeoDataSet> parsePlzFile(java.io.InputStream is) {

		Map<String,ZipGeoDataSet> plzDataSetMap = new HashMap<String, ZipGeoDataSet>();
		try (JsonParser jParser = new JsonFactory().createParser(is);) {
			
			if (jParser.nextToken() == JsonToken.START_ARRAY) {
				ZipGeoDataSet plzDataSet = new ZipGeoDataSet();
				while (jParser.nextToken() != JsonToken.END_ARRAY) {

					while (jParser.nextToken() != JsonToken.END_OBJECT) {

						String fieldname = jParser.getCurrentName();

						if ("datasetid".equals(fieldname)) {
							jParser.nextToken();
						}

						if ("recordid".equals(fieldname)) {
							jParser.nextToken();
							plzDataSet.setRecordid(jParser.getText());
						}

						if ("fields".equals(fieldname)) {
								gotoTokenValue(jParser, "note");
								plzDataSet.setCountry(jParser.getText());
								parseCoordinates(jParser, plzDataSet);								
								gotoTokenValue(jParser, "plz");
								plzDataSet.setZip(jParser.getText());
								while (jParser.nextToken() != JsonToken.END_OBJECT) {}
								while (jParser.nextToken() != JsonToken.END_OBJECT) {}
						}
					}
					plzDataSetMap.put(plzDataSet.getZip(), plzDataSet);
					plzDataSet = new ZipGeoDataSet();
				}
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return plzDataSetMap;
	}

	private  void parseCoordinates(JsonParser jParser, ZipGeoDataSet plzDataSet) throws IOException {
		gotoTokenValue(jParser, "geo_point_2d");
		jParser.nextToken();
		plzDataSet.setLatitude(new BigDecimal(jParser.getText()));
		jParser.nextToken();
		plzDataSet.setLongitude(new BigDecimal(jParser.getText()));
	}

	private  void gotoTokenValue(JsonParser jParser, String fieldname) throws IOException {
		while (true) {
			jParser.nextToken();
			if (fieldname.equals(jParser.getCurrentName())){
				jParser.nextToken();
				break;
			}
		}
	}
}
