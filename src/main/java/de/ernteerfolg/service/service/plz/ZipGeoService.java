package de.ernteerfolg.service.service.plz;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.ZipGeoData;
import de.ernteerfolg.service.repository.ZipGeoDataRepository;

@Service
public class ZipGeoService {

	private final Logger log = LoggerFactory.getLogger(ZipGeoService.class);
	private ZipGeoDataRepository zipGeoDataRepository;
	
	public ZipGeoService(ZipGeoDataRepository zipGeoDataRepository) {
		this.zipGeoDataRepository = zipGeoDataRepository;
	}
	
	public Optional<ZipGeoData> findByZip(String zip) {
		
		log.debug("SERVICE findByZip : {}", zip);
		Optional<ZipGeoData> result = Optional.empty();
		List<ZipGeoData> zipGeoDataList =  zipGeoDataRepository.findByZip(zip);
		if(zipGeoDataList.size() > 1) {
			throw new RuntimeException("Error while determinig GeoData fpr zip " + zip);
		} else if( zipGeoDataList.size() == 1 ) {
			result = Optional.of(zipGeoDataList.get(0));
		}
		return result;
	}
}
