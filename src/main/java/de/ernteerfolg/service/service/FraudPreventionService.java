package de.ernteerfolg.service.service;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.TrashMailDomain;
import de.ernteerfolg.service.repository.TrashMailDomainRepository;

@Service
public class FraudPreventionService {

	private TrashMailDomainRepository trashMailDomainRepository;
	
	public FraudPreventionService(TrashMailDomainRepository trashMailDomainRepository) {
		this.trashMailDomainRepository = trashMailDomainRepository;
	}
	
	public boolean isEmailBlacklisted(String email) {
		
        TrashMailDomain filterBy = new TrashMailDomain();
        filterBy.setDomain(getDomain(email)); 
         
        Example<TrashMailDomain> example = Example.of(filterBy); 
 
        List<TrashMailDomain> foundTarshMails = trashMailDomainRepository.findAll(example);
        
        return !foundTarshMails.isEmpty();
	}

	private String getDomain(String email) {
		if(!email.contains("@")) {
			throw new RuntimeException("invalid email " + email);
		}
		return  email.substring(email.indexOf("@") + 1);
	}
	
}
