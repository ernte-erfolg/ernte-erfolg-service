package de.ernteerfolg.service.service;

public class ValidationException extends Exception {

	private static final long serialVersionUID = -328186754536651337L;
	private Validation validation;
	
	public ValidationException(Validation validation) {
		super(validation.getDescription());
		this.validation = validation;
	}

	public final String getErrorKey() {
		return validation.name();
	}
}
