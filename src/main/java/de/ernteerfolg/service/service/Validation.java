package de.ernteerfolg.service.service;

public enum Validation {
	
	RequestedDayVsOfferedDayMmismatch("You offer days, that are not requested!"),
	DuplicateOffer("The user has already an offer for that task."),
	TaskNotFound( "The requested Task  was not found"),
	OfferNotFound( "The requested Offer was not found"),
	NoHelperLoggedIn("No helper logged in.");
	
	private String description;
	
	private Validation(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
