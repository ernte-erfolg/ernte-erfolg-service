package de.ernteerfolg.service.service;

import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.domain.User;

public interface NotificationService {

	/**
	 * Sends a notification to the user with an account activation link. Until the user didn't call
	 * this link, the account is not activated.
	 */
	void notifyUserAboutActivationLink(User user);

	/**
	 * Sends a notification to the user with an account deletion link. Until the user
	 * didn't call this link, the account is not deleted.
	 *
	 * @param user
	 */
	void notifyFarmAboutAccountDeletionLink(User user);

	/**
	 * Sends a notification to the helper that have an open offer for a task that is deleted.
	 */
	void notifyHelperAboutDeletedTask(User user, Offer offer);

	/**
	 * Sends a notification to the helper that have an offer for a task that is deactivated.
	 */
	void notifyHelperAboutDeactivatedTask(User user, Offer offer);

	/**
	 * Sends a notification to the farmer that an new offer for a task is created.
	 */
	void notifyFarmAboutNewOffer(User user, Offer offer);
}
