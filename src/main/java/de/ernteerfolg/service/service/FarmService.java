package de.ernteerfolg.service.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.ernteerfolg.service.uc.pictureUpload.PictureService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.ernteerfolg.service.domain.Farm;
import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.domain.Offer;
import de.ernteerfolg.service.repository.FarmRepository;
import de.ernteerfolg.service.repository.OfferRepository;
import de.ernteerfolg.service.security.SecurityUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FarmService {

	private FarmRepository farmRepository;
	private HelperService helperService;
	private OfferRepository offerRepository;
	private final PictureService pictureService;

    @Value("${picture.service.headerPictureHeight}")
    private int headerPictureHeight;

    @Value("${picture.service.headerPictureWidth}")
    private int headerPictureWidth;

    @Value("${picture.service.profilePictureHeight}")
    private int profilePictureHeight;

    @Value("${picture.service.profilePictureWidth}")
    private int profilePictureWidth;

    @Value("${tmp.upload}")
    private String tmpUploadDir;

    @Value("${tmp.download}")
    private String tmpDownloadDir;

	public FarmService(FarmRepository farmRepository, HelperService helperService, OfferRepository offerRepository, PictureService pictureService) {
		this.farmRepository = farmRepository;
		this.helperService = helperService;
		this.offerRepository = offerRepository;
        this.pictureService = pictureService;
    }

	public boolean isCurrentUserFarmOwner() {
		return farmRepository.findOneByUserIsCurrentUser().isPresent();
	}

	/**
	 * @return The farm that is owned by the current user.
	 * @throws IllegalStateException if the current user did not own a farm.
	 */
	public Farm getExpectedFarmForCurrentUserIsOwner() {
		return farmRepository.findOneByUserIsCurrentUser().orElseThrow(() -> new IllegalStateException("No farm logged in."));
	}

    public Optional<Farm> getFarmForCurrentUserIsOwner() {
        List<Farm> farms = farmRepository.findByUserIsCurrentUser();
        if(farms.size() > 1) {
            throw new RuntimeException("findByUserIsCurrentUser -> more than one!");
        } else if( farms.isEmpty() ) {
            return Optional.empty();
        } else {
            return Optional.of(farms.get(0));
        }
    }

    public Optional<Farm> getFarmById(Long farmId) {

		Optional<Farm> farmOptional = farmRepository.findById(farmId);

		if(farmOptional.isPresent()) {
			Farm farm = farmOptional.get();
			if( hasToBeAnonymized(farm.getId()) ) {
				farm = toAnonymousFarm(farm);
				farmOptional = Optional.of(farm);
			}
			return farmOptional;
		} else {
			return Optional.empty();
		}
	}

	private Farm toAnonymousFarm(Farm farmOld) {
		Farm farm = new Farm();
		farm.setId(farmOld.getId());
		farm.setName(farmOld.getName());
		farm.setAdditionalInformation(farmOld.getAdditionalInformation());
		return farm;
	}

	private boolean hasToBeAnonymized(Long farmId) {
		return ! (farmRepository.findOneByUserIsCurrentUser().isPresent() || isCurrentUserAHelperWithAnOfferForThatFarm(farmId));
	}

	private boolean isCurrentUserAHelperWithAnOfferForThatFarm(Long farmId) {
		boolean result = false;
		if( SecurityUtils.isCurrentUserHelper() ) {
			result = didHelperMakeAnOffer(farmId, helperService.getExpectedHelperForCurrentUser().getId());
		}
		return result;
	}

	private boolean didHelperMakeAnOffer(Long farmId, Long helperId) {
		return getHelperOfFarm(farmId).stream().filter( helper -> helper.getId().equals(helperId)).count() == 1;
	}

	private List<Helper> getHelperOfFarm(Long farmId) {
		List<Helper> result = new ArrayList<>();

		List<Offer> offersOfFarm = offerRepository.findByFarmId(farmId);
		result = offersOfFarm.stream().map(offer -> offer.getHelper()).collect(Collectors.toList());

		return result;
	}

    public Farm uploadHeader(MultipartFile profileImage) throws IOException {
        String picture = pictureService.uploadPicture(profileImage, headerPictureHeight, headerPictureWidth);
        Optional<Farm> farm = farmRepository.findOneByUserIsCurrentUser();
        if(farm.isPresent()){
            Farm farmCurrent = farm.get();
            farm.get().setHeaderPictureUUID(picture);
            farmRepository.save(farmCurrent);
            return farmCurrent;
        } else throw new RuntimeException("farmer not Found");



    }

    public Farm uploadProfile(MultipartFile profileImage) throws IOException {
        String picture = pictureService.uploadPicture(profileImage, profilePictureHeight, profilePictureWidth);
        Optional<Farm> farm = farmRepository.findOneByUserIsCurrentUser();
        if(farm.isPresent()){
            Farm farmCurrent = farm.get();
            farm.get().setProfilePictureUUID(picture);
            farmRepository.save(farmCurrent);
            return farmCurrent;
        } else throw new RuntimeException("farmer not Found");
    }

    public File getHeader() throws IOException {
        Optional<Farm> farm = farmRepository.findOneByUserIsCurrentUser();
        if(farm.isPresent()){
            String fileUUDI = farm.get().getHeaderPictureUUID();
            File header = new File(farm.get().getHeaderPictureUUID());
            FileUtils.copyToFile(pictureService.getPicture(fileUUDI), header);
            header.deleteOnExit();
            return header;

        } else throw new RuntimeException("famer not Found");

    }

    public File downloadHeader() throws IOException {

        Optional<Farm> farm = farmRepository.findOneByUserIsCurrentUser();
        if(farm.isPresent()){
            String fileUUID = farm.get().getHeaderPictureUUID();
            return getPicture(fileUUID);

        } else throw new RuntimeException("famer not Found");

    }

    public File downloadProfile() throws IOException {
        Optional<Farm> farm = farmRepository.findOneByUserIsCurrentUser();
        if(farm.isPresent()){
            String fileUUID = farm.get().getProfilePictureUUID();
            return getPicture(fileUUID);

        } else throw new RuntimeException("famer not Found");
    }

    private File getPicture(String fileUUID) throws IOException {
        File returnFile= new File(tmpDownloadDir+ fileUUID +"jpg");
        FileUtils.copyInputStreamToFile(pictureService.getPicture(fileUUID), returnFile);
        returnFile.deleteOnExit();
        return returnFile;
    }
}
