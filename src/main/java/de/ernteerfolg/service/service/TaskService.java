package de.ernteerfolg.service.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.opengis.referencing.operation.TransformException;

import de.ernteerfolg.service.domain.Helper;
import de.ernteerfolg.service.service.dto.TasksInAreaDTO;

public interface TaskService {

	/**
	 * Searches for Tasks in areas. The areas to search are specified in {@link
	 * #searchDistanceAreas}.
	 *
	 * @param onlyActiveTasks should be returned only active tasks
	 * @param taskHasWorkingDaysGreater get all tasks which have requested working days greater this
	 * date
	 * @param lat latitude of the place from which to search
	 * @param lon longitude of the place from which to search
	 * @return Zero, one or two TasksInAreaDTO Objects in a list.
	 * @see TaskServiceImplTest
	 */
	List<TasksInAreaDTO> searchTasks(boolean onlyActiveTasks, LocalDate taskHasWorkingDaysGreater,
			BigDecimal lat, BigDecimal lon) throws TransformException;


	/**
	 * Get the Helper with the given Id, if he has an offer for a task of the current logged in farm.
	 *
	 * @param id The id of the helper.
	 * @return An optional helper.
	 */
	Optional<Helper> getHelperWithOfferForCurrentFarmById(Long id);
}
